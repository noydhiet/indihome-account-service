/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/28/2019
 */
module.exports = {
    messagePayload: {
        "messages": [
            {
                "header": "Message test: personal type",
                "isChecked": false,
                "referenceId": "987654321-11",
                "messageType": "personal",
                "imageUrl": "",
                "userId": "5dcbc146024ae600194f0c85"
            },
            {
                "header": "Message test: personal type",
                "isChecked": false,
                "referenceId": "987654321-12",
                "messageType": "personal",
                "imageUrl": "",
                "userId": "5dcbc146024ae600194f0c85"
            },
            {
                "header": "Message test: public type",
                "isChecked": false,
                "referenceId": "987654321-13",
                "messageType": "public",
                "imageUrl": "",
                "userId": "5dcbc146024ae600194f0c85"
            }
        ]
    },
    invalidPayload: {
        "messages": [
            {
                "header": "Message test: personal type",
                "isChecked": false,
                "referenceId": "987654321-1",
                "messageType": "personal",
                "imageUrl": "",
                "userId": null
            }
        ]
    },
    userId: "5dcbc146024ae600194f0c85",
    updateMessageReq: {
        checked: true
    },
    invalidType: 'invalid'
};