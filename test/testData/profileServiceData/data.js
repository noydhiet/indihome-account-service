/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathne
 */
module.exports = {
  userData: {
    indiHomeNum: '5dcbc146024ae600194f0c85',
    jwt: 'asdasdasdqw1223123adas'
  },
  testLocation: {
    userId: '1',
    userEmail: 'pccw@dev.com',
    sto: 'BIN',
    deviceId: '18226081',
    locId: 'ODP-TEST-BINTARO',
    province: 'test_pro',
    provinceCode: 'test_pro_1',
    city: 'test_city',
    cityCode: 'test_1',
    district: 'test_dis',
    districtCode: 'test_dis_1',
    street: 'test_street',
    streetCode: 'test_street_1',
    blockNumber: 'test',
    postalCode: '12345',
    addressDesc: 'test',
    block: 'test',
    floor: '10',
    room: 'test',
    latitude: '0.0001',
    longitude: '0.32',
    areaCode: '100',
    rt: 'rt',
    rw: 'rw',
    system: 'sys',
  },
  indiHomeNum: '122207447517'

};
