/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/28/2019
 */
module.exports = {
  notificationPayload: {
    notifications: [
      {
        header: 'Notification test: activity type',
        isChecked: false,
        referenceId: '987654321-1',
        notificationType: 'activity',
        imageUrl: '',
        userId: '5dcbc146024ae600194f0c85',
        group: 'G1',
        body: {
          topic: 'New installation activity',
          description: 'installation scheduled',
          data: {
            ticketId: 'MYIN-1234567891261'
          }
        }
      },
      {
        header: 'Notification test: notification type',
        isChecked: false,
        referenceId: '987654321-2',
        notificationType: 'notification',
        imageUrl: '',
        userId: '5dcbc146024ae600194f0c85',
        group: 'G1',
        body: {
          topic: 'New installation notification',
          description: 'Please settle the outstanding bill soon',
          data: {
            ticketId: 'MYIN-1234567891262'
          }
        }
      },
      {
        header: 'Notification test: action type',
        isChecked: false,
        referenceId: '987654321-3',
        notificationType: 'action',
        imageUrl: '',
        userId: '5dcbc146024ae600194f0c85',
        group: 'G1',
        body: {
          topic: 'New installation will be finished soon',
          description: 'Explore features',
          data: {
            ticketId: 'MYIN-1234567891263'
          }
        }
      }
    ]
  },
  invalidPayload: {
    notifications: [
      {
        header: 'Notification test: activity type',
        isChecked: false,
        referenceId: '987654321-1',
        notificationType: 'activity',
        imageUrl: '',
        userId: null,
        group: 'G1',
        body: {
          topic: 'New installation activity',
          description: 'installation scheduled',
          data: {
            ticketId: 'MYIN-1234567891261'
          }
        }
      }
    ]
  },
  userId: '5dcbc146024ae600194f0c85',
  updateNotificationReq: {
    checked: true
  },
  invalidType: 'test'
};
