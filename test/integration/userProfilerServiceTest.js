let chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const profileService = require('../../bin/services/profileService');
const {testLocation, indiHomeNum} = require('../testData/profileServiceData/data');


describe('Profile Service', function () {

  it('Should add location to the user', function () {
    this.timeout(100000);
    return profileService.createAccountForUserLocation(testLocation).then(resp => {
      expect(resp).not.to.be.null;
      expect(resp.data).not.to.be.null;
    });
  });

  it('should get the locations of the user', function () {
    this.timeout(100000);
    return profileService.getUserLocations('1', '2').then(resp => {
      expect(resp).not.to.be.null;
      expect(resp.data).not.to.be.null;
      expect(resp.data).not.to.be.equal({});
      expect(resp.data.accounts[0].UserId).to.be.equal(1);
      expect(resp.data.accounts[0].id).to.be.equal(2);
      expect(resp.data.locations).not.to.be.null;
      expect(resp.data.locations[0].sto).to.be.equal(testLocation.sto);
      expect(resp.data.location[0].deviceId).to.be.equal(testLocation.deviceId);
      expect(resp.data.location[0].locId).to.be.equal(testLocation.locId);

    });
  });

  it('should get the not locations of the invalid user', function () {
    this.timeout(100000);
    return profileService.getUserLocations('1', 'invalid_account').catch(resp => {
      expect(resp).not.to.be.null;
    });
  });

  it('should get user account successfully', function () {
    this.timeout(100000);
    return profileService.getAccountByIndiHomeNum(indiHomeNum).then(resp => {
      expect(resp).not.to.be.null;
    });
  });

  it('should fail to get user account. Invalid params', function () {
    this.timeout(100000);
    return profileService.getAccountByIndiHomeNum(null).catch(err => {
      expect(err).not.to.be.null;
    });
  });


});
