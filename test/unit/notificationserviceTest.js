/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/28/2019
 */

const notificationService = require('../../bin/services/notificationService');
const {NOTIFICATION_TYPES} = require('../../bin/helpers/utils/constants/notificationConstants');
const testData = require('../testData/notificationServiceData/data');

let chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Notification Service', function() {
    this.timeout(100000);
    let response = null;
    let notificationObj = {};
    // create notifications before
    before(() => {
        response = notificationService.saveNotifications(testData.notificationPayload.notifications);
        notificationService.getNotificationsByType(NOTIFICATION_TYPES[0], testData.userId).then(data => {
            notificationObj = data[0];
        });
    });
    describe('Save Notifications', () => {
        it('Should save notifications successfully', function () {
            this.timeout(100000);
            return response.then(data => {
                expect(data).not.to.be.empty;
                expect(data.status).to.be.equal('success');
            });
        });
        it('Should Fail to save notifications. Invalid request data', function () {
            this.timeout(100000);
            const response = notificationService.saveNotifications(testData.invalidPayload.notifications);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Get Notifications', () => {
        it('Should get notifications by Type: '.concat(NOTIFICATION_TYPES[0]), function () {
            this.timeout(100000);
            const notifications = notificationService.getNotificationsByType(NOTIFICATION_TYPES[0], testData.userId);
            return notifications.then(data => {
                expect(data).not.to.be.empty;
                expect(data.length).to.be.above(0);
            });
        });
        it('Should get notifications by Type: '.concat(NOTIFICATION_TYPES[1]), function () {
            this.timeout(100000);
            const notifications = notificationService.getNotificationsByType(NOTIFICATION_TYPES[1], testData.userId);
            return notifications.then(data => {
                expect(data).not.to.be.empty;
                expect(data.length).to.be.above(0);
            });
        });
        it('Should get notifications by Type: '.concat(NOTIFICATION_TYPES[2]), function () {
            this.timeout(100000);
            const notifications = notificationService.getNotificationsByType(NOTIFICATION_TYPES[2], testData.userId);
            return notifications.then(data => {
                expect(data).not.to.be.empty;
                expect(data.length).to.be.above(0);
            });
        });
        it('Should fail to get notifications. Invalid type', function () {
            this.timeout(100000);
            const notifications = notificationService.getNotificationsByType(testData.invalidType, testData.userId);
            return notifications.catch(data => {
                expect(data).to.be.empty;
            });
        });
        it('Should fail to get notifications. Empty type', function () {
            this.timeout(100000);
            const notifications = notificationService.getNotificationsByType(null, testData.userId);
            return notifications.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

  describe('Update Notifications', () => {
    it('Should successfully update notification by Id', function () {
      this.timeout(100000);
      const response = notificationService.updateNotification(notificationObj.notificationId, testData.updateNotificationReq.checked);
      return response.then(data => {
        expect(data).not.to.be.empty;
      });
    });
    it('Should fail to update notification. Empty path params ', function () {
      this.timeout(100000);
      const response = notificationService.updateNotification(null, testData.updateNotificationReq.checked);
      return response.catch(data => {
        expect(data).to.be.empty;
      });
    });
    it('Should fail to update notification. Empty req body ', function () {
      this.timeout(100000);
      const response = notificationService.updateNotification(notificationObj.notificationId, null);
      return response.catch(data => {
        expect(data).to.be.empty;
      });
    });

  });
});
