/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/11/2019
 */

const messageService = require('../../bin/services/messageService');
const {MESSAGE_TYPES} = require('../../bin/helpers/utils/constants/messageConstants');
const testData = require('../testData/messageServiceData/data');

let chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Message Service', function() {
    this.timeout(100000);
    let response = null;
    let messageObj = {};
    // create messages before
    before(() => {
        response = messageService.saveMessages(testData.messagePayload.messages);
        messageService.getMessagesByType(MESSAGE_TYPES.PERSONAL, testData.userId).then(data => {
            messageObj = data[0];
        });
    });

    describe('Save Messages', () => {
        it('Should save messages successfully', function () {
            this.timeout(100000);
            return response.then(data => {
                expect(data).not.to.be.empty;
                expect(data.status).to.be.equal('success');
            });
        });
        it('Should Fail to save messages. Invalid request data', function () {
            this.timeout(100000);
            const response = messageService.saveMessages(testData.invalidPayload.messages);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Get Messages', () => {
        it('Should get messages by  of Type: personal', function () {
            this.timeout(100000);
            const messages = messageService.getMessagesByType(MESSAGE_TYPES.PERSONAL, testData.userId);
            return messages.then(data => {
                expect(data).not.to.be.empty;
                expect(data.length).to.be.above(0);
            });
        });
        it('Should get messages by of Type: public/promo', function () {
            this.timeout(100000);
            const messages = messageService.getMessagesByType(MESSAGE_TYPES.PROMO, testData.userId);
            return messages.then(data => {
                console.log(JSON.stringify(data))
                expect(data).not.to.be.empty;
                expect(data.length).to.be.above(0);
            });
        });
        it('Should fail to get messages. Invalid type', function () {
            this.timeout(100000);
            const messages = messageService.getMessagesByType(testData.invalidType, testData.userId);
            return messages.catch(data => {
                expect(data).to.be.empty;
            });
        });
        it('Should fail to get messages. Empty type', function () {
            this.timeout(100000);
            const messages = messageService.getMessagesByType(null, testData.userId);
            return messages.catch(data => {
                expect(data).to.be.empty;
            });
        });
        it('Should get all user messages', function () {
            this.timeout(100000);
            const messages = messageService.getUserMessages(testData.userId);
            return messages.then(data => {
                expect(data.data).not.to.be.empty;
                expect(data.data.allMessages).not.to.be.empty;
                expect(data.data.personalMessages).not.to.be.empty;
                expect(data.data.promoMessages).not.to.be.empty;
            });
        });
    });

    describe('Update Messages', () => {
        it('Should successfully update message by Id', function () {
            this.timeout(100000);
            const response = messageService.updateMessage(messageObj.messageId, testData.updateMessageReq.checked);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });
        it('Should fail to update message. Empty path params ', function () {
            this.timeout(100000);
            const response = messageService.updateMessage(null, testData.updateMessageReq.checked);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
        it('Should fail to update message. Empty req body ', function () {
            this.timeout(100000);
            const response = messageService.updateMessage(messageObj.messageId, null);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });
});