/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 02/20/2020
 */
const UserAPI = require('../modules/user/repositories/commands/domain');
const logger = require('../helpers/utils/logger');
const ctx = 'server-checkIndihomeAccount-profile-service';
const wrapper = require('../helpers/utils/wrapper');

const getSSOUserData = async(userId)=>{
  this.userAPI = new UserAPI();
  try{
    // connecting to identity server
    const userData = await this.userAPI.getSSOUserById(userId);
    if(userData.status != 200){
      return null
    }

    return userData.data;

  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err,500);
  }
}

const getSSOUserDataByEmail = async(email)=>{
  this.userAPI = new UserAPI();
  try{
    // connecting to identity server
    const userData = await this.userAPI.getSSOUser(email);
    if(userData.status != 200){
      return null
    }

    return userData.data;

  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err,500);
  }
}


module.exports = {
    getSSOUserData,
    getSSOUserDataByEmail
};
