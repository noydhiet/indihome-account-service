/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/11/2019
 */
const wrapper = require('../helpers/utils/wrapper');
const deviceRepository = require('../repository/deviceRepository');
const uuidv4 = require('uuid/v4');

//Logging
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'info';


const saveUserDecive = async (payload) => {
  try{
    // saving a device
    let deviceId = payload.deviceId;
    if(payload.deviceId == ''){
      deviceId = uuidv4();
    }

    let deviceObject = {
      ssoUserId:'',
      deviceId:'',
      fcmToken:'',
      deviceType:''
    };

    deviceObject.deviceId = deviceId;
    deviceObject.ssoUserId = payload.ssoUserId;
    deviceObject.fcmToken = payload.fcmToken;
    deviceObject.deviceType = payload.deviceType;
    deviceObject.ssoUserId = payload.ssoUserId;

    const saveDevice = await deviceRepository.saveDevice(deviceObject);
    return saveDevice;

  }catch (err) {
    logger.error('Error.');
    logger.error(err);
    return wrapper.formatData(err,500);
  }
};


const deleteDecive = async (fcmToken,userId) => {
  try{
    const deletedDevice = await deviceRepository.deleteDevice(fcmToken,userId);
    return deletedDevice;

  }catch (err) {
    logger.error('Error.');
    logger.error(err);
    return wrapper.formatData(err,500);
  }
};



const getDecives = async (userId) => {
  try{
    const devices = await deviceRepository.getDevicesWithEmail(userId);
    return devices;

  }catch (err) {
    logger.error('Error.');
    logger.error(err);
    return wrapper.formatData(err,500);
  }
};



module.exports = {
  saveUserDecive,deleteDecive,getDecives
};