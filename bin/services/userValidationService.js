const fs = require('fs');
const path = require('path');
let blacklistEmails = [];
const blacklistEmailsPath = path.join(__dirname, '../../data/emails/blacklistedEmails.csv');

const lineReader = require('readline').createInterface({
  input: fs.createReadStream(blacklistEmailsPath)
});
lineReader.on('line', data => blacklistEmails.push(data));

/**
 *
 * @param email
 * @returns {boolean}
 */
const isBlacklistedEmail = email => {
  return blacklistEmails.includes(email) || blacklistEmails.some(mail => email.includes(mail));
};

module.exports = {
  isBlacklistedEmail
};
