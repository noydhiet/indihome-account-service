
const ClientAuthorization = require('./clinetAuthorizationService');
const authenticationService = new ClientAuthorization();
const rp = require('request-promise');

module.exports.createRequest = async (uri, payload) => {
  const authorization = `Bearer ${await authenticationService.getJWTFForUser()}`;
  const options = {
    method: 'POST',
    uri: uri,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': authorization
    },
    body: payload,
    json: true,
    strictSSL: false
  };

  return rp.post(options);
};
