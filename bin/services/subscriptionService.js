/*
 * Copyright (c) 2020. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 2/27/20, 2:18 PM
 */

const config = require('../config/index');
const logger = require('../helpers/utils/logger');
const rp = require('request-promise');
const ctx = 'SubscriptionService';

const host = config.get('/productSubscription').productSubscriptionBase;
const updatePackageEndpoint = config.get('/productSubscription').updatePackageEndpoint;
const timeout = config.get('/productSubscription').timeout;
/**
 *
 * @param transactionId
 * @param updatedPackage
 * @param updatedPackage.price
 * @returns {Promise<{success: boolean}>}
 */
module.exports.updateSelectedPackage = async (transactionId, updatedPackage) => {
  const endpoint = `${host}${updatePackageEndpoint}/${transactionId}`;
  const options = {
    method: 'PUT',
    timeout,
    uri: endpoint,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${config.get('/authorization').token}`,
    },
    json: {package: updatedPackage},
    strictSSL: false
  };

  const response = await rp.put(options).catch(error => {
    logger.log(ctx, `system error: ${JSON.stringify(error)}`, error);
  });
  logger.log(ctx, `response from API: for ${JSON.stringify(options)} as ${JSON.stringify(response)}`);
  if (response.ok && response.status === 200) {
    return {
      success: true
    };
  }
  return {
    success: false
  };
};
