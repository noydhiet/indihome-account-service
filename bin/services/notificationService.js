/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/28/2019
 */

const joi = require('@hapi/joi');
const config = require('../config/index');
const esIndexHomeNotifications = config.get('/elasticIndexes/homeNotifications');
const insertDoc = require('../helpers/es/insertData');
const searchDoc = require('../helpers/es/searchData');
const updateDoc = require('../helpers/es/updateData');
const validator = require('../helpers/utils/validator');
const logger = require('../helpers/utils/logger');
const {BadRequestError, ConflictError} = require('../helpers/error');
const deviceRepository = require('../repository/deviceRepository');
const userProfileRepository = require('../repository/profileRepository');
const rp = require('request-promise');
const ctx = 'notificationService';
const wrapper = require('../helpers/utils/wrapper');
const ssoService = require('../services/ssoService');


/**
 * Object model for ES notification
 */
const notificationSchema = joi.object({
  header: joi.string().required(),
  referenceId: joi.string().required(),
  isChecked: joi.boolean().optional(),
  notificationType: joi.string().required(),
  category: joi.string().required(),
  imageUrl: joi.string().optional().allow(''),
  userId: joi.string().required(),
  group: joi.string().required(),
  body: joi.object().required() // can check keys wise
});

/**
 * Save notification as a bulk
 * @param {{notifications: [{imageUrl: string, header: *, notificationType: string, category: string, body: {data: {ktp: *, status: *}, topic: *, description: *}, userId: *, isChecked: boolean, referenceId: *}]}} notifications
 * - notifications payload
 * @returns {Promise<{data: *, status: string}>}
 */
async function saveNotifications(notifications) {
  let newNotifications = [];

  const userIds = notifications.reduce((rv, x) => {
    (rv[x['userId']] = rv[x['userId']] || []).push(x);
    return rv;
  }, {});


  let deviceIds = {};

  for (const [key] of Object.entries(userIds)) {
    deviceIds[`${key}`] = await deviceRepository.getDevicesWithEmail(key);
  }


  for (let notification of notifications) {
    const validateData = validator.isValidPayload(notification, notificationSchema);

    if (validateData.err) {
      throw new BadRequestError('Error in data: ' + validateData.err);
    }

    let dataIn = {
      timestamp: new Date().getTime(),
      checkDate: new Date().getTime(),
      header: notification.header,
      referenceId: notification.referenceId,
      isChecked: notification.isChecked === true,
      notificationType: notification.notificationType,
      category: notification.category,
      imageUrl: notification.imageUrl,
      userId: notification.userId,
      body: notification.body,
      group: notification.group,
    };

    //id is based on the user and group s.t. in one group, there can be only 1 card for the user
    let notificationId = `${notification.group}_${notification.userId}`;
    if (notification.body && notification.body.indiHomeNum) {
      notificationId = notificationId.concat('_', notification.body.indiHomeNum);
    }
    const metaData = {index: {_index: esIndexHomeNotifications, _type: 'notifications', _id: notificationId}};
    newNotifications.push(metaData);
    newNotifications.push(dataIn);

    const devices = deviceIds[notification.userId];

    if (devices.status === 404) {
      const msg = new BadRequestError('Error sending push notifications');
      logger.log(ctx, msg, 'No user device found to push notifications');
    }

    let pushNotification = {
      deviceTokens: devices.data,
      message: {
        notification: {
          title: notification.header,
          body: '',
          image: ''
        },
        data: {
          category: notification.category,
          type: notification.notificationType,
          referenceId: notification.referenceId
        },
        android: {
          priority: 'high'
        },
        apns: {
          headers: {
            'apns-priority': '10'
          }
        },
        webpush: {
          headers: {
            Urgency: 'high'
          }
        }
      }
    };

    if (devices.status === 200) {
      const pushStatus = await sendPushNotifications(pushNotification);
      logger.log(ctx, 'Firebase Status', pushStatus.message);
    }

  }
  const resp = await insertDoc.insertBulkDoc(newNotifications);
  if (resp.err) {
    throw new ConflictError('Error in saving data');
  }

  return {
    status: 'success',
    data: resp.data
  };
}

/**
 * Get notifications by type
 * @param {String| null} type - type of the notification
 * @param {String} userId - user Id
 * @param {String| null} indiHomeNum
 * @returns {Promise<BadRequestError|null|undefined>}
 */
const getNotificationsByType = async (type, userId, indiHomeNum = null) => {
  try {
    return await getFilteredUserNotifications(userId, type, indiHomeNum);
  } catch (e) {
    const msg = new BadRequestError('Error loading notifications');
    logger.log(ctx, msg, 'Error loading notifications');

    return msg;
  }
};

/**
 * Use to get notifications from DB by given type
 * if indiHomeNum given-> it will give all related to that account regardless of the user/userId.
 * @param {String} userId - user Id
 * @param {String} notificationType - notification Type
 * @param {String| null} indiHomeNum
 */
async function getFilteredUserNotifications(userId, notificationType, indiHomeNum) {
  const ctx = 'notificationService-getFilteredUserNotifications';

  let body;
  let must = [];
  if (notificationType) {
    must.push(
      {
        match: {
          'notificationType.keyword': notificationType
        }
      });
  }
  if (indiHomeNum) {
    must.push(
      {
        match: {
          'body.indiHomeNum.keyword': indiHomeNum
        }
      });
  } else {
    must.push(
      {
        match: {
          'userId.keyword': userId
        }
      });
  }

  body = {
    from: 0,
    size: config.get('/elasticSearch/size'),
    query: {
      bool: {
        must
      }
    }
  };

  try {
    const resp = await searchDoc(esIndexHomeNotifications, 'notifications', body);
    return resp['hits']['hits'].map((i) => {
      let source = i['_source'];
      source.notificationId = i['_id'];
      return source;
    });

  } catch (e) {
    logger.log(ctx, e, 'Search elastic data');
    return null;
  }
}

/**
 * Update notification status
 * @param {String} notificationId - notification Id
 * @param {Boolean} checked - is notification read, True: checked, False: not checked
 * @returns {Promise<BadRequestError|*>}
 */
const updateNotification = async (notificationId, checked) => {

  const body = {
    'query': {
      'match': {
        '_id': notificationId
      }
    }
  };

  try {
    const resp = await searchDoc(esIndexHomeNotifications, 'notifications', body);

    let result = resp['hits']['hits'];

    let bodyIn = result[0];

    if (bodyIn !== null) {
      bodyIn._source.isChecked = checked;
    }

    const resp2 = await updateDoc(esIndexHomeNotifications, 'notifications', notificationId, bodyIn._source);
    return resp2;
  } catch (e) {
    const msg = new BadRequestError('Error updating data');
    logger.log(ctx, msg, 'Error updating data');
    return msg;
  }
};

async function sendPushNotifications(payload) {
  const [authConfig] = config.get('/basicAuthApi');
  const options = {
    method: 'POST',
    uri: `${config.get('/notificationBaseUrl')}/users/notification/push`,
    auth: {
      user: authConfig.username,
      pass: authConfig.password,
    },
    header: {
      'Content-Type': 'application/json'
    },
    body: payload,
    json: true,
  };
  try {
    const result = await rp.post(options);
    logger.log(ctx, 'response from notification service', result);
    return result;
  } catch (err) {
    return {
      ok: false,
      status: 500,
      message: err.message,
      data: {}
    };
  }
}


const manageUserNotification = async (userId, type, isPicked) => {
  if (!userId) {
    return wrapper.error('Invalid userId', 404);
  }
  try {
    const userData = await ssoService.getSSOUserData(userId);
    if (!userData) {
      return wrapper.error('Invalid userId', 404);
    }
    let userEmail = userData.email;
    const account = await userProfileRepository.manageNotification(userEmail, type, isPicked);
    return account;

  } catch (err) {
    logger.log('error saving notification change', err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }
};


module.exports = {
  saveNotifications, getNotificationsByType, updateNotification, manageUserNotification
};
