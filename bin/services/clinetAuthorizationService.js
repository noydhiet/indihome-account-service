/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 7/11/2019, 6:28 PM
 */


const util = require('util');
const request = require('request-promise');
const logger = require('../helpers/utils/logger');
const ctx = 'AuthorizationService';
const config = require('../config');

class ClientAuthorization {

  constructor() {
    this.token = '';
  }

  async getJWTFForUser() {
    if (this.token) {
      logger.log(ctx,'using the cached gateway access token');
      return this.token;
    }
    logger.log(ctx,'requesting gateway access token');
    this.token = await this._getJWT();
    setTimeout(() => {
      this.token = '';
    }, config.get('/authorization').timeOut);
    return this.token;
  }


  /**
     * Get access token form the cline API using Basic Authentication
     *
     * user String username
     * secret String password
     **/
  async _getJWT() {

    let options = {
      method: 'GET',
      url: config.get('/authorization').host + config.get('/authorization').endpoint,
      headers:
              {
                'Authorization': 'Basic ' + config.get('/authorization').token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
      json: true,
      strictSSL: false
    };

    let resp = await request(options);
    return !util.isNullOrUndefined(resp) && !util.isNullOrUndefined(resp.data) ? resp.data['token'] : null;

  }
}

module.exports = ClientAuthorization;
