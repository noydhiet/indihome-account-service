/*
 * Copyright (c) 2020. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 2/26/20, 2:49 PM
 */

const config = require('../config/index');
const logger = require('../helpers/utils/logger');
const rp = require('request-promise');
const ctx = 'SupportService';

const host = config.get('/supportService').host;
const updatePackageEndpoint = config.get('/supportService').updateScheduleEndpoint;
const timeout = config.get('/supportService').timeout;

let token = config.get('/authorization').token;

module.exports.updateSchedule = async (transactionId, schedule) => {
  const endpoint = `${host}${updatePackageEndpoint}/${transactionId ? `?transactionId=${transactionId}` : ''}`;
  const options = {
    method: 'PUT',
    timeout,
    uri: endpoint,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${token}`,
    },
    json: {schedule},
    strictSSL: false
  };

  const response = await rp.put(options);
  logger.log(ctx, `response from API: for ${JSON.stringify(options)} as ${JSON.stringify(response)}`);

  if (response.ok && response.status === 200) {
    return {
      success: true
    };
  }
  return {
    success: false
  };
};
