/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/11/2019
 */
const joi = require('@hapi/joi');
const _ = require('lodash');
const validator = require('../helpers/utils/validator');
const {BadRequestError} = require('../helpers/error');
const logger = require('../helpers/utils/logger');
const ctx = 'server-checkIndihomeAccount-profile-service';
const UserAPI = require('../modules/user/repositories/commands/domain');
const wrapper = require('../helpers/utils/wrapper');
const profilerepository = require('../repository/profileRepository');
const accountrepository = require('../repository/accountRepository');
const validation = require('../validations');
const config = require('../config');
const ClientAuthorization = require('./clinetAuthorizationService');
const authenticationService = new ClientAuthorization();
const messageService = require('./messageService');
const {ConflictError} = require('../helpers/error');
const request = require('request-promise');
const {isBlacklistedEmail} = require('./userValidationService');
const {Account, Location} = require('../helpers/databases/sequelize');
const reserveFixedLineHost = config.get('/reservation/reserveFixedLineHost');
const reserveInternetHost = config.get('/reservation/reserveInternetHost');
const indihomeIndicator = config.get('/reservation/indihomeIndicator');
const maxAccountLimit = config.get('/reservation/maxAccountLimit');
const isImmediateReservationActive = config.get('/reservation/immediateReservationActive') === 'true';

const insertDoc = require('../helpers/es/insertData');
const searchDoc = require('../helpers/es/searchData');
const updateDoc = require('../helpers/es/updateData');
const esIndexAccountLink = config.get('/elasticIndexes/accountLinkRequest');
const ssoService = require('./ssoService');
const supportService = require('./supportService');
const subscriptionService = require('./subscriptionService');

/**
 * Object model for User Profile
 */
const userSchema = joi.object({
  email: joi.string().required(),
  name: joi.string().required(),
  password: joi.string().required(),
  mobile: joi.string().required(),
  userRole: joi.string().required(),
  status: joi.string().required(),
});

const saveAndSignUp = async (payload) => {
  const validateData = validator.isValidPayload(payload, userSchema);

  if(validateData.err) {
    const msg = new BadRequestError('Error in data');
    logger.log(ctx, msg,validateData.err);
    return {
      ok:false,
      code:403,
      status:403,
      message:'Error in request body',
      data: {}
    };
  }

  if (isBlacklistedEmail(payload.email)) {
    return {
      ok: false,
      message: 'blacklisted email',
      status: 403,
      code: 403,
      data: {}
    };
  }

  this.userAPI = new UserAPI();

  let profileData = {
    name:payload.name,
    primaryPhone:payload.mobile,
    secondaryPhone:'',
    email:payload.email,
    dob: '',
    birthPlace:'',
    country:'indonesia',
    gender: '',
    nationality:'indonesian'
  };

  try{

    // saving to identity server
    const userStatusIs = await this.userAPI.userSiginUp(payload);

    // eslint-disable-next-line no-empty
    if(userStatusIs.status != 201){
      return userStatusIs;

      //TODO if user is in identity server and not in profile service  have to handle it

    }

    // check users in the profile service
    const emailCheck = await profilerepository.getUserProfileByEmail(profileData.email);

    if(emailCheck.status == 200){
      return userStatusIs;
    }

    // saving to the Profile service
    const userData = await profilerepository.saveUser(profileData);

    if(userData.status == 200){
      return userStatusIs;
    }
    if(userData.status == 406){
      return {
        ok:false,
        message: userData.message,
        status:410,
        code:410,
        data:{ }
      };
    }
    if(userData.status == 500){
      return {
        ok:false,
        message:'server error',
        status:500,
        code:500,
        data:{ }
      };
    }
  }catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err,500);
  }
};

const getUserProfileByEmail = async(userId) => {
  if (!userId) {
    throw new BadRequestError('Invalid userId');
  }

  try {
    const userData = await ssoService.getSSOUserData(userId);
    if (!userData) {
      throw new BadRequestError('Invalid userId');
    }
    let email = userData.email;

    const include = [{model: Account, as: 'accounts', include: [{model: Location}]}];
    let profile =  await profilerepository.getUserProfileByEmail(email, include);
    let prc = await generteProfileComplition(profile);
    profile.data.profieSummary = prc;
    return profile;

  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};

const generteProfileComplition = async(profile) => {
  let reason = {
    isProfileCompleted:false,
    isEmailVerified:false,
    isPackageActive:false,
    percentage: 0
  };
  let percentage = 0; 

  if(profile.data.name && profile.data.dob && profile.data.gender ){
    percentage = percentage + 50;
    reason.isProfileCompleted = true 
  }
  if(profile.data.emailVarified){
    percentage = percentage + 10;
    reason.isEmailVerified = true; 
  }
  if(profile.data.accounts.length > 0 ){
      let activeAmount = 0;
    let accounts = profile.data.accounts;
    accounts.forEach((element) => {
      if(element.active){
        activeAmount = 40;
        reason.isPackageActive = true; 
      }
    });
    percentage = percentage + activeAmount;
  }
  reason.percentage = percentage;
  return reason;
}

const editUserProfile = async(payload,userId) => {
  if (!userId) {
    throw new BadRequestError('Invalid userId');
  }

  try {
    const userData = await ssoService.getSSOUserData(userId);
    if (!userData) {
      throw new BadRequestError('Invalid userId');
    }
    let email = userData.email;
    return await profilerepository.updateUserProfile(payload, email);
  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};

/**
 * Get user account by indoHome number
 * @param {String} indiHomeNum - indiHome Number
 * @returns {Promise<{err: *, data: *}|{code: number, data: {}, ok: boolean, status: string}|U>}
 */
const getAccountByIndiHomeNum = async(indiHomeNum) => {
  if (!indiHomeNum) {
    throw new BadRequestError('Invalid indiHomeNum');
  }

  try {
    return await profilerepository.getAccountByIndiHomeNum(indiHomeNum);
  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};

const getUserProfileById = async (userId, include) => {
  if (!userId) {
    throw new BadRequestError('Invalid email');
  }

  try {
    return await profilerepository.getUserProfileById(userId, include);
  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};

const changeUserEmail = async (payload, userId) => {
  const validateData = validator.isValidPayload(payload, validation.changeEmailRequestJoi);


  if (!userId) {
    throw new BadRequestError('Invalid userId');
  }

  if (validateData.err) {
    const msg = new BadRequestError('Error in data');
    logger.log(ctx, msg, validateData.err);
    return {
      ok: false,
      code: 403,
      status: 'Error in data',
      data: {}
    };
  }

  try{

    const userData = await ssoService.getSSOUserData(userId);
    if (!userData) {
      throw new BadRequestError('Invalid userId');
    }
    let currentUserMail = userData.email;

    this.userAPI = new UserAPI();
    // updating user email in identity server
    const user = await this.userAPI.changeUserEmail(payload);
    if(user.status != 200){
      return {
        ok:true,
        message: user.message,
        status:402,
        code:402,
        data:{ }
      };
    }

    return await profilerepository.changeUserEmail(payload, currentUserMail);
  }catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};

const changeUserMobile = async(payload, userId) => {

  if (!userId) {
    throw new BadRequestError('Invalid userId');
  }
  const validateData = validator.isValidPayload(payload, validation.changeMobileRequestJoi);

  if(validateData.err) {
    const msg = new BadRequestError('Error in data');
    logger.log(ctx, msg,validateData.err);
    return {
      ok:false,
      code:403,
      status:403,
      message: 'Error in data',
      data: {}
    };
  }

  try{

    const userData = await ssoService.getSSOUserData(userId);
    if (!userData) {
      throw new BadRequestError('Invalid userId');
    }
    let currentUserMail = userData.email;


    this.userAPI = new UserAPI();
    // updating user email in identity server
    const user = await this.userAPI.changeUserMobile(payload);
    if (user.status != 200) {
      return {
        ok: true,
        message: user.message,
        status: 402,
        code:402,
        data: {}
      };
    }

    return await profilerepository.changeUserMobile(payload, currentUserMail);
  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};

function getRevocableReservation() {
  return {
    fixedLine: {},
    internetLine: {}
  };
}

/**
 * create Account, add location to the User and reserve numbers based on the payload.userEmail for the user
 * @param payload
 * @returns {Promise<*>}
 */
const createAccountForUserLocation = async payload => {

  //mark the location as PT1 level
  payload.level = 'PT1';
  logger.log(ctx, `adding user location : ${payload}`);
  //get user
  let savedUser = await profilerepository.getUserProfileByEmail(payload.userEmail, ['accounts', 'locations']);

  if (savedUser && savedUser.data && savedUser.data.accounts &&
    savedUser.data.accounts.filter(a => a.dataValues.parent === 'self').length >= maxAccountLimit) {
    return {
      ok: false,
      message: 'user reached the max account limit',
      status: 4010,
      data: {
        accounts: savedUser.data.accounts
      }
    };
  }
  //  if not confirmed account exists, update the location and account info
  if (savedUser && savedUser.data && savedUser.data.name) {
    //get number reservations form APIs
    const userName = savedUser.data.name;
    let reservation;
    if (isImmediateReservationActive) {
      reservation = await initNumberReservations(userName, payload.sto, payload.deviceId);
    } else {
      reservation = getRevocableReservation();
    }
    getRevocableReservation();
    if (reservation && reservation.fixedLine && reservation.internetLine) {

      let userLocation;
      const inactiveAccount = savedUser.data.accounts.filter(ac => ac.dataValues).find(ac => !ac.dataValues.active);
      if (inactiveAccount) {
        userLocation = await profilerepository.updateUserLocation(savedUser.data, payload, reservation.fixedLine,
          reservation.internetLine, inactiveAccount.dataValues);
      } else {

        //add location and account data respective to the user
        userLocation = await profilerepository.addUserLocation(savedUser.data, payload, reservation.fixedLine, reservation.internetLine);
      }
      return {
        ok: true,
        message: 'location added',
        status: 201,
        data: {
          userLocation: userLocation.data,
          ...reservation
        }
      };
    }
  }

  throw new ConflictError('location adding failed');
};

/**
 * fixme: implement with default values for account
 * create Revocable Account, add location to the User and reserve numbers based on the payload.userEmail for the user
 * @param payload location information
 * @param payload.userEmail
 * @returns {Promise<*>}
 */
const createRevocableAccountForUserLocation = async payload => {
  //mark the location as PT2 level
  payload.level = 'PT2';
  logger.log(ctx, `adding revocable user location : ${JSON.stringify(payload)}`);
  //get user
  let savedUser = await profilerepository.getUserProfileByEmail(payload.userEmail, ['accounts', 'locations']);

  if (savedUser && savedUser.data && savedUser.data.accounts &&
    savedUser.data.accounts.filter(a => a.dataValues.parent === 'self').length >= maxAccountLimit) {
    return {
      ok: false,
      message: 'user reached the max account limit',
      status: 4010,
      data: {
        accounts: savedUser.data.accounts
      }
    };
  }
  //  if not confirmed account exists, update the location and account info
  if (savedUser && savedUser.data && savedUser.data.name) {

    const reservation = getRevocableReservation();
    let userLocation;
    const inactiveAccount = savedUser.data.accounts.filter(ac => ac.dataValues).find(ac => !ac.dataValues.active);
    if (inactiveAccount) {
      userLocation = await profilerepository.updateUserLocation(savedUser.data, payload, reservation.fixedLine, reservation.internetLine,
        inactiveAccount.dataValues);
    } else {
      //add location and account data respective to the user
      userLocation = await profilerepository.addUserLocation(savedUser.data, payload, reservation.fixedLine, reservation.internetLine);
    }
    return {
      ok: true,
      message: 'location added',
      status: 201,
      data: {
        userLocation: userLocation.data,
        ...reservation
      }
    };
  }

  throw new ConflictError('location adding failed');
};

const refreshAccountNumbers = async (payload) => {
  if (!payload.email) {
    return {
      ok: true,
      message: 'invalid email',
      status: 400,
      data: {
        payload,
      }
    };
  }
  let savedUser = await profilerepository.getUserProfileByEmail(payload.email, [{model: Account, as: 'accounts', include: [{model: Location}]}]);
  //  if not confirmed account exists, update the location and account info
  if (savedUser && savedUser.data && savedUser.data.name) {

    let updatedResponse;
    let updatedAccount;
    const inactiveAccount = savedUser.data.accounts.filter(ac => ac.dataValues).find(ac => !ac.dataValues.active);
    if (inactiveAccount) {
      updatedAccount = inactiveAccount.dataValues;
      const sto = updatedAccount.Location.dataValues.sto;
      const deviceId = updatedAccount.Location.dataValues.deviceId;
      if (!sto || !deviceId) {
        return {
          ok: true,
          message: 'invalid data in account',
          status: 400,
          data: {
            sto,
            deviceId,
            payload,
            account: inactiveAccount
          }
        };
      }

      //new reservation based on existing or updated location values
      const reservation = await initNumberReservations(savedUser.data.name, sto, deviceId);

      updatedAccount.internetNum = reservation.internetLine.internetNumber;
      updatedAccount.indiHomeNum = reservation.fixedLine.telephone || reservation.internetLine.internetNumber;
      updatedAccount.fixedPhoneNum = reservation.fixedLine.telephone;
      updatedAccount.ncli = reservation.fixedLine.ncli;
      updatedAccount.email = savedUser.data.email;
      updatedAccount.transaction = reservation.fixedLine.transaction;
      updatedAccount.reservationId = reservation.internetLine.reservationId;
      updatedAccount.reservationPort = reservation.fixedLine.reservationPort;

      updatedAccount.level = 'PT1';
      updatedResponse = await accountrepository.updateAccount(updatedAccount.id, updatedAccount);
    }
    return {
      ok: true,
      message: 'account updated',
      status: 200,
      data: {
        updatedResponse,
        updatedAccount
      }
    };
  }
  throw new ConflictError(`update reservations failed, user not found for user : ${payload.email}`);
};


/**
 * reserve numbers for the user
 * @param userName
 * @param sto
 * @param deviceId
 * @returns {Promise<{fixedLine: {telephone: *, transaction: *}, internetLine: {reservationId: *, internetNumber: *}}>}
 */
async function initNumberReservations(userName, sto, deviceId) {
  const [fixedLine, internetLine] = await Promise
    .all([reserveFixedLine(deviceId, sto), reserveInternetNumber(userName, sto)]);

  return {
    fixedLine,
    internetLine
  };
}

/**
 * reserve Telephone
 * @param deviceId
 * @param sto
 * @returns {Promise<{telephone: *, transaction: *}>}
 */
async function reserveFixedLine(deviceId, sto) {

  const authorization = `Bearer ${await authenticationService.getJWTFForUser()}`;
  const reqBody = {
    deviceId,
    sto,
    indihomeIndicator: indihomeIndicator
  };

  const options = {
    method: 'POST',
    uri: reserveFixedLineHost,
    headers: {
      'Content-Type': 'application/json',
      Authorization: authorization,
    },
    json: reqBody,
    strictSSL: false
  };

  logger.log(ctx, `sending data to reserve telephone API on: ${new Date()} : ${JSON.stringify(options)}`);
  const result = await request.post(options);
  logger.log(ctx, `got data from reserve telephone API on: ${new Date()} : ${JSON.stringify(result)}`);

  if (result.statusCode === '0' && result.data && result.data['telephone'] && result.data['NCLI'] && result.data['reservationPort'] !== '0' &&
    result.data['reservationTN']) {
    return {
      telephone: result.data['telephone'],
      transaction: result.data['reservationTN'],
      ncli: result.data['NCLI'],
      reservationPort: result.data['reservationPort']
    };
  }

  throw new ConflictError('ODP not available');
}

/**
 * reserve Internet
 * @param {String} name
 * @param {String} sto
 */
async function reserveInternetNumber(name, sto) {
  const authorization = `Bearer ${await authenticationService.getJWTFForUser()}`;
  const reqBody = {
    STO: sto,
    customerName: name
  };

  const options = {
    method: 'POST',
    uri: reserveInternetHost,
    headers: {
      'Content-Type': 'application/json',
      Authorization: authorization,
    },
    json: reqBody,
    strictSSL: false
  };

  try {
    logger.log(ctx, `sending data to reserve Internet API on: ${new Date()} : ${JSON.stringify(options)}`);
    const result = await request.post(options);
    logger.log(ctx, `got data from reserve Internet API on: ${new Date()}: ${JSON.stringify(result)}`);
    if (result.statusCode === '0' && result.data && result.data['Internet_Number'] && result.data['reservationID']) {
      return {
        internetNumber: result.data['Internet_Number'],
        reservationId: result.data['reservationID']
      };
    }
  } catch (error) {
    logger.log(ctx, 'error', error);
    throw new ConflictError(`unable to reserve number ${JSON.stringify(error)}`);
  }
  throw new ConflictError('unable to reserve number');
}

/**
 * get location to the User account
 * @param userId
 * @param accountId
 * @returns {Promise<{location: *, user: ({accounts}|{}), account: *}>}
 */
const getUserLocations = async (userId, accountId) => {

  if (!userId) {
    return {
      ok: false,
      code: 403,
      status: 403,
      message: 'Error in data',
      data: {}
    };
  }
  let userLocation = await profilerepository.getUserLocationsForAccount(userId, accountId);

  let locations;
  if (userLocation.data && userLocation.data.accounts && userLocation.data.accounts[0] && userLocation.data.accounts[0].dataValues) {
    locations = userLocation.data.accounts.filter(ac => ac.dataValues.Location).map(ac => ac.dataValues.Location.dataValues);
  }
  if (userLocation.data && userLocation.data.accounts && userLocation.data.accounts[0]) {
    return {
      ok: true,
      code: 200,
      status: 200,
      message: 'locations found',
      data: {
        accounts: userLocation.data.accounts.map(ac => ac.dataValues),
        locations
      }
    };
  }
  throw new ConflictError('locations not found');
};

/**
 * delete User account
 * @param userId
 * @returns {Promise<{err: *, data: *}>}
 */
const deleteUser = async (email) => {

  if (!email) {
    throw new BadRequestError('Invalid user email');
  }

  const options = {
    method: 'DELETE',
    url: `${config.get('/identityBaseUrl')}/user/${email}`,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + config.get('/authorization').token,
    },
    json: true,
    strictSSL: false
  };

  const result = await request.delete(options);
  if (result.status !== 200) {
    return {
      ok: true,
      message: result.message,
      status: 402,
      code: 402,
      data: {}
    };
  }

  return await profilerepository.deleteUser(email);
};


const verifyVerification = async (payload, userId) => {
  const validateData = validator.isValidPayload(payload, validation.verifyVerificationCodeJoi);

  if (!userId) {
    throw new BadRequestError('Invalid userId');
  }

  if (validateData.err) {
    const msg = new BadRequestError('Error in data');
    logger.log(ctx, msg, validateData.err);
    return {
      ok: false,
      code: 403,
      status: 'Error in data',
      data: {}
    };
  }

  try{

    const userData = await ssoService.getSSOUserData(userId);
    if (!userData) {
      throw new BadRequestError('Invalid userId');
    }
    let currentUserMail = userData.email;


    this.userAPI = new UserAPI();
    // updating check verfication code
    let identityPaylod = {
      channel: 'email',
      value: currentUserMail,
      verifyCode: payload.verifyCode
    };
    const user = await this.userAPI.verifyVerificationCode(identityPaylod);
    if (user.status != 200) {
      return {
        ok: true,
        message: user.message,
        status: user.status,
        code: user.status,
        data: {}
      };
    }

    return await profilerepository.verifyUserEmail(payload, currentUserMail);
  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};


/**
 * Get user accounts by indoHome number nad available options
 * @param {String} indiHomeNum - indiHome Number
 * @returns {Promise<{err: *, data: *}|{code: number, data: {}, ok: boolean, status: string}|U>}
 */
const getAccountsByIndiHomeNum = async (indiHomeNum, userEmailIn) => {
  if (!indiHomeNum) {
    throw new BadRequestError('Invalid indiHomeNum');
  }
  if (!userEmailIn) {
    throw new BadRequestError('Invalid email address');
  }
  try {
    let accountCheck = await verifyIndiHomeNumber(indiHomeNum);
    if(!accountCheck.ok){
      return accountCheck;
    }
    let payloadIn = accountCheck.data.payload.data;
    try {
      let accounts = await profilerepository.getParentAccountsByIndiHomeNum(indiHomeNum);
      let childAccounts = await profilerepository.getAccountsEmailandId(indiHomeNum,userEmailIn);

      if(accounts.status !== 200){
        return {
          ok: false,
          message: 'Service Error',
          status: 406,
          code: 200,
          data: {}
        };
      }
      //to check if account is already linked
      if (childAccounts) {
        if(!childAccounts.isConnected){
          let accountReconnect = await accountrepository.connectDisconnectAccount(indiHomeNum,childAccounts.email,true);
          if(!accountReconnect.ok){
            return {
              ok: false,
              message: 'Service Error',
              status: 406,
              code: 200,
              data: {}
            };
          }
          return {
            ok: false,
            message: 'account reconnected',
            status: 203,
            code: 200,
            data: {}
          };

        }else{

          return {
            ok: false,
            message: 'Account already linked to same user',
            status: 408,
            code: 200,
            data: {}
          };

        }
      }

      if (accounts.data.length > 0) {

        this.userAPI = new UserAPI();
        let userEmail = accounts.data[0].email;
        // get SSO data for the notifications
        const user = await this.userAPI.getSSOUser(userEmail);
        let mobileNumber = user.data.mobile;
        let ssoId = user.data._id;

        if (user.status == 200) {
          return {
            ok: false,
            message: 'Parent user found for the indihome number',
            status: 204,
            code: 200,
            data: {
              indiHomeNumber: indiHomeNum,
              parentEmail: userEmail,
              parentMobile: mobileNumber,
              parentSsoId: ssoId
            }
          };
        }

        logger.log(ctx, user.message, 'Service error');
        return {
          ok: false,
          message: 'Service Error',
          status: 406,
          code: 200,
          data: {}
        };

      }
      let getUserProfile = await profilerepository.getUserProfileByEmail(userEmailIn);
      if (!getUserProfile.ok) {
        return {
          ok: false,
          message:'Parent user found for the indihome number',
          status: getUserProfile.status,
          code: 200,
          data: {}
        };
      }

      if (getUserProfile.data.accounts && getUserProfile.data.accounts.length > 0) {
        const arraySparse = getUserProfile.data.accounts;
        let numCallbackRuns = 0
        arraySparse.forEach((element) => {
          if(element.parent=='self' && active && isConnected ){
            numCallbackRuns++
          }
        })
        if(numCallbackRuns >= 3){
          return {
            ok: false,
            message:'User already have maximun number of primary accounts',
            status: 407,
            code: 200,
            data: {}
          };
        }
      }

      let location = {
        UserId:getUserProfile.data.id,
        addressDesc:payloadIn.ALAMAT,
      };
      let setPackage = {
        indiHomeNum:indiHomeNum,
        packageCode:payloadIn.PAKET
      };
      let accountDetails = {
        indiHomeNum: indiHomeNum,
        email: userEmailIn,
        UserId: getUserProfile.data.id,
        Location:location,
        packages:setPackage,
        activatedDate: new Date(),
        parent: 'self',
        active:true
      };

      if (payloadIn.NCLI && payloadIn.WITEL && payloadIn.ND_INET && payloadIn.ND_POTS) {
        accountDetails.internetNum = payloadIn.ND_INET,
        accountDetails.fixedPhoneNum = payloadIn.ND_POTS,
        accountDetails.ncli = payloadIn.NCLI,
        accountDetails.witel = payloadIn.WITEL;
      }
      //Saving an Account to the user
      let userAccount = await accountrepository.saveAccountforUser(accountDetails);
      if (!userAccount.ok) {
        return {
          ok: false,
          message: userAccount.message,
          status: 406,
          code: 200,
          data: {}
        };

      }
      return {
        ok: true,
        message: 'Indihome number added to the user',
        status: 200,
        code: 200,
        data: {}
      };

    } catch (err) {
      logger.log(ctx, err, 'API connection error');
      return wrapper.formatData(err, 500);
    }
  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};


/**
 * This method is to send account requests
 * @param {String} payload - indiHomeNumber and user data
 * @returns {Promise<{err: *, data: *}|{code: number, data: {}, ok: boolean, status: string}|U>}
 */
const sendAccountLinkRequest = async (indiHomeNumber, email, parentSsoId) => {

  try {
    let parentUser = await profilerepository.getParentAccountsByIndiHomeNum(indiHomeNumber);
    try {
      if (parentUser.data.length > 0) {

        let parentId = parentUser.data[0].id;
        let childAccounts = await profilerepository.getChildAccountsByIndiHomeNum(indiHomeNumber, parentId);

        if (childAccounts.length >= 4) {
          return {
            ok: false,
            message: 'This acount already reached maximun number of users',
            status:407,
            code:407,
            data: {}
          };
        }

        // Save and send notification

        //to get client ID,to handle in case the email got changed.
        let childProfile = await profilerepository.getUserProfileByEmail(email);

        if (!childProfile.ok) {
          return {
            ok: false,
            message: 'Error fetching child account',
            status: 406,
            code: 406,
            data: {}
          };

        }

        // This is the request record of account log.
        let newNotifications = [];
        const metaData = {'index': {'_index': esIndexAccountLink, '_type': 'requests'}};

        let accountRequest = {
          indiHomeNumber: indiHomeNumber,
          parentAccountId: parentId,
          parentSsoId: parentSsoId,
          childProfileId: childProfile.data.id,
          childProfileEmail: email,
          dateResponded:null,
          status: 'PENDING'
        };
        newNotifications.push(metaData);
        newNotifications.push(accountRequest);

        const resp = await insertDoc.insertBulkDoc(newNotifications);

        if (resp.err) {
          return {
            ok: false,
            message: 'Error saving data',
            status: 406,
            code: 406,
            data: {}
          };
        }

        let accountRequestId = resp.items[0].index._id;
        //Sending notification
        //TODO Handle Language
        //Success Notification to parent
        let parentMessage = 'Access requested to IndiHome account ' + indiHomeNumber;
        let successNotificationParent = await sendRequestNotifications(accountRequestId, parentMessage, parentSsoId, 'ACCOUNT_LINK_REQUEST');

        if (successNotificationParent.ok) {
          return {
            ok: true,
            message: 'Success sending the account link request',
            status: 200,
            code: 200,
            data: {}
          };
        }
        return {
          ok: false,
          message: successNotificationParent.message,
          status: 406,
          code: 406,
          data: {}
        };

      }

      return {
        ok: false,
        message: 'invalid parent user',
        status: 404,
        code: 404,
        data: {}
      };
    } catch (err) {
      logger.log(ctx, err, 'API connection error');
      return wrapper.formatData(err, 500);
    }
  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};


// get account requests for a referance number

const getAccountRequest = async (referenceId) => {

  const body = {
    'query': {
      'match': {
        '_id': referenceId
      }

    }
  };

  try {
    const resp = await searchDoc(esIndexAccountLink, 'requests', body);

    let result = resp['hits']['hits'];

    let bodyIn = result[0];
    let indihomeNumber = '';
    let childProfileNumber = '';
    let status ='PENDING'
    let dateResponded = null;

    if (bodyIn !== null) {

      indihomeNumber = bodyIn._source.indiHomeNumber;
      childProfileNumber = bodyIn._source.childProfileId;
      status = bodyIn._source.status;
      dateResponded = bodyIn._source.dateResponded;
      

      //get child profile by ID
      let childProfile = await profilerepository.getUserProfileById(childProfileNumber);

      if (!childProfile) {
        return {
          ok: false,
          message: 'cannot find the child account',
          status: 4004,
          code: 4004,
          data: {}
        };
      }

      let requestData = {
        indiHomeNumber: indihomeNumber,
        childName: childProfile.name,
        childEmail: childProfile.email,
        childId: childProfile.id,
        childMobile: childProfile.primaryPhone,
        referenceId: referenceId,
        status:status,
        dateResponded:dateResponded
      };

      return {
        ok: true,
        message: 'Success fetching the request',
        status: 200,
        code: 200,
        data: requestData
      };
    }

  } catch (e) {
    const msg = new BadRequestError('Error saving data');
    logger.log(ctx, msg, 'Error saving data');
    return msg;
  }
};

const getAccountRequestRecords = async (referenceId) => {
  const body = {
    'query': {
      'match': {
        '_id': referenceId
      }

    }
  };

  try {
    const resp = await searchDoc(esIndexAccountLink, 'requests', body);

    let result = resp['hits']['hits'];
    let bodyIn = result[0];

    if (bodyIn !== null) {
      return bodyIn._source;
    }
    return null;

  } catch (e) {
    const msg = new BadRequestError('Error saving data');
    logger.log(ctx, msg, 'Error saving data');
    return msg;
  }
};


const updateRequest = async (referenceId, isAccepted) => {

  const body = {
    'query': {
      'match': {
        '_id': referenceId
      }

    }
  };

  try {
    const resp = await searchDoc(esIndexAccountLink, 'requests', body);

    let result = resp['hits']['hits'];

    let bodyIn = result[0];

    if (bodyIn !== null) {

      if (isAccepted) {
        bodyIn._source.status = 'ACCEPTED';
      } else {
        bodyIn._source.status = 'REJECTED';
      }
      bodyIn._source.dateResponded = new Date().getTime();
    }

    const resp2 = await updateDoc(esIndexAccountLink, 'requests', referenceId, bodyIn._source);
    return resp2;
  } catch (e) {
    const msg = new BadRequestError('Error saving data');
    logger.log(ctx, msg, 'Error saving data');
    return null;
  }
};


const accountLinkResponse = async (referenceId, isAccepted) => {

  //get the request records
  let request = await getAccountRequestRecords(referenceId);

  if (!request) {
    return {
      ok: false,
      message: 'Invalid reference Number',
      status: 406,
      code: 406,
      data: {}
    };

  }

  //get parent account to create the child account
  let parentAccount = await accountrepository.getAccountById(request.parentAccountId);
  //get child profile
  let childProfile = await profilerepository.getUserProfileById(request.childProfileId);

  if (!parentAccount.dataValues) {
    return {
      ok: false,
      message: 'cannot find the parent account',
      status: 404,
      code: 404,
      data: {}
    };
  }
  let parentData = parentAccount.dataValues;
  let parentLocation = parentAccount.Location;
  let parentPackages = parentAccount.packages

  if (!childProfile) {
    return {
      ok: false,
      message: 'cannot find the child account',
      status: 406,
      code: 406,
      data: {}
    };
  }
  // get SSO data for the notifications
  this.userAPI = new UserAPI();
  const user = await this.userAPI.getSSOUser(childProfile.email);
  let childSsoId = user.data._id;
  let childName = user.data.name;

  if (isAccepted) {

    //TODO add the location and package data


    let location = {
      addressDesc:parentLocation.addressDesc,
      areaCode:parentLocation.areaCode,
      block:parentLocation.block,
      blockNumber:parentLocation.blockNumber,
      cableName:parentLocation.cableName,
      city:parentLocation.city,
      cityCode:parentLocation.cityCode,
      district:parentLocation.district,
      districtCode:parentLocation.districtCode,
      floor:parentLocation.floor,
      id:parentLocation.id,
      isNewRecord:parentLocation.isNewRecord,
      latitude:parentLocation.latitude,
      level:parentLocation.level,
      locId:parentLocation.locId,
      longitude:parentLocation.longitude,
      postalCode:parentLocation.postalCode,
      primary:parentLocation.primary,
      UserId:childProfile.id
    }

    let packages = []
    parentPackages.forEach((element )=> {
      let setPackage = {
        indiHomeNum:parentData.indiHomeNum,
        packageCode:element.dataValues.packageCode
      };
      packages.push(setPackage);
    });

    let accountDetails = {
      indiHomeNum: parentData.indiHomeNum,
      email: childProfile.email,
      UserId: request.childProfileId,
      parent: parentData.id,
      internetNum: parentData.internetNum,
      fixedPhoneNum: parentData.fixedPhoneNum,
      Location:location,
      packages:packages,
      ncli: parentData.ncli,
      witel: parentData.witel,
      activatedDate: new Date().getTime(),
      active:true
    };

    //Saving an Account to the user
    let userAccount = await accountrepository.saveAccountforUser(accountDetails);


    if (!userAccount.ok) {
      return {
        ok: false,
        message: userAccount.message,
        status: userAccount.status,
        code: userAccount.code,
        data: {}
      };

    }

    //update the request records
    let updateReq = await updateRequest(referenceId, true);

    if (!updateReq) {
      return {
        ok: false,
        message: 'Error updating the status',
        status: 406,
        code: 406,
        data: {}
      };

    }


    //Success Notification to Child

    let childMessage = 'Your requst has been approved to manage account ' + request.indiHomeNumber;
    let successNotificationChild = await sendRequestNotifications(referenceId, childMessage, childSsoId, 'ACCOUNT_LINK_REQUEST_APPROVED');

    //Success Notification to parent
    let parentMessage = 'You have approved to manage account request from ' + childName;
    let successNotificationParent = await sendRequestNotifications(referenceId, parentMessage, request.parentSsoId, 'ACCOUNT_LINK_RESPONSE_APPROVED');


    return {
      ok: true,
      message: 'Indihome number added to the user',
      status: 201,
      code: 201,
      data: {}
    };

  }

  //Success Notification to Child

  let childMessage = 'Your requst has been rejected to manage account ' + request.indiHomeNumber;
  let successNotificationChild = await sendRequestNotifications(referenceId, childMessage, childSsoId, 'ACCOUNT_LINK_REQUEST_REJECTED');

  //Success Notification to parent
  let parentMessage = 'You have rejected the Manage account request from ' + childName;
  let successNotificationParent = await sendRequestNotifications(referenceId, parentMessage, request.parentSsoId, 'ACCOUNT_LINK_RESPONSE_REJECTED');

  //update the request records
  let updateReq = await updateRequest(referenceId, false);

  if (!updateReq) {
    return {
      ok: false,
      message: 'Error updating the status',
      status: 406,
      code: 406,
      data: {}
    };

  }

  return {
    ok: true,
    message: 'successfully rejected',
    status: 200,
    code: 200,
    data: {}
  };


};


const sendRequestNotifications = async (referenceId, message, ssoId, type) => {

  let inboxObject = [{
    header: message,
    referenceId: referenceId,
    isChecked: false,
    messageType:'personal',
    messageSubType: type,
    imageUrl: '',
    userId: ssoId
  }];
  return await messageService.saveMessages(inboxObject);
};


const verifyIndiHomeNumber = async (indihomeNumber) => {
  try{
    this.userAPI = new UserAPI();
    // updating check verfication code
    let portPaylod = {
      'ND':indihomeNumber
    };
    const user = await this.userAPI.checkIndiHomeNumber(portPaylod);
    if(user.ok){
      return {
        ok:true,
        message:'Vaild indihome Number',
        status:200,
        code:200,
        data:{
          payload:user
        }
      };
    }

    if(user.status == 500){
      return {
        ok:false,
        message:'Error with core api',
        status:500,
        code:500,
        data:{}
      };
    }

    return {
      ok: false,
      message: 'Invalid indihome Number',
      status: 404,
      code: 200,
      data: {}
    };

  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};

const verifyAccountId = async (accountId) => {
  try{
    const account  = await accountrepository.getAccountById(accountId);
    return {
      ok: account ? true : false,
      message: account ? 'valid portfolio id' : 'invalid portfolio id',
      status:200,
      code: account ? 200 : 404,
      data: {}
    };
  } catch (err) {
    logger.log(ctx, err, 'API connection error');
    return wrapper.formatData(err, 500);
  }
};

/**
 *
 * @param userInfo.transactionId
 * @param userInfo.profile
 * @param userInfo.account
 * @param userInfo.location
 * @param userInfo.schedule
 * @param userInfo.packageInfo
 * @param userInfo.packageInfo.price
 * @param userInfo.schedule.crewId
 * @param userInfo.schedule.bookingId
 * @param userInfo.schedule.scheduledTime
 * @returns {Promise<*>}
 */
const updateUserInformation = async (userInfo) => {
  const {transactionId, profile, account, location, schedule, packageInfo} = userInfo;

  if (_.isEmpty(transactionId)) {
    return {
      ok: false,
      message: 'Invalid transaction id',
      status: 4000,
      data: {}
    };
  }

  let updates = [];

  if (!_.isEmpty(profile) || !_.isEmpty(account) || !_.isEmpty(location)) {
    updates.push(profilerepository.updateUserInformation(transactionId, profile, account, location));
  }

  if (!_.isEmpty(schedule)) {
    updates.push(supportService.updateSchedule(transactionId, schedule));
  }

  if (!_.isEmpty(packageInfo)) {
    updates.push(subscriptionService.updateSelectedPackage(transactionId, packageInfo));
  }

  return await Promise.all(updates)
    .then(value => {
      return {
        ok: true,
        message: 'updated',
        status: 200,
        data: {...value}
      };
    })
    .catch(reason => {
      logger.log(ctx, reason, 'update error');
      return {
        ok: false,
        message: `system error: ${reason.message}`,
        status: 500,
        data: {}
      };
    });
};

module.exports = {
  saveAndSignUp,
  getAccountByIndiHomeNum,
  getAccountsByIndiHomeNum,
  verifyAccountId,
  getUserProfileByEmail,
  changeUserEmail,
  changeUserMobile,
  createAccountForUserLocation,
  createRevocableAccountForUserLocation,
  refreshAccountNumbers,
  getUserProfileById,
  getUserLocations,
  deleteUser,
  verifyVerification,
  editUserProfile,
  getAccountRequest,
  sendAccountLinkRequest,
  accountLinkResponse,
  verifyIndiHomeNumber,
  updateUserInformation
};
