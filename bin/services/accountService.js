/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Sajidh Zazahir on 2/01/2020, 6:28 PM
 */
const logger = require('../helpers/utils/logger');
const rp = require('request-promise');
const _ = require('lodash');
const util = require('util');
const userAccountRepository = require('../repository/accountRepository');
const profileRepository = require('../repository/profileRepository');
const KtpRepository = require('../repository/es/ktpRepository');
const ktpRepository = new KtpRepository();
const SvmCallRepository = require('../repository/es/svmCallRepository');
const svmCallRepository = new SvmCallRepository();
const {ACCOUNT_STATUS, REDIS_KEYS} = require('../helpers/utils/constants/profileConstants');
const wrapper = require('../helpers/utils/wrapper');
const redis = require('redis');
const crypto = require('crypto');
const config = require('../config');
const dateHelper = require('../helpers/utils/dateHelper');
const {promisify} = require('util');
const ssoService = require('../services/ssoService');
const redisCache = config.get('/redisCache');
const coreApiService = require('../services/coreAPIService');
const notificationService = require('../services/notificationService');
const {
  NOTIFICATION_CATEGORIES, NOTIFICATION_TYPES, SVM_LEVEL, KYC_STATUS, NOTIFICATION_GROUPS
} = require('../helpers/utils/constants/accountServiceConstants');
const {BadRequestError} = require('../helpers/error');
const ctx = 'server-checkIndihomeAccount-account-service';

let cache = redis.createClient(redisCache.port, redisCache.host, {
  auth_pass: redisCache.password
}).on('error', err => logger.log('redis auth failed', err, 'cache error'));
cache.getAsync = promisify(cache.get).bind(cache);
cache.on('connect', () => {
  logger.log('check cache enabled', '', 'cache connected');
});
cache.on('error', err => {
  logger.log('check cache enabled', err, 'cache error');
});
const {ConflictError, NotFoundError} = require('../helpers/error');

/**
 * Get rewards active status
 * @param {Object} payload - {indiHomeNum}
 * @returns {Promise<{err: *, data: null}|{err: null, data: *}>}
 */
const getRewardsActiveStatus = async (payload) => {
  if (_.isEmpty(payload.indiHomeNum)) {
    logger.log('rewards active status', 'Invalid indiHomeNum.', 'error');
    return wrapper.error('fail', 'Invalid indiHomeNum given.', 400);
  }
  const acct = await userAccountRepository.getAccountData(payload.indiHomeNum).catch(error => logger.log('retrieving account from db', error, 'error in getting account'));
  if (!_.isEmpty(acct)) {
    const activeStatus = acct.rewardsActive;
    return wrapper.data(activeStatus, 'Successfully received active status', 200);

  }
  return wrapper.error('fail', 'Account not found', 404);

};

/**
 * Set rewards active status
 * @param {Object} payload - {indiHomeNum, status}
 * @returns {Promise<{err: *, data: null}|{err: null, data: *}>}
 */
const setRewardsActiveStatus = async (payload) => {
  const {indiHomeNum, status} = payload;
  const active = status === ACCOUNT_STATUS.ACTIVE ? true : false;
  const data = {
    indiHomeNum,
    status: active
  };
  const result = await userAccountRepository.updateRewardsActiveStatus(data);
  if (result.err) {
    logger.log('Update rewards active status', result.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }

  return wrapper.data(result, 'Successfully updated rewards active status', 200);
};

/**
 * Get account active status
 * @param {Object} payload - {indiHomeNum}
 * @returns {Promise<{err: *, data: null}|{err: null, data: *}>}
 */
const getActiveStatus = async (payload) => {
  let result = await cache.getAsync(payload.indiHomeNum);
  result = JSON.parse(result) || {};
  if (!result || util.isNullOrUndefined(result[ACCOUNT_STATUS.ACTIVE])) {
    const acct = await userAccountRepository.getAccountData(payload.indiHomeNum).catch(error => logger.log('retrieving account from db', error, 'error in getting account'));
    if (acct) {
      const acctCache = result;
      acctCache[REDIS_KEYS.ACTIVE] = acct.active;
      // set status in account cache
      cache.set(acct.indiHomeNum, JSON.stringify(acctCache), redis.print);
      result = await cache.getAsync(payload.indiHomeNum);
      result = JSON.parse(result);
    } else {
      logger.log('Account active status', 'Account not found', 'error');
      return wrapper.error('fail', 'Account not found', 404);
    }
  }

  if(result.err){
    logger.log('Received account active status', result.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }
  const acctStatus = result[ACCOUNT_STATUS.ACTIVE];
  return wrapper.data(acctStatus, 'Successfully received active status', 200);
};

/**
 * Set account active status
 * @param {Object} payload - {indiHomeNum, status}
 * @returns {Promise<{err: *, data: null}|{err: null, data: *}>}
 */
const setActiveStatus = async (payload) => {
  const {indiHomeNum, status} = payload;
  const active = status === ACCOUNT_STATUS.ACTIVE ? 1: 0;
  const data = {
    indiHomeNum,
    status: active
  };
  const result = await userAccountRepository.updateAccountStatus(data);
  if(result.err){
    logger.log('update account status', result.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }

  // set status in account cache
  let acctCache = await cache.getAsync(payload.indiHomeNum);
  acctCache = JSON.parse(acctCache) || {};
  acctCache[REDIS_KEYS.ACTIVE] = active;
  cache.set(indiHomeNum, JSON.stringify(acctCache), redis.print);
  return wrapper.data(result, 'Success updated account', 200);
};

const updateSvmLevel = async (payload) => {
  const result = await userAccountRepository.updateSvmLevel(payload);
  if(result.err){
    logger.log('update account status', result.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }

  // set svm Level in account cache
  let acctCache = await cache.getAsync(payload.indiHomeNum);
  acctCache = JSON.parse(acctCache) || {};
  acctCache[REDIS_KEYS.SVM_LEVEL] = payload.svmLevel.toString();

  const res = cache.set(payload.indiHomeNum, JSON.stringify(acctCache), redis.print);
  return wrapper.data(result, 'Success updated svm level', 201);
};

const getSvmLevel = async (indiHomeNum) => {
  let result = await cache.getAsync(indiHomeNum);
  result = JSON.parse(result) || {};

  if(!result || !result[REDIS_KEYS.SVM_LEVEL]){
    const userSvm = await userAccountRepository.getAccountData(indiHomeNum)
      .catch(error =>  logger.log('retrieving account from db', error, 'error in getting account'));
    if(userSvm){
      const acctCache = result;
      acctCache[REDIS_KEYS.SVM_LEVEL] = userSvm.svmLevel.toString();
      // set svm Level in account cache
      cache.set(userSvm.indiHomeNum, JSON.stringify(acctCache), redis.print);
      result = await cache.getAsync(indiHomeNum);
      result = JSON.parse(result);
    } else {
      return wrapper.error('fail', 'Account not found', 404);
    }
  }

  if (result.err) {
    logger.log('update account status', result.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }
  const svmLevel = result[REDIS_KEYS.SVM_LEVEL];
  return wrapper.data(svmLevel, 'Success updated svm level', 201);
};

const updateSVMInCoreSystem = async (indiHomeNum, userData) => {

  const result = await profileRepository.getAccountsEmailandId(indiHomeNum, userData.email).catch(error =>  logger.log('retrieving account from db', error, 'error in getting account'));
  if (!result) {
    return wrapper.error(new NotFoundError('Account not found'));
  }

  const updateProfilePayload = {
    NCLI: result.ncli,
    ND: result.indiHomeNum,
    nama: userData.name,
    relasi: '',
    hp: userData.mobile,
    email: userData.email,
    sumber: 'MYCX-147',
    korespondensi: '',
    kota: '',
    fb: '',
    twitter: ''
  };

  const data = await coreApiService.createRequest(config.get('/svm/coreSystemSvmUpdate'), updateProfilePayload).catch(error =>  {
    logger.log('error updating profile in core API', error, 'error updating profile in core API');
    const msg = new ConflictError('error updating profile in core API');
    return wrapper.error(msg);
  });

  return data;
};

const verifyIpAddress = async (payload, userData) => {
  const data = await coreApiService.createRequest(config.get('/svm/verifyInternetNumberHost'), {
    ipAddr: payload.ip
  }).catch(error =>  {
    logger.log('retrieving internet number', error, 'error retrieving internet number');
    const msg = new ConflictError('error retrieving internet number');
    return wrapper.error(msg);
  });

  if(data.statusCode === '0'){
    if(payload.indiHomeNum === data.data.ND){
      return await updateSvmLevel({
        indiHomeNum: payload.indiHomeNum,
        svmLevel: SVM_LEVEL.TWO
      }).then(async () => {
        const result = await updateSVMInCoreSystem(payload.indiHomeNum, userData);
        if(result.statusCode !== '0'){
          return wrapper.error(new NotFoundError('error updating profile in core API'));
        }
        return result;
      });
    }
    return wrapper.error(new NotFoundError('verification failed invalid internet number'));
  }

  const msg = new ConflictError('error retrieving internet number');
  return wrapper.error(msg);

};

const verifyKtpNumber = async (payload, userData) => {

  let data = await coreApiService.createRequest(config.get('/svm/verifyKtpAutomated'), {
    'nd' : payload.indiHomeNum
  }).catch(error =>  {
    logger.log('retrieving ktp number', error, 'error verifying ktp number');
    const msg = new ConflictError('error retrieving internet number');
    return wrapper.error(msg);
  });

  if(data.statusCode === '0'){
    if(data.data && data.data.idNum === payload.ktp){
      await updateSvmLevel({
        indiHomeNum: payload.indiHomeNum,
        svmLevel: SVM_LEVEL.ONE
      });

      return {
        ok:true,
        status:200,
        message:'verification successful',
        data: {
          status: 'successful'
        }
      };
    }
  }
  else {
    const account = await profileRepository.getAccountsEmailandId(payload.indiHomeNum, userData.email);
    if(!account){
      const msg = new ConflictError('account not found');
      return wrapper.error(msg);
    }

    const user = await profileRepository.getUserProfileByEmail(userData.email);
    if(!user.data){
      const msg = new ConflictError('user not found');
      return wrapper.error(msg);
    }

    let customerId = Math.floor(Date. now() / 1000);
    const ktpData = await ktpRepository.getKtpData(payload.indiHomeNum, account.id);
    if(ktpData && ktpData.data){
      customerId = ktpData.data.customerId;
    }

    const payloadKtp = {
      customer_id: customerId + '',
      id_ktp: payload.ktp,
      email: userData.email,
      hp: userData.mobile,
      nama: user.data.name,
      gender: user.data.gender,
      no_inet: payload.indiHomeNum,
      no_pots: account.fixedPhoneNum,
      flag: '0',
      portfolio_id: account.id + '',
      source: 'MIHX'
    };

    data = await coreApiService.createRequest(config.get('/svm/verifyKtpManual'), payloadKtp).catch(error =>  {
      logger.log(ctx, error, 'ktp manual verification failed');
      const msg = new ConflictError('ktp manual verification failed');
      return wrapper.error(msg);
    });

    if (data.code === '99' || data.code === '11'){

      const ktpData = {
        customerId: customerId,
        userId: userData.userId,
        indiHomeNum: payload.indiHomeNum,
        ktp: payload.ktp,
        status: 'in-progress',
        portfolioId: account.id + ''
      };

      if(data.code === '99'){
        await ktpRepository.saveKtpData(ktpData);
      }

      await sendKtpValidationNotification(ktpData, 'ktp verification in progress', data.info || 'verification in progress');
      return {
        ok:true,
        status:200,
        message: data.info || 'verification in progress',
        data: {
          status: 'in-progress'
        }
      };
    }
    const msg = new ConflictError('core api manual verification request failed');
    return wrapper.error(msg);

  }

  const msg = new ConflictError('error verifying');
  return wrapper.error(msg);
};

const updateVerifiedKtp = async (payload) => {
  const ktpData = await ktpRepository.getKtpData(payload.indiHomeNum, payload.portfolioId);
  if(ktpData && ktpData.err){
    return wrapper.error(new NotFoundError('ktp data not found'));
  }
  ktpData.data.status = payload.status;
  await ktpRepository.saveKtpData(ktpData.data);

  let topic = 'ktp verification successful';
  if(payload.status === 'fail')
  {
    topic = 'ktp verification failed';
  }
  else{
    await updateSvmLevel({
      indiHomeNum: payload.indiHomeNum,
      svmLevel: SVM_LEVEL.ONE
    });
  }

  await sendKtpValidationNotification(ktpData.data, topic, topic);
  return {};
};

async function sendKtpValidationNotification(payload, topic, description){
  let notifications = [
    {
      userId: payload.userId,
      header: topic,
      isChecked: false,
      referenceId: payload.ktp,
      notificationType: NOTIFICATION_TYPES.ACTIVITY,
      category: NOTIFICATION_CATEGORIES.VERIFY_KTP,
      imageUrl: '',
      group: NOTIFICATION_GROUPS.FULFILLMENT,
      body: {
        idHeader: 'Order ID',
        transactionId: '',//fixme--> require the transactionId/ Track id of this ktp success
        topic: topic,
        description: description,
        data: {
          ktp: payload.ktp,
          status: payload.status
        }
      }
    }
  ];

  await notificationService.saveNotifications(notifications);
}

const verifySvmByCall = async (userMobile, indiHomeNum) => {

  const requestPayload = {
    TrxID: 'DMS'+ await dateHelper.getDateFormatted(),
    Phone: userMobile,
    PIN: Math.floor(1000 + Math.random() * 9000) +'',
    KeyPublic: 'myIndihome'
  };

  requestPayload.indiHomeNum = indiHomeNum;

  const svmCallData = await svmCallRepository.getSvmCallData(userMobile);
  if(svmCallData && svmCallData.data){
    svmCallData.data.PIN = Math.floor(1000 + Math.random() * 9000) +'';
    await svmCallRepository.updateCallData(svmCallData.data);
  }
  else{
    await svmCallRepository.saveSvmCallData(requestPayload);
  }

  delete requestPayload.indiHomeNum;
  let sha = crypto.createHash('sha1');
  sha.update('INFOMEDIA#APP#41894b339c64429ae7c78a17eabce3f1a0140f8c#'+ requestPayload.TrxID +'#'+ requestPayload.Phone +'#'+ requestPayload.PIN +'#NUSANTARA');
  requestPayload['KeyHash'] = sha.digest('hex');

  const data = await coreApiService.createRequest(config.get('/svm/verifyByCall'), requestPayload).catch(error =>  {
    logger.log('processing call request', error, 'error processing call request');
    const msg = new ConflictError('error processing call request');
    return wrapper.error(msg);
  });

  if(data.statusCode === '0'){
    return { ok:true, status:200, message:'call request success', data: data.data };
  }

  const msg = new ConflictError('call request failed');
  return wrapper.error(msg);
};

const validateSvmCallPin = async (payload, userMobile) => {

  const svmCallData = svmCallRepository.getSvmCallData(userMobile);
  if(svmCallData.PIN === payload.pin){
    return {
      ok:true,
      status:200,
      message:'validation success',
      data: {}
    };
  }

  const msg = new NotFoundError('invalid pin');
  return wrapper.error(msg);
};

const saveKyc = async (payload, userId) => {
  try {
    if (!userId) {
      return wrapper.error('Invalid userId', 404);
    }

    const userData = await ssoService.getSSOUserData(userId);
    if (!userData) {
      return wrapper.error('Invalid userId', 404);
    }
    let userEmail = userData.email;

    let packageInformation = await getPackageInformation(payload.indiHomeNum, 'someUser');
    let profileData = await profileRepository.getUserProfileByEmail(userEmail);
    if (packageInformation.data && packageInformation.data[0]) {
      const options = await submitFCC(packageInformation.data[0], profileData.data);
      const result = await rp.post(options);
      if (result && result.code === 0) {
        let userData = await profileRepository.getAccountByIndiHomeNum(payload.indiHomeNum);
        if (userData) {
          const result = await userAccountRepository.saveKyc(
            {
              trackId: payload.trackId,
              document: payload.document,
              signature: payload.signature,
              documentWithUser: payload.documentWithUser,
              identityType: payload.documentType,
              AccountId: userData.data.id
            }
          );

          if (result.err) {
            logger.log('save kyc', result.err, 'error');
            return wrapper.error('fail', 'Internal Server Error', 500);
          }
          return wrapper.data(result, 'Success save kyc', 201);
        }
      }
      else{
        const msg = new ConflictError('submit FCC failed');
        logger.log('SaveKyc', msg, 'save failed');
        return wrapper.error(msg);
      }
    }
    else{
      const msg = new ConflictError('package not found');
      logger.log('SaveKyc', msg, 'save failed');
      return wrapper.error(msg);
    }

  } catch (err) {
    return wrapper.error(err);
  }
};

async function getJWT() {
  let options = {
    method: 'GET',
    url: config.get('/authorization').host + config.get('/authorization').endpoint,
    headers:
      {
        'Authorization': 'Basic ' + config.get('/authorization').token,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    json: true,
    strictSSL: false
  };

  let resp = await rp.get(options);
  return !util.isNullOrUndefined(resp) && !util.isNullOrUndefined(resp.data) ? resp.data['token'] : null;
}

async function getPackageInformation(indiHomeNum, userId) {
  const [authConfig] = config.get('/basicAuthApi');
  const options = {
    uri: `${config.get('/productSubscription/productSubscriptionBase')}${config.get('/productSubscription/selectedPackages')}${userId}?indiHomeNum=${indiHomeNum}`,
    auth: {
      user: authConfig.username,
      pass: authConfig.password,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    json: true // Automatically parses the JSON string in the response
  };

  try {
    const result = await rp.get(options);
    return result;
  } catch (err) {
    return wrapper.error(err);
  }
}

async function getPackageInformationByTransactionId(transactionId) {
  const [authConfig] = config.get('/basicAuthApi');
  const options = {
    uri: `${config.get('/productSubscription/productSubscriptionBase')}${config.get('/productSubscription/selectedPackagesByTransactionId')}${transactionId}`,
    auth: {
      user: authConfig.username,
      pass: authConfig.password,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    json: true // Automatically parses the JSON string in the response
  };

  try {
    const result = await rp.get(options);
    return result;
  } catch (err) {
    return wrapper.error(err);
  }
}

async function submitFCC(packageInformation, profileData) {
  const token = await getJWT();
  const authorization = 'Bearer ' + token;
  let date = new Date();
  let dateStr =
    date.getFullYear() + '-' +
    ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
    ('00' + date.getDate()).slice(-2) + ' ' +
    ('00' + date.getHours()).slice(-2) + ':' +
    ('00' + date.getMinutes()).slice(-2) + ':' +
    ('00' + date.getSeconds()).slice(-2);

  const data = {
    'ORDER_ID': packageInformation.id,
    'ORDER_TYPE_ID': 14,
    'ORDER_DTM': dateStr,
    'ORDER_STATUS_ID': 2,
    'SUBJECT': profileData.email,
    'IDENTITY': packageInformation.transactionId,
    'NAME': 'Open Order',
    'XN4': '10M',
    'XS1': profileData.name,
    'DATA3': packageInformation.package.description || '',
    'XS3': profileData.name,
    'XS5': profileData.primaryPhone,
    'XS7': 'MYINDIHOMEX',
    'DATA2': 'ODP-BIN-FBV/31',
    'XS2': '10M',
    'DATA6': 'LAIN-LAIN',
    'XS14': 'BIN',
    'XS11': profileData.secondaryPhone
  };

  return {
    method: 'POST',
    uri: `${config.get('/fcc/submitFccHost')}`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': authorization
    },
    form: {
      guid: 0,
      code: 0,
      data: JSON.stringify(data)
    },
    json: true,
    strictSSL: false
  };
}

const getKyc = async (trackId) => {
  const result = await userAccountRepository.getKycData(trackId).catch(error => logger.log('retrieving kyc data from db', error, 'kyc error'));
  if (result && result.err) {
    logger.log('retrieving kyc data failed', result.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }
  return wrapper.data(result, 'Success retrieving kyc data', 200);
};

const cancelOrder = async (payload) => {
  const result = await userAccountRepository.cancelOrder(payload).catch(error => logger.log('updating kyc order', error, 'updating kyc error'));
  if (result.err) {
    logger.log('retrieving kyc data failed', result.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }
  return wrapper.data(result, 'Success retrieving kyc data', 201);
};

const updateKycData = async (payload) => {

  if(!Object.values(KYC_STATUS).some(val => val === payload.validationStatus.toUpperCase())){
    logger.log(ctx, 'Error. invalid validation status.','');
    return wrapper.error(new BadRequestError('Invalid validation status'));
  }

  let result = await userAccountRepository.updateKyc(payload).catch(error => logger.log('updating kyc order', error, 'updating kyc error'));
  if (payload.validationStatus.toUpperCase() === KYC_STATUS.PROCESSED) {
    result = await userAccountRepository.updateUserData(payload).catch(error => logger.log('updating kyc order', error, 'updating account data error'));
  }

  await userAccountRepository.sendKycStatusNotifications(payload).catch(error => logger.log('sending notification kyc order', error, 'error sending notification'));

  if (result && result.err) {
    logger.log('retrieving kyc data failed', result.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }
  return wrapper.data(result, 'Success retrieving kyc data', 201);
};

const loadOrder = async (trackId) => {
  const result = await userAccountRepository.getKycDataWithUserAndLocation(trackId).catch(error => logger.log('retrieving kyc data from db', error, 'kyc error'));
  if (result && result.err) {
    logger.log('retrieving kyc data failed', result.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }

  let packageInformation = await getPackageInformationByTransactionId(trackId);
  if (packageInformation && packageInformation.err) {
    logger.log('retrieving kyc data failed', packageInformation.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }

  let billingInformation = await userAccountRepository.getBillingData(trackId);
  if (billingInformation && billingInformation.err) {
    logger.log('retrieving billing information failed', packageInformation.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }

  if(billingInformation){
    delete billingInformation.dataValues.AccountId;
    delete billingInformation.dataValues.trackId;
    delete billingInformation.dataValues.id;
  }

  let data = {};
  if (result) {
    data = {
      name: result.name,
      primaryPhone: result.primaryPhone,
      secondaryPhone: result.secondaryPhone,
      email: result.email,
      packageSelected: packageInformation.data.package.id,
      packagePrice: packageInformation.data.package.price,
      customer: {
        name: result.name,
        identityType: result.identityType,
        identityNumber: result.idNum,
        placeOfBirth: result.birthPlace,
        dateOfBirth: result.dob,
        validityPeriod: result.idExpDate,
        profession: result.profession,
        nationality: result.nationality,
        city: result.locations[0].city,
        cityCode: result.locations[0].cityCode,
        district: result.locations[0].district,
        districtCode: result.locations[0].districtCode,
        street: result.locations[0].street,
        streetCode: result.locations[0].streetCode,
        blockNumber: result.locations[0].blockNumber,
        postalCode: result.locations[0].postalCode
      },
      billing : billingInformation || {}
    };
  }
  return wrapper.data(data, 'Success retrieving kyc data', 200);
};

const setDefaultAccount = async(indiHomeNum, userId) => {
  if (!userId) {
    return wrapper.error('Invalid userId', 404);
  }

  try {
    const userData = await ssoService.getSSOUserData(userId);
    if (!userData) {
      return wrapper.error('Invalid userId', 404);
    }
    let userEmail = userData.email;
    const account = await userAccountRepository.setDefaultAccount(indiHomeNum, userEmail);
    return account;

  } catch (err) {
    logger.log('error saving default account', err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }
};

const disconnectAccount = async (indiHomeNum, userId, isConnect) => {
  if (!userId) {
    return wrapper.error('Invalid userId', 404);
  }

  try {
    const userData = await ssoService.getSSOUserData(userId);
    if (!userData) {
      return wrapper.error('Invalid userId', 404);
    }
    let userEmail = userData.email;
    const account = await userAccountRepository.connectDisconnectAccount(indiHomeNum, userEmail, isConnect);
    return account;

  } catch (err) {
    logger.log('error saving default account', err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }
};

const deleteAccount = async (indiHomeNum, userId, childId) => {

  if (!userId) {
    return wrapper.error('Invalid userId', 404);
  }

  try {
    const userData = await ssoService.getSSOUserData(userId);
    if (!userData) {
      return wrapper.error('Invalid userId', 404);
    }
    let parentEmail = userData.email;
    const account = await userAccountRepository.getParentAccount(indiHomeNum, parentEmail);
    if (!account.ok) {
      return wrapper.error('Invalid parent user for the account', 404);
    }

    const deleteAccount = await userAccountRepository.deleteAccount(indiHomeNum, childId);

    return deleteAccount;

  } catch (err) {
    logger.log('error saving default account', err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }
};

const getManageUsers = async(indiHomeNumber,userData)=>{
  const userAccounts = await userAccountRepository.getLinkedUsers(indiHomeNumber);
  // eslint-disable-next-line no-empty
  let accountsOut = [];
  if(userAccounts.status == 200){
    let accounts = userAccounts.data;
    accounts.forEach((value) => {
      let acc = {};
      acc.name = value['name'];
      acc.id = value.accounts[0].id;
      acc.location = value.accounts[0].Location.addressDesc;
      acc.indiHomeNumber = indiHomeNumber;
      if (value.accounts[0].parent === 'self') {
        acc.isOwner = true;
      } else {
        acc.isOwner = false;
      }
      if (value.accounts[0].email === userData.email) {
        acc.isUser = true;
      } else {
        acc.isUser = false;
      }
      accountsOut.push(acc);
    });

    userAccounts.data = accountsOut;
    return userAccounts;
  }
  return userAccounts;
};

const setVerificationDeadline = async (indiHomeNum, deadline) => {
  if (!deadline) {
    deadline = new Date();
    deadline.setHours(deadline.getHours() + 24);
  }

  const data = {
    indiHomeNum,
    verificationDeadline: deadline
  };
  const result = await userAccountRepository.updateAccountVerificationDeadline(data);
  if (result.err) {
    logger.log('update account status', result.err, 'error');
    return wrapper.error('fail', 'Internal Server Error', 500);
  }

  return {
    ok: true,
    status: 200,
    message: 'Successfully updated account',
    data: result,
  };
};

module.exports = {
  getRewardsActiveStatus,
  setRewardsActiveStatus,
  getActiveStatus,
  setActiveStatus,
  updateSvmLevel,
  getSvmLevel,
  saveKyc,
  getKyc,
  cancelOrder,
  updateKycData,
  loadOrder,
  setDefaultAccount,
  deleteAccount,
  getManageUsers,
  disconnectAccount,
  setVerificationDeadline,
  verifyIpAddress,
  verifyKtpNumber,
  updateVerifiedKtp,
  verifySvmByCall,
  validateSvmCallPin
};

