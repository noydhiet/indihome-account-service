/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 10/23/2019
 */

const joi = require('@hapi/joi');
const _ = require('lodash');
const config = require('../config/index');
const esIndexInboxNotifications = config.get('/elasticIndexes/inboxNotifications');

const insertDoc = require('../helpers/es/insertData');
const searchDoc = require('../helpers/es/searchData');
const updateDoc = require('../helpers/es/updateData');
const validator = require('../helpers/utils/validator');
const logger = require('../helpers/utils/logger');
const {BadRequestError} = require('../helpers/error');
const ctx = 'messageService-checkIndihomeAccount';
const {MESSAGE_TYPES} = require('../helpers/utils/constants/messageConstants');
const deviceRepository = require('../repository/deviceRepository');
const rp = require('request-promise');


/**
 * Use this method to save Notifications, can save as a bulk or single
 */

/**
 * Save bulk of messages
 * @param {Array} messages - message payload
 * @returns {Promise<{data: *, status: string}>}
 */
const saveMessages = async (messages=[]) => {

  let newNotifications = [];
  const metaData = {'index': {'_index': esIndexInboxNotifications, '_type': 'messages'}};


  const userIds = messages.reduce((rv, x) => {
    (rv[x['userId']] = rv[x['userId']] || []).push(x);
    return rv;
  }, {});


  let deviceIds = {};

  for (const [key, value] of Object.entries(userIds)) {
    const devices = await deviceRepository.getDevicesWithEmail(key);
    deviceIds[`${key}`] = devices;
  }


  for (let message of messages) {
    const validateData = validator.isValidPayload(message, messageSchema);

    if(validateData.err) {
      return {
        ok:false,
        message:'Error in data: '+ validateData.err,
        status:409,
        code:200,
        data:{}
      };

    }

    let messageSubType = '';

    if(message.messageSubType){
      messageSubType = message.messageSubType;
    }


    let dataIn = {
      timestamp: new Date().getTime(),
      checkDate: new Date().getTime(),
      header: message.header,
      referenceId: message.referenceId,
      isChecked: false,
      messageType: message.messageType,
      messageSubType: messageSubType,
      imageUrl: message.imageUrl,
      userId: message.userId
    };

    newNotifications.push(metaData);
    newNotifications.push(dataIn);

    const devices = deviceIds[message.userId];

    if(devices.status == 404 || devices.data.length === 0){
      const msg = new BadRequestError('Error sending push notifications');
      logger.log(ctx, msg, 'No user device found to push notifications');
      return {
        ok: false,
        message: 'No user device found to push notifications',
        status: 404,
        code: 200,
        data: {}
      };
    }

    let pushNotification = {
      deviceTokens: devices.data,
      message: {
        notification: {
          title: message.header,
          body: '',
          image: ''
        },
        data: {
          category: 'inbox',
          type: message.messageType,
          messageSubType:messageSubType,
          referenceId: message.referenceId
        },
        android: {
          priority: 'high'
        },
        apns: {
          headers: {
            'apns-priority': '10'
          }
        },
        webpush: {
          headers: {
            Urgency: 'high'
          }
        }
      }
    };

    if (devices.status == 200) {
      const pushStatus = await sendPushNotifications(pushNotification);
      if (!pushStatus.ok) {
        return {
          ok: false,
          message: 'Error sending push notifications',
          status:409,
          code:200,
          data:{}
        };
      }
      logger.log(ctx, 'Firebase Status', pushStatus.message);
    }

  }
  const resp = await insertDoc.insertBulkDoc(newNotifications);
  if (resp.err) {
    return {
      ok:false,
      message:'Error in saving data',
      status:409,
      code:200,
      data:{}
    };
  }

  return {
    ok:true,
    message:'Success saving data',
    status:200,
    code:200,
    data:resp.data
  };

};

/**
 * Get user inbox messages by type
 * @param {String} type - message type
 * @param {String} userId - user Id
 * @returns {Promise<BadRequestError|null|undefined>}
 */
const getMessagesByType = async (type, userId) => {
  try {
    return await getFilteredUserMessages(userId, type);
  } catch (e) {
    const msg = new BadRequestError('Error loading notifications');
    logger.log(ctx, msg, 'Error loading notifications');

    return msg;
  }
};

/**
 * Provides All inbox messages(Notifications)
 * @param {String} userId - user Id
 */
const getUserMessages = async (userId) => {
  try {
    const personalHits = await getFilteredUserMessages(userId,MESSAGE_TYPES.PERSONAL);
    const publicHits = await getFilteredUserMessages(userId,MESSAGE_TYPES.PROMO);

    // formatted to be used in the general wrapper.
    let formattedInbox = {
      data:{
        allMessages:[],
        promoMessages:[],
        personalMessages:[]
      } };

    let allMessages = [];
    let promoMessages = [];
    let personalMessages = [];

    if(!_.isEmpty(personalHits)){
      personalMessages = personalHits;
    }

    if(!_.isEmpty(publicHits)){
      promoMessages = publicHits;
    }

    if(!_.isEmpty(personalMessages)) {
      allMessages.push(personalMessages );
    }
    if(!_.isEmpty(promoMessages)) {
      allMessages.push(promoMessages);
    }

    formattedInbox.data.allMessages = allMessages;

    formattedInbox.data.personalMessages = personalMessages;

    formattedInbox.data.promoMessages = promoMessages;

    return formattedInbox;
  } catch (e) {
    // console.log(e);
    const msg = new BadRequestError('Error loading notifications');
    logger.log(ctx, msg, 'Error loading notifications');
    return msg;
  }
};



/**
 * Use to get Personal and Public notifications from DB by type
 * @param {String} userID - user ID
 * @param {String} messageType - message Type
 */
const getFilteredUserMessages = async (userID, messageType) => {
  const ctx = 'messageService-getFilteredUserMessages';

  const body = {
    'from': 0,
    'size': config.get('/elasticSearch/size'),
    'query': {
      'bool': {
        'must': [
          {
            'match': {
              'userId':userID
            }
          },
          {
            'match': {
              'messageType':messageType
            }
          }
        ]
      }
    },
    'sort': [
      {
        'timestamp': {
          'order': 'desc'
        }
      }]
  };

  try {
    const resp = await searchDoc(esIndexInboxNotifications,'messages',body);
    let results = resp['hits']['hits'].map((i) => {
      let sourse = i['_source'];
      sourse.messageId= i['_id'];
      return sourse;
    });
    return results;

  } catch (e) {
    logger.log(ctx, e, 'Search elastic data');
    return null;
  }
};

/**
 * Update message
 * @param {String} messageId - message Id
 * @param {Boolean} checked - is message read, True: checked, False: not checked
 * @returns {Promise<BadRequestError|*>}
 */
const updateMessage = async (messageId, checked) => {

  const body = {
    'query': {
      'match': {
        '_id': messageId
      }

    }
  };

  try {
    const resp = await searchDoc(esIndexInboxNotifications,'messages', body);

    let result = resp['hits']['hits'];

    let bodyIn = result[0];

    if(bodyIn !== null){

      bodyIn._source.isChecked = checked;

    }

    const resp2 = await updateDoc(esIndexInboxNotifications,'messages',messageId,bodyIn._source);
    return resp2;
  } catch (e) {
    const msg = new BadRequestError('Error saving data');
    logger.log(ctx, msg, 'Error saving data');
    return msg;
  }
};

/**
 * Object model for ES notification
 */
const messageSchema = joi.object({
  header: joi.string().required(),
  referenceId: joi.string().required(),
  isChecked: joi.boolean().optional(),
  messageType: joi.string().required(),
  messageSubType: joi.string().optional(),
  imageUrl: joi.string().optional().allow(''),
  userId: joi.string().required()
});



const sendPushNotifications = async(payload)=>{
  const [ authConfig ] = config.get('/basicAuthApi');
  const options = {
    method: 'POST',
    uri: `${config.get('/notificationBaseUrl')}/users/notification/push`,
    auth: {
      user: authConfig.username,
      pass: authConfig.password,
    },
    header: {
      'Content-Type': 'application/json'
    },
    body: payload,
    json: true,
  };

  try {
    const result = await rp.post(options);
    logger.log(ctx, 'response from notification service', result);
    return result;
  } catch (err) {
    return {
      ok:false,
      status:500,
      message: err.message,
      data: {}
    };
  }
};



module.exports = {
  saveMessages, getUserMessages, getMessagesByType, updateMessage
};

