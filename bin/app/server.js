const express = require('express');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const bodyParser = require('body-parser');
const basicAuth = require('../auth/basic_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
const logger = require('../helpers/utils/logger');
const { InternalServerError } = require('../helpers/error');
const fs = require('fs');
const path = require('path');
const spec = fs.readFileSync(path.join(__dirname, '../swagger/swagger.yaml'), 'utf8');
const jsyaml = require('js-yaml');
const swaggerDocument = jsyaml.safeLoad(spec);
const UserProfileConnect = require('../helpers/databases/sequelize');
const ctx = 'server-checkIndihomeAccount';

// routes
const notificationRoutes = require('../routes/notification');
const messageRoutes = require('../routes/message');
const accountRoutes = require('../routes/account');
const userProfileRoutes = require('../routes/userProfile');

function AppServer() {
  this.server = express();

  this.server.use(cors());
  this.server.use(bodyParser.json({limit: '50mb'}));
  this.server.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

  // required for basic auth
  this.server.use(basicAuth.init());
  this.server.use(bodyParser.json());

  //To check SQL connections
  UserProfileConnect.sequelize.sync({ force: false , alter : true })
    .then(() => {
      logger.log(ctx, 'All is well', 'Database & tables created!');
    })
    .catch(err => {
      logger.log(ctx, 'Unable to connect to the database:', err);
    });


  this.server.get('/', (req, res) => {
    wrapper.response(res, 'success', wrapper.data('Index'), 'This service is running properly');
  });

  this.server.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  // ------ Routes -------
  // user
  this.server.use('/account', accountRoutes);

  // inbox messages
  this.server.use('/account/inbox', messageRoutes);

  // home notifications
  this.server.use('/account/notifications', notificationRoutes);

  // user profile router
  this.server.use('/user', userProfileRoutes);

}

module.exports = AppServer;
