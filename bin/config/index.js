require('dotenv').config();
const confidence = require('confidence');

const config = {
  port: process.env.PORT,
  authorization: {
    host: process.env.AUTHORIZATION_API_HOST,
    endpoint: process.env.AUTHORIZATION_API_ENDPOINT,
    token: process.env.AUTHORIZATION_TOKEN,
    timeOut: process.env.AUTHORIZATION_TIMEOUT
  },
  basicAuthApi: [
    {
      username: process.env.BASIC_AUTH_USERNAME,
      password: process.env.BASIC_AUTH_PASSWORD
    }
  ],
  mysqlConfig: {
    connectionLimit: process.env.MYSQL_CONNECTION_LIMIT,
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE
  },
  redisCache: {
    host: process.env.REDIS_CACHE_HOST,
    port: process.env.REDIS_CACHE_PORT,
    password: process.env.REDIS_CACHE_PASSWORD
  },
  identityBaseUrl: process.env.IDENTITY_BASE_URL,
  notificationBaseUrl:process.env.NOTIFICATION_BASE_URL,
  authenticationPort: process.env.AUTHENTICATION_PORT,
  indihomeBaseUrl: process.env.INDIHOME_BASE_URL,
  indihomeBasicAuth: process.env.INDIHOME_BASIC_AUTH,
  jwtAuthKey: process.env.JWT_AUTH_KEY,
  dsnSentryUrl: process.env.DSN_SENTRY_URL,
  elasticSearch: {
    size: process.env.ELASTICSEARCH_MAX_SIZE,
    connectionClass: process.env.ELASTICSEARCH_CONNECTION_CLASS,
    apiVersion: process.env.ELASTICSEARCH_API_VERSION,
    host: [
      process.env.ELASTICSEARCH_HOST
    ],
    maxRetries: process.env.ELASTICSEARCH_MAX_RETRIES,
    requestTimeout: process.env.ELASTICSEARCH_REQUEST_TIMEOUT,
  },
  elasticIndexes: {
    inboxNotifications: process.env.ELASTICSEARCH_INBOX_NOTIFICATIONS_INDEX,
    homeNotifications: process.env.ELASTICSEARCH_HOME_NOTIFICATIONS_INDEX,
    accountLinkRequest: process.env.ELASTICSEARCH_ACCOUNT_LINK_REQUEST_INDEX,
    getEsSVMCallDataIndex: process.env.ELASTICSEARCH_SVM_CALL_DATA_INDEX,
    getEsSVMKtpDataIndex: process.env.ELASTICSEARCH_SVM_KTP_DATA_INDEX
  },
  homeNotificationTypes: process.env.HOME_NOTIFICATION_TYPES,
  reservation: {
    reserveFixedLineHost: process.env.RESERVATION_FIXED_LINE_HOST,
    reserveInternetHost: process.env.RESERVATION_INTERNET_HOST,
    indihomeIndicator: process.env.RESRERVATION_INDIHOME_INDICATOR,
    maxAccountLimit: process.env.RESRERVATION_MAX_ACCOUNT_LIMIT,
    immediateReservationActive: process.env.RESERVATION_IMMEDIATE_RESERVATION,
  },
  fcc: {
    submitFccHost: process.env.FCC_BASE_URL
  },
  productSubscription: {
    productSubscriptionBase: process.env.PRODUCT_SUBSCRIPTION_BASE,
    selectedPackages: process.env.GET_SELECTED_INTERNET_PACKAGES,
    selectedPackagesByTransactionId: process.env.GET_SELECTED_INTERNET_PACKAGES_BY_TRANSACTIONID,
    updatePackageEndpoint: process.env.UPDATE_SELECTED_INTERNET_PACKAGES_BY_TRANSACTIONID,
    timeout: 30000,
  },
  supportService: {
    host: process.env.USER_SUPPORT_HOST,
    updateScheduleEndpoint: process.env.USER_SUPPORT_UPDATE_SCHEDULE_ENDPOOINT,
    timeout: 30000,
  },
  svm: {
    verifyInternetNumberHost: process.env.VERIFY_INTERNET_NUMBER,
    verifyByCall: process.env.VERIFY_USER_PHONE,
    verifyKtpAutomated: process.env.VERIFY_USER_KTP_AUTOMATED,
    verifyKtpManual: process.env.VERIFY_USER_KTP_MANUAL,
    coreSystemSvmUpdate: process.env.CORE_SYSTEM_SVM_UPDATE
  }
};

const store = new confidence.Store(config);

exports.get = (key) => store.get(key);
