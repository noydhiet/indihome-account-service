const joi = require('joi');

const changeEmailRequestJoi = joi.object({
  resetEmailToken: joi.string().required(),
  value: joi.string().required(),
});

const changeMobileRequestJoi = joi.object({
  resetMobileToken: joi.string().required(),
  value: joi.string().required(),
});

const verifyVerificationCodeJoi = joi.object({
  verifyCode: joi.string().required(),
});

module.exports = {
    changeEmailRequestJoi, changeMobileRequestJoi, verifyVerificationCodeJoi
};

