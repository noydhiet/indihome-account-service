class InboxItem {
  constructor() {
    this.time = null;
    this.header = null;
    this.creationDate = new Date();
    this.isChecked = false;
    this.checkDate = null;
    this.messageId = null;
    this.referenceId = null;
    this.messageType = null;
    this.imageUrl = null;
  }
}

module.exports = InboxItem;
