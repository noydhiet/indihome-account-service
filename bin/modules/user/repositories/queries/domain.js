/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable no-trailing-spaces */
const rp = require('request-promise');
const validate = require('validate.js');
const wrapper = require('../../../../helpers/utils/wrapper');
const logger = require('../../../../helpers/utils/logger');
const config = require('../../../../config');
const commonUtil = require('../../../../helpers/utils/common');
const { NotFoundError, InternalServerError } = require('../../../../helpers/error');
const UserCommand = require('../commands/domain');
const query = require('./query');
const queryModel = require('./query_model');
const userProfileRepository = require('../../../../repository/profileRepository');

class User{

  constructor(){
    this.userCommand = new UserCommand();
  }

  async getAccessToken() {
    const ctx = 'queries-getAccessToken';
    const [ authUser ] = config.get('/basicAuthApi');
    const options = {
      method: 'GET',
      uri: `${config.get('/identityBaseUrl')}:${config.get('/authenticationPort')}/user/access-token`,
      auth: {
        user: authUser.username,
        pass: authUser.password
      },
      headers: {
        'Content-Type': 'application/json',
      },
      json: true,
    };

    try {
      const result = await rp.get(options);
      if (result.ok===false) {
        const msg = new InternalServerError('Authentication service connection lost');
        logger.log(ctx, msg, 'Authentication service connection lost');
        return wrapper.error(msg);
      }
      return wrapper.data(result.data);
    } catch (err) {
      logger.log(ctx, err, 'API connection error');
      return wrapper.error(err);
    }
  }


  async checkIndihomeAccount(indihomeNumber) {
    const ctx = 'queries-checkIndihomeAccount';
    try {
      const userData = await userProfileRepository.getBasicUserByIndiHomeNum(indihomeNumber);

      if (userData.code == 406) {
        const msg = new NotFoundError('Indihome user not found');
        logger.log(ctx, msg, 'user not found');
        return wrapper.formatData(userData,406);
      }
      if (userData.code == 404) {
        const msg = new NotFoundError('Indihome Account Found');
        logger.log(ctx, msg, 'Account not found');
        return wrapper.formatData(userData,404);
      }

      const account = {
        email: userData.data.email,
        mobileNumber: userData.data.primaryPhone,
        resetPasswordToken: commonUtil.generateRandomString(20),
        resetPasswordExpires: 32532995083000 // 1 hour

      };
      const payload = {
        email: userData.data.email,
        name: userData.data.name,
        password: commonUtil.generateRandomString(8),
        mobile: userData.data.primaryPhone,
        indihomeNumber: null,
        userRole: 'generalUser',
        status: 'migrated',
        resetPasswordToken:account.resetPasswordToken,
      	resetPasswordExpires:account.resetPasswordExpires
      };
      const user = await this.userCommand.saveUser(payload);

      if(user.status === 409){
        return wrapper.formatData(user,410);
      }
      if(user.status === 410){
        return wrapper.formatData(user,410);
      }
      if(user.status === 201){
        return wrapper.formatData(account,201);
      }
      if(user.status === 500){
        return wrapper.formatData(user,500);
      }
    } catch (err) {
      logger.log(ctx, err, 'API connection error');
      return wrapper.formatData(err,500);
    }
  }

  async checkISandProfile(payload) {

    let dataIn = payload;
    const ctx = 'queries-checkIndihomeAccount';
    try {
      const isUser = await this.userCommand.checkUserIs(dataIn);

      if(isUser.status === 500){
        return wrapper.formatData(new InternalServerError('Identity connection error'),500);
      }
      else if(isUser.status === 402){
        return wrapper.formatData(isUser.data,402);
      }
      else if(isUser.status === 403){
        return wrapper.formatData(isUser.data,403);
      }
      else if(isUser.data.isUser){
        return wrapper.formatData(isUser.data,200);
      }


      let userData ={};
      userData.code = 404;

      let value = dataIn.value;
      let type = dataIn.type;

      if(type === 'email'){
        userData = await userProfileRepository.getUserProfileByEmail(value);
      }

      if(type === 'mobile'){
        userData = await userProfileRepository.getUserProfileByMobile(value);
      }

      if (userData.code === 404) {
        const msg = new NotFoundError('Indihome Account Found');
        logger.log(ctx, msg, 'Account not found');
        return wrapper.formatData(isUser.data,404);
      }

      const account = {
        email: userData.data.email,
        mobileNumber: userData.data.primaryPhone,
        resetPasswordToken: commonUtil.generateRandomString(20),
        resetPasswordExpires:32532995083000
      };
      const payload = {
        email: userData.data.email,
        name: userData.data.name,
        password: commonUtil.generateRandomString(8),
        mobile: userData.data.primaryPhone,
        indihomeNumber: null,
        userRole: 'generalUser',
        status: 'migrated',
        resetPasswordToken:account.resetPasswordToken,
      	resetPasswordExpires:account.resetPasswordExpires
      };
      const user = await this.userCommand.saveUser(payload);

      if(user.status == 409){
        return wrapper.formatData(user,410);
      }
      if(user.status == 410){
        return wrapper.formatData(user,410);
      }
      if(user.status == 201){
        return wrapper.formatData(account,201);
      }
      if(user.status == 500){

        return wrapper.formatData(user,500);
      }

    } catch (err) {
      logger.log(ctx, err, 'API connection error');
      return wrapper.formatData(err,500);
    }
  }

  async getDeviceId(data) {

    if (validate.isEmpty(data)) {

      return wrapper.error(false, 'user id can`t be empty', 400);

    }

    let getResult = await query.getDeviceByUserId(data);

    if (validate.isEmpty(getResult.data['data'])) {
      return wrapper.error('device id not found', 401);
    }

    let result = getResult.data['data'][0];

    let modelUser = queryModel.user();
    modelUser.userId = result.user_id;
    modelUser.deviceId = result.device_id;

    return wrapper.data(modelUser, 'Success get device_id', 201);

  }

}

module.exports = User;
