const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../config/index');
const Mysql = require('../../../../helpers/databases/mysql/db');

const getDeviceByUserId = async (data) => {
  const db = new Mysql(config.get('/mysqlConfig'));
  const userId = data.userId;
  const query = 'SELECT * FROM myIndihome.account WHERE user_id = ?';
  const result = await db.query(query,[userId]);
  if (result.err) {
    return wrapper.error('fail', 'cannot get  device_id', 500);
  }
  return wrapper.data(result, 'success get  device_id', 200);
};

module.exports = {
  getDeviceByUserId
};
