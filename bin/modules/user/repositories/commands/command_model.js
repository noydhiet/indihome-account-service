const joi = require('joi');
const {ACCOUNT_STATUS} = require('../../../../helpers/utils/constants/profileConstants');
const saveDeviceJoi = joi.object({
  userId: joi.string().required(),
  deviceId: joi.string().required(),
});

const updateAccountStatusJoi = joi.object({
  indiHomeNum: joi.string().required(),
  status: joi.string().valid(ACCOUNT_STATUS.ACTIVE, ACCOUNT_STATUS.INACTIVE).required()
});

const updateSvmLevelJoi = joi.object({
  indiHomeNum: joi.string().required(),
  email: joi.string().required(),
  svmLevel: joi.number().required()
});

const verifyIpJoi = joi.object({
  indiHomeNum: joi.string().required(),
  ip: joi.string().required()
});

const verifyKtpJoi = joi.object({
  indiHomeNum: joi.string().required(),
  ktp: joi.string().required()
});

const updateVerifyKtpJoi = joi.object({
  portfolioId: joi.string().required(),
  indiHomeNum: joi.string().required(),
  status: joi.string().required(),
});

const requestSvmCallJoi = joi.object({
  indiHomeNum: joi.string().required()
});

const verifyByCallJoi = joi.object({
  pin: joi.string().required(),
  indiHomeNum: joi.string().required()
});

const getAccountCacheJoi = joi.object({
  indiHomeNum: joi.string().required()
});

const saveKycJoi = joi.object({
  trackId: joi.string().required(),
  indiHomeNum: joi.string().required(),
  documentType: joi.string().required(),
  document: joi.string().required(),
  signature: joi.string().required(),
  documentWithUser: joi.string().required(),
  documentNumber: joi.string().required(),
  name: joi.string().optional().allow(''),
  mothersName: joi.string().optional().allow(''),
  dob: joi.string().optional().allow(''),
});

const cancelOrderJoi = joi.object({
  trackId: joi.string().required(),
  reasonId: joi.string().required(),
  reasonDescription: joi.string().required(),
  cancelledBy: joi.string().required()
});

const updateKycJoi = joi.object({
  trackId: joi.string().required(),
  validationStatus: joi.string().required(),
  identityType: joi.string().optional().allow(''),
  validatedBy: joi.string().required(),
  invalidReason: joi.string().optional().allow(''),
  customer: joi.object().optional(),
  billing: joi.object().optional()
});

const updateUserJoi = joi.object({
  dob: joi.string().required(),
  gender: joi.string().required(),
  name: joi.string().required()
});


module.exports = {
  saveDeviceJoi,
  updateAccountStatusJoi,
  updateSvmLevelJoi,
  getAccountCacheJoi,
  updateKycJoi,
  saveKycJoi,
  cancelOrderJoi,
  updateUserJoi,
  verifyIpJoi,
  verifyKtpJoi,
  updateVerifyKtpJoi,
  verifyByCallJoi,
  requestSvmCallJoi
};

