const User = require('./domain');

const saveDevice = async (payload) => {
  const user = new User();
  const postCommand = async (payload) => {
    return await user.create(payload);
  };
  return postCommand(payload);
};

const getDevice = async (payload) => {
  const user = new User();
  const putCommand = async (payload) => {
    return await user.getDevice(payload);
  };
  return putCommand(payload);
};

const deleteDevice = async (payload) => {
  const user = new User();
  const deleteCommand = async (payload) => {
    return await user.delete(payload);
  };
  return deleteCommand(payload);
};

module.exports = {
  saveDevice,
  getDevice,
  deleteDevice
};
