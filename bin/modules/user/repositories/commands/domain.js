const rp = require('request-promise');
const config = require('../../../../config');
const wrapper = require('../../../../helpers/utils/wrapper');
const command = require('../commands/command');
const logger = require('../../../../helpers/utils/logger');
const util = require('util');
const { ConflictError, ConditionNotMetError } = require('../../../../helpers/error');

class User{

  async saveUser(payload){
    const [ authConfig ] = config.get('/basicAuthApi');
    const options = {
      method: 'POST',
      uri: `${config.get('/identityBaseUrl')}/user/userInfomation`,
      auth: {
        user: authConfig.username,
        pass: authConfig.password,
      },
      header: {
        'Content-Type': 'application/json'
      },
      body: payload,
      json: true,
    };

    try {
      const result = await rp.post(options);
      return result;
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async userSiginUp(payload){
    const [ authConfig ] = config.get('/basicAuthApi');
    const options = {
      method: 'POST',
      uri: `${config.get('/identityBaseUrl')}/user/signup`,
      auth: {
        user: authConfig.username,
        pass: authConfig.password,
      },
      header: {
        'Content-Type': 'application/json'
      },
      body: payload,
      json: true,
    };

    try {
      const result = await rp.post(options);
      return result;
    } catch (err) {
      return {
        ok:false,
        status:500,
        message: err.message,
        data: {}
      };
    }
  }

  async changeUserEmail(payload){
    const [ authConfig ] = config.get('/basicAuthApi');
    const options = {
      method: 'POST',
      uri: `${config.get('/identityBaseUrl')}/user/changeEmail`,
      auth: {
        user: authConfig.username,
        pass: authConfig.password,
      },
      header: {
        'Content-Type': 'application/json'
      },
      body: payload,
      json: true,
    };

    try {
      const result = await rp.post(options);
      return result;
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async verifyVerificationCode(payload){
    const [ authConfig ] = config.get('/basicAuthApi');
    const options = {
      method: 'POST',
      uri: `${config.get('/identityBaseUrl')}/user/verifyVerification`,
      auth: {
        user: authConfig.username,
        pass: authConfig.password,
      },
      header: {
        'Content-Type': 'application/json'
      },
      body: payload,
      json: true,
    };

    try {
      const result = await rp.post(options);
      return result;
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async changeUserMobile(payload){
    const [ authConfig ] = config.get('/basicAuthApi');
    const options = {
      method: 'POST',
      uri: `${config.get('/identityBaseUrl')}/user/changeMobile`,
      auth: {
        user: authConfig.username,
        pass: authConfig.password,
      },
      header: {
        'Content-Type': 'application/json'
      },
      body: payload,
      json: true,
    };

    try {
      const result = await rp.post(options);
      return result;
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async checkUserIs(payload){
    const [ authConfig ] = config.get('/basicAuthApi');
    const options = {
      uri: `${config.get('/identityBaseUrl')}/user/userCheck`,
      qs: {
        type: payload.type,
        value:payload.value,
      },
      auth: {
        user: authConfig.username,
        pass: authConfig.password,
      },
      headers: {
        'Content-Type': 'application/json'
      },
      json: true // Automatically parses the JSON string in the response
    };

    try {
      const result = await rp.get(options);
      return result;
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async create(payload){
    const tmp = {
      userId: payload.userId,
      deviceId: payload.deviceId
    };
    const result = await command.deviceSave(tmp);
    if(result.err){
      logger.log('device save', result.err, 'error');
      return wrapper.error('fail', 'Internal Server Error', 500);
    }

    payload['faqId'] = tmp.faqId;
    return wrapper.data(payload, 'Success save device', 201);
  }

  async getDevice(payload){
    const tmp = {
      userId: payload.userId
    };
    const result = await command.deviceSave(tmp);
    if(result.err){
      logger.log('device save', result.err, 'error');
      return wrapper.error('fail', 'Internal Server Error', 500);
    }

    payload['faqId'] = tmp.faqId;
    return wrapper.data(payload, 'Success save device', 201);
  }

  async delete(payload){
    const resultFind = await command.deviceGetOneById(payload);
    if(resultFind.length === 0) {
      return wrapper.error('fail', 'device not found', 500);
    }

    const resultDelete = await command.deviceDelete(payload);
    if(resultDelete.err) {
      return wrapper.error('fail', 'Drop data device failed', 500);
    }

    return wrapper.data(resultDelete.data, 'Success drop Data device', 301);
  }

  async getJWT() {
    let options = {
      method: 'GET',
      url: config.get('/authorization').host + config.get('/authorization').endpoint,
      headers:
        {
          'Authorization': 'Basic ' + config.get('/authorization').token,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
      json: true,
      strictSSL: false
    };

    let resp = await rp.get(options);
    return !util.isNullOrUndefined(resp) && !util.isNullOrUndefined(resp.data) ? resp.data['token'] : null;
  }

  //CheckIndiHome number

  async checkIndiHomeNumber(payload){
    let options = await this.generateHttpRequest(payload, '/gateway/telkom-isiska-getPortofolio/1.0/getPortofolio');
    try {
      const result = await rp.post(options);
      if(result && result.statusCode && result.statusCode === '0'){
        return {ok:true,
          message:'success',
          status:200,
          data:result.data}
      }
      const msg = new ConflictError(result.returnMessage);
      logger.log('checkIndiHome', msg, result.returnMessage);
      return {ok:false,
        message:'invalid number',
        status:404,
        data:{}};
    } catch (err) {
      const msg = new ConflictError(err);
      logger.log('checkIndiHome', msg, err);
       return {ok:false,
        message:'getPortofolio core api error ',
        status:500,
        data:{}};
    }
  }



  async uploadAndCheckOCR(payload){
    let result =  await this.uploadOCR(payload);
    if(result && result.data && result.data.scanRef){
      let i;
      let ocrScanResult;
      for (i = 0; i < 3; i++) {
        await this.sleep(5000);
        ocrScanResult = await this.checkOCR({
          scanReferenceId: result.data.scanRef
        });

        if(ocrScanResult){
          return ocrScanResult;
        }
      }

      return ocrScanResult;
    }

    return result;
  }

  async sleep(ms){
    return new Promise(resolve=>{
      setTimeout(resolve,ms);
    });
  }

  async uploadOCR(payload){
    let options = await this.generateHttpRequest(payload, '/gateway/telkom-peruri-ocr/1.0/uploadOCR');
    try {
      const result = await rp.post(options);
      if(result && result.statusCode && result.statusCode === '0'){
        return wrapper.data(result.data);
      }
      const msg = new ConflictError('Upload failed');
      logger.log('uploadOCR', msg, 'Upload failed');
      return wrapper.error(msg);

    } catch (err) {
      return wrapper.error(err);
    }
  }

  async checkOCR(payload){
    let options = await this.generateHttpRequest(payload, '/gateway/telkom-peruri-ocr/1.0/checkOCR');
    try {
      const result = await rp.post(options);
      if(result && result.statusCode && result.statusCode === '0'){
        const res = await this.generateCheckOCRResponse(result.data);
        return wrapper.data(res);
      }
      const msg = new ConflictError('Invalid document');
      logger.log('checkOCR', msg, 'Invalid document');
      return wrapper.error(msg);
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async generateCheckOCRResponse(data){
    return {
      upload_id : data.upload_id,
      scanReferenceId: data.scanReferenceId,
      religion: data.Agama,
      address: data.Alamat,
      validUntil: data.BerlakuHingga,
      gender: data.JenisKelamin,
      district: data.Kecamatan,
      subDistrict: data.Kelurahan,
      nationality: data.Kewarganegaraan,
      city: data.Kotakab,
      nik: data.NIK,
      name: data.Nama,
      profession: data.Pekerjaan,
      province: data.Propinsi,
      RtRw: data.RtRw,
      martialStatus: data.StatusPerkawinan,
      dateOfBirth: data.TanggalLahir,
      placeOfBirth: data.TempatLahir
    };
  }
  
  //modify this method to compare more parameters in the future
  async compareCheckOcrWithRegisterJson(jo1, jo2){
    if(jo1.city !== jo2.city){
      return false;
    }else if(jo1.province !== jo2.province)
    {
      return false;
    }
    else if(jo1.address !== jo2.address){
      return false;
    }
    else if(jo1.placeOfBirth !==  jo2.placeOfBirth){
      return false;
    }
    else if(jo1.dateOfBirth !== jo2.dateOfBirth){
      return false;
    }
    return true;
  }

  async registerKtp(payload){
    let checkOcr = await this.checkOCR({
      scanReferenceId: payload.scanRef
    });

    delete payload['scanRef'];
    let options = await this.generateHttpRequest(payload, '/gateway/telkom-peruri-digisign/1.0/registration');
    if(!checkOcr || !this.compareCheckOcrWithRegisterJson(checkOcr, payload)){
      throw new ConditionNotMetError('Invalid payload');
    }

    try {
      const result = await rp.post(options);
      return wrapper.data(result.data);
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async verifyVideo(payload){
    let options = await this.generateHttpRequest(payload, '/gateway/telkom-peruri-digisign/1.0/videoVerification');
    try {
      const result = await rp.post(options);
      if(result.data && result.data.statusCode === '0'){
        return await this.activateUser({
          email: payload.email
        });
      }
      return wrapper.error('video verification failed');
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async activateUser(payload){
    let options = await this.generateHttpRequest(payload, '/gateway/telkom-peruri-digisign/1.0/activate');
    try {
      const result = await rp.post(options);
      return wrapper.data(result.data);
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async checkCertificate(payload){
    let options = await this.generateHttpRequest(payload, '/gateway/telkom-peruri-digisign/1.0/checkCertificate');
    try {
      const result = await rp.post(options);
      return wrapper.data(result.data);
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async generateHttpRequest(payload, route) {
    const token = await this.getJWT();
    const authorization = 'Bearer ' + token;

    return {
      method: 'POST',
      uri: `${config.get('/indihomeBaseUrl')}${route}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authorization
      },
      json: payload,
      strictSSL: false
    };
  }

  async getSSOUser(email){
    const [ authConfig ] = config.get('/basicAuthApi');
    const options = {
      method: 'GET',
      uri: `${config.get('/identityBaseUrl')}/user/userInfomation/`+ email,
      auth: {
        user: authConfig.username,
        pass: authConfig.password,
      },
      header: {
        'Content-Type': 'application/json'
      },
      json: true,
    };

    try {
      const result = await rp.get(options);
      return result;
    } catch (err) {
      return wrapper.error(err);
    }
  }

  async getSSOUserById(userId){
    const [ authConfig ] = config.get('/basicAuthApi');
    const options = {
      method: 'GET',
      uri: `${config.get('/identityBaseUrl')}/user/userSsoProfile/`+ userId,
      auth: {
        user: authConfig.username,
        pass: authConfig.password,
      },
      header: {
        'Content-Type': 'application/json'
      },
      json: true,
    };

    try {
      const result = await rp.get(options);
      return result;
    } catch (err) {
      return wrapper.error(err);
    }
  }
}

module.exports = User;
