const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../config/index');
const Mysql = require('../../../../helpers/databases/mysql/db');

const deviceSave = async (_) => {
  const { userId, deviceId  } = _;
  const db = new Mysql(config.get('/mysqlConfig'));
  const query = 'INSERT INTO myIndihome.account (user_id, device_id) VALUES (?, ?)';
  const result = await db.query(query,[userId,deviceId]);
  if(result.err){
    return wrapper.error('fail', 'cannot insert device', 500);
  }
  return wrapper.data('Success Save Device');
};

const deviceGetOneById = async (_) => {
  const { userId, deviceId } = _;
  const db = new Mysql(config.get('/mysqlConfig'));
  const query = 'SELECT * FROM myIndihome.account WHERE user_id= ? and device_id= ?';
  const result = await db.query(query,[userId,deviceId]);
  if(result.err) {
    return wrapper.error('fail', 'cannot count data device', 500);
  }
  return result;
};

const deviceDelete = async (_) => {
  const { userId,deviceId } = _;
  const db = new Mysql(config.get('/mysqlConfig'));
  const query = 'DELETE FROM myIndihome.account WHERE user_id= ? and device_id= ? ';
  const result = await db.query(query,[userId,deviceId]);
  if(result.err){
    return wrapper.error('fail', 'cannot delete data device', 500);
  }
  return result;
};

module.exports = {
  deviceSave,
  deviceGetOneById,
  deviceDelete
};
