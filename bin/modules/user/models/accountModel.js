/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/02/2019
 */
module.exports = (sequelize, type) => {
  return sequelize.define('Account', {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    indiHomeNum: {type: type.STRING},
    internetNum: {type: type.STRING},
    //fixme: this can be empty because some may not have the fixed phones (also ReserveV2 is failing as well)
    fixedPhoneNum: {type: type.STRING},
    ncli: {type: type.STRING, unique: false},
    email: type.STRING,
    private: {type: type.BOOLEAN, defaultValue: false},
    active: {type: type.BOOLEAN, defaultValue: false},
    rewardsActive: {type: type.BOOLEAN, defaultValue: false},
    type: {type: type.STRING, defaultValue: 'INTERNET'},
    transaction: {type: type.STRING},
    reservationId: {type: type.STRING},
    reservationPort: {type: type.STRING},
    svmLevel: { type: type.INTEGER, defaultValue: 0},
    region: { type: type.STRING },
    parent: { type: type.STRING },
    witel: { type: type.STRING },
    activatedDate: { type: type.STRING },
    deactivatedDate: { type: type.STRING },
    verificationDeadline: { type: type.DATE },
    isDefault: {type: type.BOOLEAN, defaultValue: false},
    isConnected: {type: type.BOOLEAN, defaultValue: true}
  });
};
