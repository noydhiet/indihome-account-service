/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/11/2019
 */
module.exports = (sequelize, type) => {
  return sequelize.define('Device', {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    ssoUserId:type.STRING,
    deviceId: {type: type.STRING, unique: 'deviceId'},
    fcmToken: {type: type.STRING, unique: 'fcmToken'},
    deviceType:type.STRING,
  });
};
