/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/26/2019
 */
module.exports = (sequelize, type) => {
  return sequelize.define('AccountLink', {
    id: {
      allowNull: false,
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    parentId: {
      allowNull: false,
      type: type.INTEGER,
    },
    childId: {
      allowNull: false,
      type: type.INTEGER,
    },
  });
} ;
