
module.exports = (sequelize, type) => {
  return sequelize.define('BillingDetails', {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    trackId: {type: type.STRING},
    name: {type: type.STRING},
    identityType: {type: type.STRING},
    identityNumber: {type: type.STRING},
    placeOfBirth: {type: type.STRING},
    dateOfBirth: {type: type.STRING},
    validityPeriod: {type: type.STRING},
    profession: {type: type.STRING},
    nationality: {type: type.STRING},
    city:{type: type.STRING},
    cityCode: {type: type.STRING},
    district: {type: type.STRING},
    districtCode: {type: type.STRING},
    street: {type: type.STRING},
    streetCode: {type: type.STRING},
    blockNumber: {type: type.STRING},
    postalCode: {type: type.STRING}
  });
};

