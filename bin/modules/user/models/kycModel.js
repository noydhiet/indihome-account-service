module.exports = (sequelize, type) => {
  return sequelize.define('kyc', {
    id: {
      allowNull: false,
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    trackId: type.STRING,
    document: type.TEXT,
    documentWithUser: type.TEXT,
    signature: type.TEXT,
    identityType: type.STRING,
    isCancelled: {type: type.BOOLEAN, defaultValue: false},
    cancelledReasonId: type.INTEGER,
    cancelledReason: type.STRING,
    cancelledBy: type.STRING,
    validationStatus: type.STRING,
    validatedBy: type.STRING,
    invalidReason: type.STRING
  });
} ;
