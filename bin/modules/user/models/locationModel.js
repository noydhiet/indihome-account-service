/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/11/2019
 */
module.exports = (sequelize, type) => {
  return sequelize.define('Location', {
    id: {
      allowNull: false,
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    province: type.STRING,
    provinceCode: type.STRING,
    city: type.STRING,
    cityCode: type.STRING,
    district: type.STRING,
    districtCode: type.STRING,
    street: type.STRING,
    streetCode: type.STRING,
    blockNumber: type.STRING,
    postalCode: type.STRING,
    addressDesc: type.STRING,
    block: type.STRING,
    floor: type.INTEGER,
    room: type.STRING,
    latitude: type.FLOAT,
    longitude: type.FLOAT,
    areaCode: type.STRING,
    sto: type.STRING,
    deviceId: type.STRING,
    system: type.STRING,
    locId: type.STRING,
    primary: type.BOOLEAN,
    rt: type.STRING,
    rw: type.STRING,
    level: {type: type.STRING, defaultValue: 'PT1'},
    cableName: {type: type.STRING, defaultValue: ''}
  });
} ;
