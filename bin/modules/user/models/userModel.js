/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/11/2019
 */
module.exports = (sequelize, type) => {
  return sequelize.define('User', {
    id: {
      allowNull: false,
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: type.STRING,
    primaryPhone:{
      type: type.STRING(32),
      allowNull: false,
      unique: 'primaryPhone'
    },
    secondaryPhone: {
      type: type.STRING(32)
    },
    email:{
      type: type.STRING(128),
      allowNull: false,
      unique: 'email'
    },
    password: { type: type.STRING},
    dob: type.STRING,
    birthPlace: type.STRING,
    identityType: type.STRING,
    identityStatus: type.STRING,
    emailVarified: {type: type.BOOLEAN, defaultValue: false},
    phoneVerified: type.STRING,
    idNum: type.STRING,
    idExpDate: type.STRING,
    mothersName: type.STRING,
    country: type.STRING,
    nationality: type.STRING,
    profession:type.STRING,
    gender: type.STRING,
    msisdn: type.STRING,
    profileStatus: type.STRING,
    isPurchaseNoti:{type: type.BOOLEAN, defaultValue: true},
    isBillNoti:{type: type.BOOLEAN, defaultValue: true},
    isAccountChangeNoti:{type: type.BOOLEAN, defaultValue: true},
    isPromotionsNoti:{type: type.BOOLEAN, defaultValue: true},
    language:{type: type.STRING, defaultValue:'en'},
  });
} ;
