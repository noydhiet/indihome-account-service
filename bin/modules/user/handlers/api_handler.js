const wrapper = require('../../../helpers/utils/wrapper');
const UserQuery = require('../repositories/queries/domain');
const commandHandler = require('../repositories/commands/command_handler');
const queryHandler = require('../repositories/queries/query_handler');
const modelCommand = require('../repositories/commands/command_model');
const validator = require('../../../helpers/utils/validator');
const ssoService = require('../../../services/ssoService');

const checkIndihomeAccount = async (req, res) => {
  const { indihomeNumber } = req.params;
  const userQuery = new UserQuery();
  try{
    const getData = await userQuery.checkIndihomeAccount(indihomeNumber);

    if(getData.err == 409){
      wrapper.formattedResponse(res, false, 410, 'User already migrated', {}, 200, );
    }
    if(getData.err == 410){
      wrapper.formattedResponse(res, false, 410, 'User already migrated', {}, 200, );
    }
    if(getData.err == 201){
      wrapper.formattedResponse(res, false, 200, 'User data successfully migrated,setup a new password', getData.data, 200);
    }
    if(getData.err == 404){
      wrapper.formattedResponse(res, false, 404, 'Indihome Account Found', getData.data, 200);
    }
    if(getData.err == 406){
      wrapper.formattedResponse(res, false, 406, 'Indihome user not found', getData.data, 200);
    }
    if(getData.err == 500){
      wrapper.formattedResponse(res, false, 500, 'System error', {}, 500);
    }
    if(getData.err == 200){
      wrapper.formattedResponse(res, false, 403, 'User already registered', {}, 200);
    }
  }catch(err){
    wrapper.formattedResponse(res, false, 500, 'System error', err, 500);
  }
};


const checkUser = async (req, res) => {
  let payload = {type:'',value:''};
  payload.type = req.query.type;
  payload.value = req.query.value;
  const userQuery = new UserQuery();

  try{
    const getData = await userQuery.checkISandProfile(payload);

    if(getData.err === 409){
      return wrapper.formattedResponse(res,false,409,'Error migrating user data, Conflict in user email',{},200,);
    }
    else if(getData.err === 410){
      return wrapper.formattedResponse(res,false,410,'Error migrating user data, Conflict in user phone number',{},200,);
    }
    else if(getData.err === 201){
      return wrapper.formattedResponse(res,false,203,'User data successfully migrated,setup a new password',getData.data,200);
    }
    else if(getData.err === 500){
      return wrapper.formattedResponse(res,false,500,'System error',{},500);
    }
    else if(getData.err === 200){
      return wrapper.formattedResponse(res,true,200,'User registered',getData.data,200);
    }
    else if(getData.err === 404){
      return wrapper.formattedResponse(res,false,404,'User not registered',getData.data,200);
    }
    else if(getData.err === 402){
      return wrapper.formattedResponse(res,false,402,'User already migrated, set new password',getData.data,200);
    } else if (getData.err === 403) {
      return wrapper.formattedResponse(res, false, 403, 'blacklisted email', getData.data, 200);
    }

  }catch(err){
    wrapper.formattedResponse(res,false,500,'System error',err,500);
  }
};


const getDeviceByUser = async (req, res) => {

  const postRequest = async () => {

    return queryHandler.getDeviceById(req.params);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result)
      : wrapper.paginationResponse(res, 'success', result, result.message);
  };
  sendResponse(await postRequest());
};

const accountService = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.saveDeviceJoi);
  const postData = async (result) => {
    if(result.err){
		    return result;
    }
	    return await commandHandler.saveDevice(payload);
  };
  const sendResponse = async (result) => {
	    (result.err) ? wrapper.response(res, 'fail', result, 'Save device failed')
	      : wrapper.response(res, 'success', result, 'Save device success');
  };
  sendResponse(await postData(await validatePayload));
};

const DeletAccountService = async (req, res) => {
  const payload = req.body;

  const validatePayload = validator.isValidPayload(payload, modelCommand.saveDeviceJoi);
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return await commandHandler.deleteDevice(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'delete device failed')
      : wrapper.response(res, 'success', result, 'delete device success');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports = {
  checkIndihomeAccount,
  getDeviceByUser,
  accountService,
  DeletAccountService,
  checkUser
};
