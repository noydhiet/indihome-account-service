const rp = require('request-promise');
const config = require('../../../../config');
const wrapper = require('../../../../helpers/utils/wrapper');
const UserQuery = require('../../../user/repositories/queries/domain');
const logger = require('../../../../helpers/utils/logger');
const { ConflictError } = require('../../../../helpers/error');

class Registration{

  constructor() {
    this.userQuery = new UserQuery();
  }

  async registerUser(payload) {
    const ctx = 'commands-registerUser';
    const { email, fullName, password, mobileNumber, gender } = payload;
    const tokenData = await this.userQuery.getAccessToken();
    if(tokenData.err) {
      return wrapper.error(tokenData.err);
    }

    const userData = { email, fullname : fullName, password, msisdn : mobileNumber, gender };

    const options = {
      method: 'POST',
      uri: `${config.get('/indihomeBaseUrl')}/gateway/myIndiHomeAPI-api/1.0/user-signup`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: `Bearer ${tokenData.data.token}`
      },
      form: {
        guid: '',
        code: 0,
        data: JSON.stringify(userData)
      },
      json: true,
    };

    try {
      const { info } = await rp.post(options);
      if (info !== 'OK') {
        const msg = new ConflictError('Registration failed, user already exist');
        logger.log(ctx, msg, 'Registration failed');
        return wrapper.error(msg);
      }

      return wrapper.data('Registration user successfull');
    } catch (err) {
      logger.log(ctx, err, 'API connection error');
      return wrapper.error(err);
    }
  }
}

module.exports = Registration;
