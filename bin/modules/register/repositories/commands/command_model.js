const joi = require('@hapi/joi');

const register = joi.object({
  fullName: joi.string().required(),
  email: joi.string().required(),
  password: joi.string().required(),
  mobileNumber: joi.string().required(),
  gender: joi.string().required()
});

module.exports = {
  register
};
