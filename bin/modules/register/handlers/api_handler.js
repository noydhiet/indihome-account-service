const wrapper = require('../../../helpers/utils/wrapper');
const RegisterCommand = require('../repositories/commands/domain');
const validator = require('../../../helpers/utils/validator');
const commandModel = require('../repositories/commands/command_model');

const registerUser = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, commandModel.register);
  const registerCommand = new RegisterCommand();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return registerCommand.registerUser(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Check Exsisting Indihome Account')
      : wrapper.response(res, 'success', result, 'Check Exsisting Indihome Account');
  };
  sendResponse(await postData(validatePayload));
};

module.exports = {
  registerUser
};
