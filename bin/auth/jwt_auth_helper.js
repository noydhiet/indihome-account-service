
const jwt = require('jsonwebtoken');
const wrapper = require('../helpers/utils/wrapper');
const { ERROR } = require('../helpers/http-status/status_code');
const { UnauthorizedError, ForbiddenError } = require('../helpers/error');
const ssoService = require('../services/ssoService');

const getToken = (headers) => {
  if (headers && headers.authorization && headers.authorization.includes('Bearer')) {
    const parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted.pop();
    }
  }
  return undefined;
};

module.exports.verifyToken = function ( requireSSOData = true) {
  return async function(req, res, next)
  {
    const result = {
      err: null,
      data: null
    };
    const verifyKey = process.env.JWT_AUTH_KEY;
    const token = getToken(req.headers);
    if (!token) {
      result.err = new ForbiddenError('Invalid token!');
      return wrapper.response(res, 'fail', result, 'Invalid token!', ERROR.FORBIDDEN);
    }
    try {
      req.userData = jwt.verify(token, verifyKey);
      if (requireSSOData) {
        const userData = await ssoService.getSSOUserData(req.userData.userId);
        if (!userData) {
          return wrapper.error('SSO user not found!', 404);
        }
        req.userData.email = userData.email;
        req.userData.mobile = userData.mobile;
      }
      next();
    } catch (error) {
      if (error instanceof jwt.TokenExpiredError) {
        result.err = new UnauthorizedError('Access token expired!');
        return wrapper.response(res, 'fail', result, 'Access token expired!', ERROR.UNAUTHORIZED);
      }
      result.err = new UnauthorizedError('Token is not valid!');
      return wrapper.response(res, 'fail', result, 'Token is not valid!', ERROR.UNAUTHORIZED);
    }
  };
};
