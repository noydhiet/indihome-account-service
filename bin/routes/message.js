/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/29/2019
 */

const express = require('express');
const router = express.Router();
const basicAuth = require('../auth/basic_auth_helper');
const bearerAuth = require('../auth/jwt_auth_helper');

const MessageController = require('../controllers/messageController');


router.get('/', bearerAuth.verifyToken(), MessageController.getAllUserInboxMessages);

router.get('/messages/:type', bearerAuth.verifyToken(), MessageController.getMessagesByType);

router.post('/messages/:messageId/status', bearerAuth.verifyToken(), MessageController.updateMessage);

router.post('/messages', basicAuth.isAuthenticated, MessageController.saveMessages);

module.exports = router;
