const express = require('express');
const router = express.Router();
const basicAuth = require('../auth/basic_auth_helper');
const bearerAuth = require('../auth/jwt_auth_helper');
const userHandler = require('../modules/user/handlers/api_handler');
const ProfileController = require('../controllers/userProfileController');

//user profile
//@todo: change API to resource reference format
router.get('/userCheck', basicAuth.isAuthenticated, userHandler.checkUser);

router.post('/signup', basicAuth.isAuthenticated, ProfileController.signUpUser);

router.get('/profile', bearerAuth.verifyToken(), ProfileController.searchUserByEmail);

router.get('/profile/:userId', basicAuth.isAuthenticated,ProfileController.searchUserByEmail);

router.put('/profile', bearerAuth.verifyToken(), ProfileController.editUserProfile);

router.post('/indihomeNumber',basicAuth.isAuthenticated,ProfileController.postAccountByIndiHomeNum);

router.get('/checkIndihomeNumber/:indiHomeNumber',basicAuth.isAuthenticated,ProfileController.getIndihomeNumberValidity);

router.get('/checkPortfolioId/:accountId',basicAuth.isAuthenticated, ProfileController.getAccountIdValidity);

router.get('/account/:indiHomeNumber', basicAuth.isAuthenticated,ProfileController.getAccountByIndiHomeNum);

router.post('/accountRequest',basicAuth.isAuthenticated,ProfileController.postAccountLinkRequests);

router.post('/accountResponse',bearerAuth.verifyToken(),ProfileController.postAccountLinkResponse);

router.put('/email', bearerAuth.verifyToken(), ProfileController.changeUserEmail);

router.put('/mobile', bearerAuth.verifyToken(), ProfileController.changeUserMobile);

router.post('/verifyVerification',bearerAuth.verifyToken(), ProfileController.verrifyEmail);

router.get('/location',basicAuth.isAuthenticated, ProfileController.getUserLocation);

router.post('/location', basicAuth.isAuthenticated, ProfileController.addUserLocation);

router.put('/reservations', basicAuth.isAuthenticated, ProfileController.refreshAccountNumbers);

router.get('/accountRequest/:referenceId', basicAuth.isAuthenticated, ProfileController.getAccountLinkRequest);

router.post('/revocable-location', basicAuth.isAuthenticated, ProfileController.addUserRevocableLocation);

router.delete('/:email', basicAuth.isAuthenticated, ProfileController.deleteUser);

router.put('/subscription-information', basicAuth.isAuthenticated, ProfileController.updateUserInformation);

module.exports = router;
