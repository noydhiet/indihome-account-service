/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/11/2019
 */

const express = require('express');
const router = express.Router();
const basicAuth = require('../auth/basic_auth_helper');
const bearerAuth = require('../auth/jwt_auth_helper');
const userHandler = require('../modules/user/handlers/api_handler');
const deviceController = require('../controllers/userDeviceController');
const accountController =  require('../controllers/accountController');

// rewards activation
router.get('/reward/status', basicAuth.isAuthenticated, accountController.getRewardsActiveStatus);

router.put('/reward/status', basicAuth.isAuthenticated, accountController.setRewardsActiveStatus);

// account activation
router.get('/activeStatus', basicAuth.isAuthenticated, accountController.getActiveStatus);

router.put('/activeStatus', basicAuth.isAuthenticated, accountController.setActiveStatus);
router.put('/verification-deadline', basicAuth.isAuthenticated, accountController.setVerificationDeadline);

//device
router.get('/device/:userId', basicAuth.isAuthenticated, deviceController.getDevices);

router.post('/device', bearerAuth.verifyToken(), deviceController.saveUserDevices);

router.delete('/device', bearerAuth.verifyToken(), deviceController.deleteDevice);

//kycsignup
router.get('/kyc', basicAuth.isAuthenticated, accountController.getKycImages);

router.post('/kyc', bearerAuth.verifyToken(), accountController.submitKyc);

router.put('/kyc', basicAuth.isAuthenticated, accountController.updateData);

router.get('/kyc/loadOrder', basicAuth.isAuthenticated, accountController.loadOrder);

router.post('/kyc/cancelOrder', basicAuth.isAuthenticated, accountController.cancelOrder);

router.get('/kyc/status', bearerAuth.verifyToken(), accountController.getKyc);


//svm
router.put('/svm', bearerAuth.verifyToken(), accountController.updateSvmLevel);

router.get('/svm/:indiHomeNum', basicAuth.isAuthenticated, accountController.getSvmLevel);

router.post('/svm/verify/ip', bearerAuth.verifyToken(), accountController.verifyIp);

router.post('/svm/verify/ktp', bearerAuth.verifyToken(), accountController.verifyKtp);

router.put('/svm/verify/ktp', basicAuth.isAuthenticated, accountController.updateVerifiedKtp);

router.get('/svm/verify/call/:indiHomeNum', bearerAuth.verifyToken(), accountController.processVerificationCall);

router.put('/svm/verify/call/:indiHomeNum', bearerAuth.verifyToken(), accountController.validateSvmCall);


//account Management
router.post('/defaultAccount',bearerAuth.verifyToken(), accountController.setDefaultAccount);
router.post('/disconnetAccount',bearerAuth.verifyToken(), accountController.disconnetAccount);
router.post('/removeChildAccount',bearerAuth.verifyToken(), accountController.deleteAccount);
router.post('/users',bearerAuth.verifyToken(), accountController.getUserAccounts);


module.exports = router;
