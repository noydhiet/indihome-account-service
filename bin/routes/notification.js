/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/29/2019
 */

const express = require('express');
const router = express.Router();
const basicAuth = require('../auth/basic_auth_helper');
const bearerAuth = require('../auth/jwt_auth_helper');

const NotificationController = require('../controllers/notificationController');

router.get('/', bearerAuth.verifyToken(), NotificationController.getNotificationsByType);

router.put('/:notificationId/status', bearerAuth.verifyToken(), NotificationController.updateNotification);

router.post('/', basicAuth.isAuthenticated, NotificationController.saveNotifications);

router.post('/settings',bearerAuth.verifyToken(), NotificationController.manageUserNotification);

module.exports = router;
