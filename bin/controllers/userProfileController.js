/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/05/2019
 */
const wrapper = require('../helpers/utils/wrapper');
const {STATUS_TYPES, ERROR } = require('../helpers/http-status/status_code');
const {BadRequestError} = require('../../bin/helpers/error');
const _ = require('lodash');
const validator = require('../helpers/utils/validator');
const modelCommand = require('../modules/user/repositories/commands/command_model');
const userProfileService = require('../services/profileService');
const UserAPI = require('../modules/user/repositories/commands/domain');


// eslint-disable-next-line no-empty-function
module.exports.signUpUser = (req, res) => {
  const user = req.body;

  if(_.isEmpty(user)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty user profile.')),
      'User cannot be empty', ERROR.BAD_REQUEST);
  }
  userProfileService.saveAndSignUp(user)
    .then(resp => {

      return  wrapper.formattedResponse(res,resp.ok,resp.status,resp.message,resp.data,200);

    })
    .catch(err => {
      return  wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.searchUserByEmail = (req, res) => {
  const userId = req.userData && req.userData.userId || req.params.userId;

  if(_.isEmpty(userId)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Invalid userId.')),
      'userId cannot be empty', ERROR.BAD_REQUEST);
  }
  userProfileService.getUserProfileByEmail(userId)
    .then(resp => {
      return  wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

// to get indihome number information and available options

module.exports.postAccountByIndiHomeNum = (req, res) => {
  const indiHomeNum = req.body.indihomeNumber;
  const custEmail = req.body.childEmail;

  if(_.isEmpty(indiHomeNum)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Invalid indiHomeNum.')),
      'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
  }
  if(_.isEmpty(custEmail)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Invalid childEmail.')),
      'childEmail cannot be empty', ERROR.BAD_REQUEST);
  }

  userProfileService.getAccountsByIndiHomeNum(indiHomeNum,custEmail)
    .then(resp => {
      wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};


// to get indihome accounts by indihome number

module.exports.getAccountByIndiHomeNum = (req, res) => {
  const indiHomeNum = req.params.indiHomeNumber;

  if(_.isEmpty(indiHomeNum)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Invalid indiHomeNum.')),
      'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
  }
  userProfileService.getAccountByIndiHomeNum(indiHomeNum,indiHomeNum)
    .then(resp => {
      return  wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return  wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

// Send account link requests
module.exports.postAccountLinkRequests = (req, res) => {
  const indiHomeNum = req.body.indihomeNumber;
  const custEmail = req.body.childEmail;
  const parentSsoId = req.body.parentSsoId;

  if(_.isEmpty(indiHomeNum)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Invalid indiHomeNum.')),
      'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
  }
  userProfileService.sendAccountLinkRequest(indiHomeNum,custEmail,parentSsoId)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};



// respond to account link requests

module.exports.postAccountLinkResponse = (req, res) => {
  const referenceId = req.body.referenceId;
  const reponseBool = req.body.isAccepted;

  if(_.isEmpty(referenceId)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Invalid referenceId.')),
      'Reference no cannot be empty', ERROR.BAD_REQUEST);
  }
  userProfileService.accountLinkResponse(referenceId,reponseBool)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};


// to get indihome number information and available options

module.exports.getAccountLinkRequest= (req, res) => {
  const referenceId = req.params.referenceId;

  if(_.isEmpty(referenceId)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Invalid indiHomeNum.')),
      'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
  }
  userProfileService.getAccountRequest(referenceId)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};


// To check the indihome number validity

module.exports.getIndihomeNumberValidity= (req, res) => {
  const indihomeNumber = req.params.indiHomeNumber;

  if(_.isEmpty(indihomeNumber)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Invalid indihomeNumber.')),
      'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
  }
  userProfileService.verifyIndiHomeNumber(indihomeNumber)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

// To check the indihome number validity

module.exports.getAccountIdValidity= (req, res) => {
  const accountId = req.params.accountId;

  if(_.isEmpty(accountId)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Invalid account id.')),
      'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
  }
  userProfileService.verifyAccountId(accountId)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.changeUserEmail = (req, res) => {
  const payload = req.body;
  userProfileService.changeUserEmail(payload, req.userData.userId)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};


module.exports.changeUserMobile = (req, res) => {
  const payload = req.body;
  userProfileService.changeUserMobile(payload,req.userData.userId)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.addUserLocation = (req, res) => {
  userProfileService.createAccountForUserLocation(req.body)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.addUserRevocableLocation = (req, res) => {
  userProfileService.createRevocableAccountForUserLocation(req.body)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.getUserLocation = (req, res) => {
  const userId = req.query.userId;
  const accountId = req.query.accountId || '';
  userProfileService.getUserLocations(userId, accountId)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.editUserProfile = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.updateUserJoi);
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return await userProfileService.editUserProfile(payload, req.userData.userId);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Edit user profile failed')
      : wrapper.response(res, 'success', result, 'Edit user profile successful');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports.deleteUser = (req, res) => {
  const email = req.params.email;
  userProfileService.deleteUser(email)
    .then(resp => {
      return  wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.verrifyEmail = (req, res) => {
  const payload = req.body;
  userProfileService.verifyVerification(payload, req.userData.userId)
    .then(resp => {
      return  wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.refreshAccountNumbers = (req, res) => {
  const payload = req.body;
  userProfileService.refreshAccountNumbers(payload, payload.email)
    .then(resp => {
      return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.updateUserInformation = (req, res) => {

  userProfileService.updateUserInformation(req.body)
    .then(resp => {
      return  wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};



