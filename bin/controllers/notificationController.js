/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/28/2019
 */

const notificationService = require('../services/notificationService');
const wrapper = require('../helpers/utils/wrapper');
const {STATUS_TYPES, SUCCESS, ERROR } = require('../helpers/http-status/status_code');
const {BadRequestError} = require('../../bin/helpers/error');
const _ = require('lodash');
const {NOTIFICATION_TYPES} = require('../../bin/helpers/utils/constants/notificationConstants');

/**
 * Save user notifications
 */
module.exports.saveNotifications = (req, res) => {
  const notifications = req.body.notifications;

  if(_.isEmpty(notifications)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty notification.')),
      'Notification cannot be empty', ERROR.BAD_REQUEST);
  }
  notificationService.saveNotifications(notifications)
    .then(resp => {
      wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Data saved successfully', SUCCESS.CREATED);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

/**
 * Get notifications by type
 */
module.exports.getNotificationsByType = (req, res) => {
  const type = req.query.type;
  const indiHomeNum = req.query.indiHomeNum;
  const { userId } = req.userData;

  //validate notification types
  if (!_.isEmpty(type) && !NOTIFICATION_TYPES.includes(type)) {
    throw new BadRequestError('Invalid Type.');
  }
  notificationService.getNotificationsByType(type, userId,indiHomeNum)
    .then(resp => {
      if(_.isEmpty(resp)) {
        wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'No notifications found yet', SUCCESS.OK);
      } else {
        wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Data received successfully', SUCCESS.OK);
      }
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

/**
 * Update notification status
 */
module.exports.updateNotification = (req, res) => {
  const notificationId = req.params.notificationId;
  const checked= req.body.checked;

  if(_.isEmpty(notificationId) || _.isEmpty(req.body)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('notificationId or body cannot be empty')),
      'notificationId or body cannot be empty', ERROR.BAD_REQUEST);
  }
  notificationService.updateNotification(notificationId, checked)
    .then(resp => {
      wrapper.response(res, STATUS_TYPES.SUCCESS, resp, 'Data updated successfully', SUCCESS.OK);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};


module.exports.manageUserNotification = async (req, res) => {
  const userId = req.userData.userId;
  const type = req.body.type;
  const value = req.body.value;

  if(_.isEmpty(type)){
    wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error('Invalid indihomeNumber.'));
  }
  notificationService.manageUserNotification(userId,type,value)
    .then(resp => {
      wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};
