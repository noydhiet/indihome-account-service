/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/28/2019
 */

const messageService = require('../services/messageService');
const wrapper = require('../helpers/utils/wrapper');
const {STATUS_TYPES, SUCCESS, ERROR } = require('../helpers/http-status/status_code');
const {BadRequestError} = require('../../bin/helpers/error');
const _ = require('lodash');
const {MESSAGE_TYPES} = require('../helpers/utils/constants/messageConstants');
/**
 * Save user notifications
 */
module.exports.saveMessages = (req, res) => {
  const messages = req.body.messages;

  if(_.isEmpty(messages)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty messages.')),
      'Messages cannot be empty', ERROR.BAD_REQUEST);
  }
  messageService.saveMessages(messages)
    .then(resp => {
      wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

/**
 * Get all inbox messages
 */
module.exports.getAllUserInboxMessages = (req, res) => {
  const { userId } = req.userData;

  messageService.getUserMessages(userId)
    .then(resp => {
      wrapper.response(res, STATUS_TYPES.SUCCESS, resp, 'Data received successfully', SUCCESS.OK);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

/**
 * Get all user messages by type
 */
module.exports.getMessagesByType = (req, res) => {
  const type = req.params.type;
  const { userId } = req.userData;

  if(_.isEmpty(type)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty type given.')),
      'Type cannot be empty', ERROR.BAD_REQUEST);
  }
  //validate message types
  if (![MESSAGE_TYPES.PERSONAL, MESSAGE_TYPES.PROMO].includes(type)) {
    throw new BadRequestError('Invalid Type.');
  }
  messageService.getMessagesByType(type, userId)
    .then(resp => {
      if(_.isEmpty(resp)) {
        wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'No message found yet', SUCCESS.OK);
      } else {
        wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Data received successfully', SUCCESS.OK);
      }
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

/**
 * Update message stats
 */
module.exports.updateMessage = (req, res) => {
  const messageId = req.params.messageId;
  const checked= req.body.checked;

  if(_.isEmpty(messageId) || _.isEmpty(req.body)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('messageId or body cannot be empty')),
      'messageId or body cannot be empty', ERROR.BAD_REQUEST);
  }

  messageService.updateMessage(messageId, checked)
    .then(resp => {
      wrapper.response(res, STATUS_TYPES.SUCCESS, resp, 'Data updated successfully', SUCCESS.OK);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};
