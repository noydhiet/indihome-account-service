/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/26/2019
 */
const wrapper = require('../helpers/utils/wrapper');
const {STATUS_TYPES,ERROR } = require('../helpers/http-status/status_code');
const {BadRequestError} = require('../../bin/helpers/error');
const _ = require('lodash');
const userDeviceService = require('../services/deviceService');

module.exports.saveUserDevices = (req, res) => {
  let user = req.body;
  if(req.userData.email){
    user.ssoUserId = req.userData.userId;
  }else{
    user.ssoUserId = 'guest';
  }

  if(_.isEmpty(user)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty device.')),
      'Device cannot be empty', ERROR.BAD_REQUEST);
  }
  userDeviceService.saveUserDecive(user)
    .then(resp => {
      wrapper.formattedResponse(res,resp.ok,resp.status,resp.message,resp.data,200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};


module.exports.deleteDevice = (req, res) => {
  let user = req.body;
  if(req.userData.email){
    user.ssoUserId = req.userData.userId;
  }else{
    user.ssoUserId = 'guest';
  }

  if(_.isEmpty(user)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty device.')),
      'Device cannot be empty', ERROR.BAD_REQUEST);
  }
  userDeviceService.deleteDecive(user.fcmToken,user.ssoUserId)
    .then(resp => {
      wrapper.formattedResponse(res,resp.ok,resp.status,resp.message,resp.data,200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.getDevices = (req, res) => {
  let user = req.params;

  if(_.isEmpty(user)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty request.')),
      'param cannot be empty', ERROR.BAD_REQUEST);
  }
  userDeviceService.getDecives(user.userId)
    .then(resp => {
      wrapper.formattedResponse(res,resp.ok,resp.status,resp.message,resp.data,200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};