/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Sajidh Zazahir on 12/24/2019
 */

const accountService = require('../services/accountService');
const wrapper = require('../helpers/utils/wrapper');
const validator = require('../helpers/utils/validator');
const modelCommand = require('../modules/user/repositories/commands/command_model');
const {BadRequestError} = require('../../bin/helpers/error');
const {STATUS_TYPES, SUCCESS, ERROR } = require('../helpers/http-status/status_code');
const _ = require('lodash');

/**
 * Get rewards active status
 */
module.exports.getRewardsActiveStatus = async (req, res) => {
  const payload = {indiHomeNum: req.query.indiHomeNum};
  const validatePayload = validator.isValidPayload(payload, modelCommand.getAccountCacheJoi);
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return await accountService.getRewardsActiveStatus(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Get rewards active status failed')
        : wrapper.response(res, 'success', result, 'Get rewards active status successful');
  };
  sendResponse(await postData(await validatePayload));
};

/**
 * Set rewards active status
 */
module.exports.setRewardsActiveStatus = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.updateAccountStatusJoi);
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return await accountService.setRewardsActiveStatus(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Update rewards status failed')
      : wrapper.response(res, 'success', result, 'Update rewards status successful');
  };
  sendResponse(await postData(await validatePayload));
};

/**
 * Get account active status
 */
module.exports.getActiveStatus = async (req, res) => {
  const payload = {indiHomeNum: req.query.indiHomeNum};
  const validatePayload = validator.isValidPayload(payload, modelCommand.getAccountCacheJoi);
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return await accountService.getActiveStatus(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Get account active status failed')
      : wrapper.response(res, 'success', result, 'Get account active status successful');
  };
  sendResponse(await postData(await validatePayload));
};

/**
 * Set active status
 */
module.exports.setActiveStatus = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.updateAccountStatusJoi);
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return await accountService.setActiveStatus(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Update account status failed')
      : wrapper.response(res, 'success', result, 'Update account status successful');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports.updateSvmLevel = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.updateSvmLevelJoi);
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return await accountService.updateSvmLevel(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Update svm level failed')
      : wrapper.response(res, 'success', result, 'Update svm level successful');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports.getSvmLevel = async (req, res) => {
  const indiHomeNum = req.params.indiHomeNum;
  if(_.isEmpty(indiHomeNum)){
    return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty user profile.')),
      'indiHomeNum be empty', ERROR.BAD_REQUEST);
  }
  const getData = async (result) => {
    if(result && result.err){
      return result;
    }
    return await accountService.getSvmLevel(indiHomeNum);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Get svm level failed')
      : wrapper.response(res, 'success', result, 'Get svm level successful');
  };
  sendResponse(await getData());
};

module.exports.verifyIp = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.verifyIpJoi);
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return await accountService.verifyIpAddress(payload, req.userData);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Verify ip failed')
      : wrapper.response(res, 'success', result, 'verify ip success');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports.verifyKtp = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.verifyKtpJoi);
  const postData = async (result) => {
    if(result && result.err){
      return result;
    }
    return await accountService.verifyKtpNumber(payload, req.userData);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Verify ktp failed')
      : wrapper.response(res, 'success', result, 'verify ktp success');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports.updateVerifiedKtp = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.updateVerifyKtpJoi);
  const postData = async (result) => {
    if(result && result.err){
      return result;
    }
    return await accountService.updateVerifiedKtp(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Update ktp verification status failed')
      : wrapper.response(res, 'success', result, 'Update ktp verification status success');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports.processVerificationCall = async (req, res) => {
  const indiHomeNum = req.params.indiHomeNum;
  const payload = {indiHomeNum: indiHomeNum};
  const validatePayload = validator.isValidPayload(payload, modelCommand.requestSvmCallJoi);

  const getData = async (result) => {
    if(result && result.err){
      return result;
    }
    return await accountService.verifySvmByCall(req.userData.mobile, indiHomeNum);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'processing verification call failed')
      : wrapper.response(res, 'success', result, 'processing verification call success');
  };
  sendResponse(await getData(await validatePayload));
};

module.exports.validateSvmCall = async (req, res) => {
  const payload = req.body;
  payload.indiHomeNum = req.params.indiHomeNum;
  const validatePayload = validator.isValidPayload(payload, modelCommand.verifyByCallJoi);
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return await accountService.validateSvmCallPin(payload, req.userData.mobile);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'validating call information failed')
      : wrapper.response(res, 'success', result, 'validating call information success');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports.submitKyc = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.saveKycJoi);
  const postData = async (result) => {
    if(result && result.err){
      return result;
    }
    return await accountService.saveKyc(payload, req.userData.userId);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Update account status failed')
      : wrapper.response(res, 'success', result, 'Update account status successful');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports.getKycImages = async (req, res) => {
  const trackId = req.query.trackId;
  accountService.getKyc(trackId)
    .then(resp => {
      let data = {};
      if(resp.data){
        data = {
          document: resp.data.document,
          signature: resp.data.signature,
          documentWithUser: resp.data.documentWithUser
        };
      }
      resp.data = data;
      wrapper.response(res, STATUS_TYPES.SUCCESS, resp, 'Data received successfully', SUCCESS.OK);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.getKyc = async (req, res) => {
  const trackId = req.query.trackId;
  accountService.getKyc(trackId)
    .then(resp => {
      wrapper.response(res, STATUS_TYPES.SUCCESS, resp, 'Data received successfully', SUCCESS.OK);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.cancelOrder = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.cancelOrderJoi);
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return await accountService.cancelOrder(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'cancelling order failed')
      : wrapper.response(res, 'success', result, 'cancelling order successful');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports.updateData = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, modelCommand.updateKycJoi);
  const postData = async (result) => {
    if(result && result.err){
      return result;
    }
    return await accountService.updateKycData(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, result.err || 'Update kyc data failed')
      : wrapper.response(res, 'success', result, 'Update kyc data successful');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports.loadOrder = async (req, res) => {
  const trackId = req.query.trackId;
  accountService.loadOrder(trackId)
    .then(resp => {
      wrapper.response(res, STATUS_TYPES.SUCCESS, resp, 'Data received successfully', SUCCESS.OK);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};


module.exports.setDefaultAccount = async (req, res) => {
  const userId = req.userData.userId;
  const indiHomeNumber = req.body.indiHomeNumber;

  if(_.isEmpty(indiHomeNumber)){
    wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error('Invalid indihomeNumber.'));
  }
  accountService.setDefaultAccount(indiHomeNumber,userId)
    .then(resp => {
      wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};



module.exports.disconnetAccount = async (req, res) => {
  const userId = req.userData.userId;
  const indiHomeNumber = req.body.indiHomeNumber;

  if(_.isEmpty(indiHomeNumber)){
    wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error('Invalid indihomeNumber.'));
  }
  accountService.disconnectAccount(indiHomeNumber,userId,false)
    .then(resp => {
      wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};


module.exports.deleteAccount = async (req, res) => {
  const userId = req.userData.userId;
  const childId = req.body.childId;
  const indiHomeNumber = req.body.indiHomeNumber;

  if(_.isEmpty(indiHomeNumber)){
    wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error('Invalid indihomeNumber.'));
  }
  accountService.deleteAccount(indiHomeNumber,userId,childId)
    .then(resp => {
      wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};


module.exports.getUserAccounts = (req, res) => {
  const indiHomeNumber = req.body.indiHomeNumber;
  const userData = req.userData;
  accountService.getManageUsers(indiHomeNumber,userData)
    .then(resp => {
      wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

module.exports.setVerificationDeadline = (req, res) => {
  const indiHomeNumber = req.body.indiHomeNumber;
  accountService.setVerificationDeadline(indiHomeNumber)
    .then(resp => {
      wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, 200);
    })
    .catch(err => {
      wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(err));
    });
};

