/* eslint-disable no-console */
const userAccountConnect = require('../helpers/databases/sequelize');
const UserProfileConnect = require('../helpers/databases/sequelize');
const {Location,Package} = require('../helpers/databases/sequelize');
const { BadRequestError } = require('../helpers/error');
const logger = require('../helpers/utils/logger');
const ctx = 'account-repository';
const notificationService = require('../services/notificationService');
const ssoService = require('../services/ssoService');
const { NOTIFICATION_CATEGORIES, NOTIFICATION_TYPES, KYC_STATUS} = require('../helpers/utils/constants/accountServiceConstants');

const updateRewardsActiveStatus = async (payload) => {
  // activate
  if(payload.status) {
    return await userAccountConnect.Account.update(
      { rewardsActive: payload.status
      },
      { where: { indiHomeNum: payload.indiHomeNum } }
    );
  }
  // deactivate
  return await userAccountConnect.Account.update(
    { rewardsActive: payload.status
    },
    { where: { indiHomeNum: payload.indiHomeNum } }
  );

};

const updateAccountStatus = async (payload) => {
  if(payload.status) {
    return await userAccountConnect.Account.update(
      { active: payload.status,
        activatedDate : new Date().toISOString()
      },
      { where: { indiHomeNum: payload.indiHomeNum } }
    );
  }
  return await userAccountConnect.Account.update(
    { active: payload.status,
      deactivatedDate : new Date().toISOString()
    },
    { where: { indiHomeNum: payload.indiHomeNum } }
  );

};

const updateSvmLevel = async (payload) => {
  return await userAccountConnect.Account.update(
    { svmLevel: payload.svmLevel },
    { where: { indiHomeNum: payload.indiHomeNum } }
  );
};

const getAccountData = async (indiHomeNum) => {
  try {
    let account = await userAccountConnect.Account.findOne(
      {where: {indiHomeNum: indiHomeNum}}
    );
    if (account && account.dataValues) {
      return account.dataValues;
    }
  } catch (error) {
    const msg = new BadRequestError('Error while finding account');
    logger.log(ctx, msg, error);
  }
  return null;
};

const saveAccountforUser = async (payload) => {
  let userDetails = await UserProfileConnect.Account.create(payload,{include:[Location,{model: Package, as: 'packages'} ]}).then((account) => {
    logger.log(ctx, 'Account saved in user', account.dataValues);
    return {
      ok: true,
      status: 200,
      message: 'success',
      data: payload
    };
  }).catch((error) => {
    let msg;
    if (error.message) {
      msg = error.message;
    }
    else {
      msg = error.errors[0].message;
    }
    logger.log(ctx, msg, error);
    return {
      ok: false,
      status: 406,
      message: error.errors[0].message,
      data: {}
    };
  });
  if (!userDetails.ok) {
    return {
      ok: false,
      status: userDetails.status,
      message:userDetails.message,
      data: {}
    };
  }
  return userDetails;

};

const getAccountById = async (id) => {

  try {
    let user = await UserProfileConnect.Account.findOne({
      where: {id}, include:['Location','packages']
    });
    if (user && user.dataValues) {
      return user;
    }
  } catch (error) {
    const msg = new BadRequestError('Error while finding user');
    logger.log(ctx, msg, error);
  }
  return null;
};

const saveKyc = (payload) => {
  return userAccountConnect.Kyc.findOne(
    {where: {trackId: payload.trackId}}
  ).then((kycData) => {
    if(kycData){
      return userAccountConnect.Kyc.update({
        documentWithUser: payload.documentWithUser,
        signature: payload.signature,
        document: payload.document,
        identityType: payload.identityType
      }, {where: { trackId : payload.trackId}});
    }
    return userAccountConnect.Kyc.create(payload);
  });

};

const getKycData = async (trackId) => {
  return userAccountConnect.Kyc.findOne(
    {where: {trackId: trackId}}
  );
};

const getBillingData = async (trackId) => {
  return userAccountConnect.BillingDetails.findOne(
    {where: {trackId: trackId}}
  );
};

const getKycDataWithUserAndLocation = async (trackId) => {
  return userAccountConnect.User.findOne(
    {include:[{
      model: userAccountConnect.Account,as: 'accounts',
      include:[{
        model : userAccountConnect.Kyc,as: 'kycs',
        where: { trackId : trackId}
      }]
    },
    {
      model: userAccountConnect.Location, as: 'locations'
    }]}
  );
};

const getLinkedUsers = async (indiHomeNumIn) => {
  const account = userAccountConnect.User.findAll(
    {
      include:[{
        model : userAccountConnect.Account, as: 'accounts',
        where: {indiHomeNum: indiHomeNumIn},include: [{model: Location}]
      }]
    },
  ).then((accountData) => {
    if (!accountData) {
      logger.log(ctx, 'No account found', 'No account');
      return null;
    }
    logger.log(ctx, 'Account fetched', accountData.dataValues);
    return {
      ok: true,
      code: 200,
      status: 200,
      message:'success',
      data: accountData
    };
  }).catch((err) => {
    const msg = new BadRequestError('Error while finding account');
    logger.log(ctx, msg, err);
    return {
      ok: false,
      code: 500,
      status: 500,
      message: 'Error while finding account',
      data: {}
    };
  });
  if (!account) {
    return {
      ok: false,
      code: 404,
      status: 404,
      message: 'No User',
      data: {}
    };
  }
  return account;
};

const cancelOrder = async (payload) => {
  return userAccountConnect.Kyc.update(
    {
      cancelledReasonId: payload.reasonId,
      cancelledReason: payload.reasonDescription,
      cancelledBy: payload.cancelledBy,
      isCancelled: true,
      validationStatus: KYC_STATUS.CANCELLED
    },
    { where: { trackId: payload.trackId } }
  );
};

const updateKyc = async (payload) => {
  let updatePayload  =   {
    validationStatus: payload.validationStatus,
    validatedBy: payload.validatedBy,
    invalidReason: payload.validationStatus.toUpperCase() !== KYC_STATUS.PROCESSED ? payload.invalidReason : ''
  };

  if(payload.validationStatus.toUpperCase() === KYC_STATUS.PROCESSED){
    updatePayload.identityType = payload.identityType;
  }

  return userAccountConnect.Kyc.update(
    updatePayload,
    { where: { trackId: payload.trackId } }
  );
};

const updateUserData = async (payload) => {
  return await userAccountConnect.Kyc.findOne(
    { where: { trackId: payload.trackId } }
  ).then((userData) => {
    if(userData){
      return userAccountConnect.Account.findOne({ where: { id: userData.AccountId } }).then(
        (account) => {
          if(account){
            return userAccountConnect.User.update(
              {
                name: payload.customer.name,
                dob: payload.customer.dateOfBirth,
                identityType: payload.identityType,
                identityStatus: 'validated',
                idNum: payload.customer.identityNumber,
                profession: payload.customer.profession,
                nationality: payload.customer.nationality,
                birthPlace: payload.customer.placeOfBirth,
                idExpDate: payload.customer.validityPeriod
              },
              { where: { id: account.UserId } }
            ).then(() => {
              return userAccountConnect.Location.update(
                {
                  city: payload.customer.city,
                  cityCode: payload.customer.cityCode,
                  district: payload.customer.district,
                  districtCode: payload.customer.districtCode,
                  street: payload.customer.street,
                  streetCode: payload.customer.streetCode,
                  blockNumber: payload.customer.blockNumber,
                  postalCode: payload.customer.postalCode,
                },
                { where: { UserId: account.UserId } }
              ).then(() => {
                userAccountConnect.BillingDetails.findOne({ where: { AccountId: account.id }})
                  .then((oldBillingData) => {
                    let billingData = payload.billing || payload.customer;
                    billingData.AccountId = account.id;
                    billingData.trackId = payload.trackId;
                    if(oldBillingData){
                      return userAccountConnect.BillingDetails.update( billingData,
                        { where: { id: oldBillingData.id } }
                      );
                    }
                    return userAccountConnect.BillingDetails.create( billingData);
                  });
              }).catch(e => logger.log(ctx, 'update user failed', ''));
            });
          }
        }
      );
    }

    return null;
  })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding kyc data');
      logger.log(ctx, msg, err);
      return null;
    });
};

const sendKycStatusNotifications = async (payload) => {
  let topic  = '';
  let description = '';
  const status = payload.validationStatus.toUpperCase();

  switch(status){
  case KYC_STATUS.PROCESSING:
    topic = 'Verifying your information';
    description = 'Please wait while we verify your ID & signature';
    break;
  case KYC_STATUS.INVALID:
    topic = 'Please reupload documents';
    description = 'Please reupload invalid documents to continue';
    break;
  case KYC_STATUS.PROCESSED:
    topic = 'Continue your order';
    description = 'Your documents are verified please continue your order';
    break;
  case KYC_STATUS.NOT_RESPONDING:
    topic = 'Validation couldn\'t be completed';
    description = payload.invalidReason;
    break;
  }

  return await userAccountConnect.Kyc.findOne(
    { where: { trackId: payload.trackId } }
  ).then((userData) => {
    if(userData){
      return userAccountConnect.Account.findOne({ where: { id: userData.AccountId } }).then(
        async (account) => {

          const user = await ssoService.getSSOUserDataByEmail(account.email);
          const notification = [
            {
              userId: user._id,
              header: 'Kyc status: ' + payload.validationStatus,
              isChecked: false,
              referenceId: payload.trackId,
              notificationType: NOTIFICATION_TYPES.ACTIVITY,
              category: NOTIFICATION_CATEGORIES.UPDATE_KYC,
              imageUrl: '',
              body: {
                topic: topic,
                description: description, // VD not given
                data: {
                  payload: payload
                }
              }
            }
          ];
          await notificationService.saveNotifications(notification);
        }
      );
    }

    return null;
  })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding kyc data');
      logger.log(ctx, msg, err);
      return null;
    });
};

const updateAccount = async (accountId, account) => {
  return await userAccountConnect.Account.update(
    account,
    {where: {id: accountId}}
  );
};

const setDefaultAccount = async (indiHomeNumber, userMail) => {

  let userDetails = await UserProfileConnect.Account.update(
    {isDefault:false},
    {where: {email: userMail,isDefault:true}}
  )
    .then((userData) => {
      if (!userData) {
        logger.log(ctx, 'No user found', 'No user');
        return {
          ok: false,
          status: 404,
          message: 'No User',
          data: {}
        };
      }

      return {
        ok: true,
        status: 200,
        message: 'success',
        data: {}
      };

    })
    .catch((err) => {
      const errMsg = new BadRequestError('Error while finding user');
      logger.log(ctx, errMsg, err);
      return {
        ok: false,
        status: 404,
        message: errMsg.message,
        data: {}
      };
    });

  if(!userDetails.ok) {
    return userDetails;
  }

  let setAccount = await UserProfileConnect.Account.update(
    {isDefault:true},
    {where: {email: userMail,indiHomeNum:indiHomeNumber}}
  ).then((userData) => {
    if (!userData) {
      logger.log(ctx, 'No user found', 'No user');
      return {
        ok: false,
        status: 404,
        message: 'No User',
        data: {}
      };
    }

    return {
      ok: true,
      status: 200,
      message: 'success',
      data: {}
    };

  })
    .catch((err) => {
      const errMsg = new BadRequestError('Error while finding user');
      logger.log(ctx, errMsg, err);
      return {
        ok: false,
        status: 404,
        message: errMsg.message,
        data: {}
      };
    });

  if(!setAccount.ok){
    return setAccount;
  }

  return {
    ok: true,
    status: 200,
    message: 'success updating default',
    data: {
      default:indiHomeNumber
    }
  };
};


const deleteAccount = async (indiHomeNumber,userId) => {

  let accountDetails = await UserProfileConnect.Account.destroy({
    where: { indiHomeNum:indiHomeNumber,id:userId }
  }).then((userData) => {
    if (!userData) {
      logger.log(ctx, 'No account', 'No account');
      return null;
    }
    logger.log(ctx, 'account delete', userData.dataValues);
    return {
      ok: true,
      status: 200,
      message: 'account successfully deleted',
      data: {}
    };
  }).catch((err) => {
    const msg = new BadRequestError('Error while finding user');
    logger.log(ctx, msg, err);
    return {
      ok: false,
      status: 500,
      message: 'Internal server error',
      data: {}
    };
  });

  if (!accountDetails) {
    return {
      ok: false,
      status: 404,
      message: 'No account',
      data: {}
    };
  }

  return accountDetails;
};


const getParentAccount = async (indiHomeNumIn,email) => {

  let account = await UserProfileConnect.Account.findAll({
    where: {indiHomeNum: indiHomeNumIn,parent:'self',email:email}
  })
    .then((accountData) => {

      if (!accountData) {
        logger.log(ctx, 'No account found', 'No account');
        return null;
      }

      logger.log(ctx, 'Account fetched', accountData.dataValues);
      return {
        ok: true,
        code: 200,
        status: 200,
        message:'success',
        data: accountData
      };
    })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding account');
      logger.log(ctx, msg, err);
      return {
        ok: false,
        code: 500,
        status: 500,
        message: 'Error while finding account',
        data: {}
      };


    });

  if (!account) {
    return {
      ok: false,
      code: 404,
      status: 404,
      message: 'No User',
      data: {}
    };
  }

  return account;
};

const connectDisconnectAccount = async (indiHomeNumber, userMail,isConnected) => {

  let setAccount = await UserProfileConnect.Account.update(
    {isConnected:isConnected},
    {where: {email: userMail,indiHomeNum:indiHomeNumber}}
  ).then((userData) => {
    if (!userData) {
      logger.log(ctx, 'No user found', 'No user');
      return {
        ok: false,
        status: 404,
        message: 'No User',
        data: {}
      };
    }

    return {
      ok: true,
      status: 200,
      message: 'success',
      data: {}
    };

  })
    .catch((err) => {
      const errMsg = new BadRequestError('Error while finding user');
      logger.log(ctx, errMsg, err);
      return {
        ok: false,
        status: 404,
        message: errMsg.message,
        data: {}
      };
    });

  if(!setAccount.ok){
    return setAccount;
  }

  return {
    ok: true,
    status: 200,
    message: 'success disconnecting account',
    data: {
      default:indiHomeNumber
    }
  };
};

/**
 *
 * @param updatedAccount
 * @param updatedAccount.indiHomeNum
 * @param updatedAccount.verificationDeadline
 * @returns {Promise<*>}
 */
const updateAccountVerificationDeadline = async (updatedAccount) => {
  if (updatedAccount.indiHomeNum) {
    return await userAccountConnect.Account.update(
      {
        verificationDeadline: updatedAccount.verificationDeadline
      },
      {where: {indiHomeNum: updatedAccount.indiHomeNum}}
    );
  }
  logger.log(ctx, `no indiHomeNum number provided${JSON.stringify(updatedAccount)}`, 'error');
  throw new BadRequestError('no indiHomeNum number provided');
};


const getAccountsByEmail = async (emailIn) => {
  return UserProfileConnect.Account.findAll({
    where: {email: emailIn}
  });
};

module.exports = {
  updateAccountStatus,
  updateRewardsActiveStatus,
  saveKyc,
  updateSvmLevel,
  getAccountData,
  getKycData,
  updateUserData,
  updateKyc,
  cancelOrder,
  getKycDataWithUserAndLocation,
  saveAccountforUser,
  getAccountById,
  updateAccount,
  setDefaultAccount,
  sendKycStatusNotifications,
  getParentAccount,
  deleteAccount,
  getLinkedUsers,
  connectDisconnectAccount,
  updateAccountVerificationDeadline,
  getBillingData,
  getAccountsByEmail
};
