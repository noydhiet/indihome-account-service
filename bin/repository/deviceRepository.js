/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/26/2019
 */
const joi = require('@hapi/joi');
const UserProfileConnect = require('../helpers/databases/sequelize');
const _ = require('lodash');
const validator = require('../helpers/utils/validator');
const { BadRequestError } = require('../helpers/error');
const logger = require('../helpers/utils/logger');
const ctx = 'server-checkIndihomeAccount-profile-service';



const deviceSchema = joi.object({
  deviceId: joi.string().required(),
  ssoUserId: joi.string().required(),
  fcmToken: joi.string().required(),
  deviceType: joi.string()
});


const checkFCM = async (fcmToken) => {

  let userDetails = await UserProfileConnect.Device.findOne({
    where: { fcmToken: fcmToken }
  })
    .then((userData) => {

      if (!userData) {
        logger.log(ctx, 'No user found', 'No user');
        return {
          ok: false,
          status: 404,
          message: 'No User',
          data: {}
        };
      }

      logger.log(ctx, 'devices fetched', '');
      return {
        ok: true,
        status: 200,
        message: 'success',
        data: {}
      };
    })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding user');
      logger.log(ctx, msg, err);
    });

  if (!userDetails) {
    return {
      ok: false,
      status: 404,
      message: 'No User',
      data: {}
    };
  }

  return userDetails;
};


const saveDevice = async (payload) => {

  const validateData = validator.isValidPayload(payload, deviceSchema);
  if (validateData.err) {
    const msg = new BadRequestError('Error in data');
    logger.log(ctx, msg, validateData.err);
    return {
      ok: false,
      status: 403,
      message: 'Error in data',
      data: {}
    };
  }


  let userDetails = await UserProfileConnect.Device.upsert(payload).then((device) => {
    logger.log(ctx, 'Device saved in user', device.dataValues);
    return {
      ok: true,
      status: 200,
      message: 'success',
      data: payload
    };
  }).catch((error) => {
    let msg;
    if (error.message) {
      msg = error.message;
    }
    else {
      msg = error.errors[0].message;
    }
    logger.log(ctx, msg, error);
    return {
      ok: false,
      status: 406,
      message: error.errors[0].message,
      data: {}
    };
  });
  if (!userDetails) {
    return {
      ok: false,
      status: 500,
      message: 'error saving',
      data: {}
    };
  }
  return userDetails;
};


const getDevicesWithEmail = async (userId) => {

  let userDetails = await UserProfileConnect.Device.findAll({
    where: { ssoUserId: userId },
    attributes: ['fcmToken']
  })
    .then((userData) => {

      if (!userData) {
        logger.log(ctx, 'No user found', 'No user');
        return null;
      }

      logger.log(ctx, 'devices fetched', userData);
      let deviceIds = [];
      userData.forEach(element => deviceIds.push(element.fcmToken));
      return {
        ok: true,
        status: 200,
        message: 'success',
        data: deviceIds
      };
    })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding user');
      logger.log(ctx, msg, err);
    });

  if (!userDetails) {
    return {
      ok: false,
      status: 404,
      message: 'No User',
      data: {}
    };
  }

  return userDetails;
};

const deleteDevice = async (fcmToken,userId) => {

  let userDetails = await UserProfileConnect.Device.destroy({
    where: { fcmToken: fcmToken,ssoUserId:userId }
  }).then((userData) => {
    if (!userData) {
      logger.log(ctx, 'No user found', 'No user');
      return null;
    }
    logger.log(ctx, 'device delete', userData.dataValues);
    return {
      ok: true,
      status: 200,
      message: 'device successfully deleted',
      data: {}
    };
  }).catch((err) => {
    const msg = new BadRequestError('Error while finding user');
    logger.log(ctx, msg, err);
  });

  if (!userDetails) {
    return {
      ok: false,
      status: 404,
      message: 'No device',
      data: {}
    };
  }

  return userDetails;
};


module.exports = {
  saveDevice,deleteDevice,getDevicesWithEmail
};
