/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/05/2019
 */
const joi = require('@hapi/joi');
const UserProfileConnect = require('../helpers/databases/sequelize');
const _ = require('lodash');
const validator = require('../helpers/utils/validator');
const {BadRequestError} = require('../helpers/error');
const logger = require('../helpers/utils/logger');
const ctx = 'server-checkIndihomeAccount-profile-service';

const {Location, Account} = require('../helpers/databases/sequelize');

/**
 * Object model for User Profile
 */
const userSchema = joi.object({
  name: joi.string().required(),
  primaryPhone: joi.string().required(),
  secondaryPhone: joi.string().optional().allow(''),
  email: joi.string().required(),
  dob: joi.string().optional().allow(''),
  birthPlace: joi.string().optional().allow(''),
  country: joi.string().optional().allow(''),
  gender: joi.string().optional().allow(''),
  nationality: joi.string().optional().allow('')
});

//todo: schema
const locationSchema = joi.object({
  userId: joi.string().optional(),
  userEmail: joi.string().required(),
  deviceId: joi.string().allow('').optional(),
  sto: joi.string().allow('').optional(),
  locId: joi.string().allow('').optional(),
  province: joi.string().optional(),
  provinceCode: joi.string().optional(),
  city: joi.string().optional(),
  cityCode: joi.string().optional(),
  district: joi.string().optional(),
  districtCode: joi.string().optional(),
  street: joi.string().optional(),
  streetCode: joi.string().optional(),
  blockNumber: joi.string().optional(),
  postalCode: joi.string().optional(),
  addressDesc: joi.string().optional(),
  block: joi.string().optional(),
  floor: joi.string().optional(),
  room: joi.string().optional(),
  latitude: joi.string().required(),
  longitude: joi.string().required(),
  areaCode: joi.string().optional(),
  rt: joi.string().optional(),
  rw: joi.string().optional(),
  system: joi.string().allow('').optional(),
  level: joi.string().optional(),
  cableName: joi.string().allow('').optional(),
});

/**
 * This method is used to fetch the users by using indihome number.
 * @param {Number} indiHomeNumIn - Indihome number of the users.
 */

const getBasicUserByIndiHomeNum = async (indiHomeNumIn) => {

  let userEmail = await UserProfileConnect.Account.findOne({
    where: {indiHomeNum: indiHomeNumIn},
  })
    .then((findedUser) => {

      if (!findedUser) {
        logger.log(ctx, 'No account found', 'No account');
        return null;
      }

      return findedUser.dataValues.email;
    })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding account');
      logger.log(ctx, msg, err);
    });


  if (!userEmail) {
    return {
      ok: false,
      code: 404,
      status: 404,
      message: 'No account',
      data: {}
    };
  }

  let userDetails = await UserProfileConnect.User.findOne({
    where: {email: userEmail},
  })
    .then((userData) => {

      if (!userData) {
        logger.log(ctx, 'No user found', 'No user');
        return null;
      }

      logger.log(ctx, 'User fetched', userData.dataValues.name);
      return {
        ok: true,
        code: 200,
        status: 200,
        message: 'success',
        data: userData.dataValues
      };
    })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding user');
      logger.log(ctx, msg, err);
    });

  if (!userDetails) {
    return {
      ok: false,
      code: 406,
      status: 406,
      message: 'No User',
      data: {}
    };
  }

  return userDetails;
};

const getUserProfileByEmail = async (userEmail, include = ['accounts']) => {

  let userDetails = await UserProfileConnect.User.findOne({
    where: {email: userEmail}, include: include
  })
    .then((userData) => {

      if (!userData) {
        logger.log(ctx, 'No user found', 'No user');
        return null;
      }

      logger.log(ctx, 'User fetched', userData.dataValues.name);
      return {
        ok: true,
        code: 200,
        status: 200,
        message: 'success',
        data: userData.dataValues
      };
    })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding user');
      logger.log(ctx, msg, err);
      return {
        ok: false,
        code: 200,
        status: 406,
        message: 'Error while finding user',
        data: {}
      };
    });

  if (!userDetails) {
    return {
      ok: false,
      code: 404,
      status: 404,
      message: 'No User',
      data: {}
    };
  }

  return userDetails;
};

const getAccountByIndiHomeNum = async (indiHomeNum, include = ['Location']) => {

  let account = await UserProfileConnect.Account.findOne({
    where: {indiHomeNum: indiHomeNum}, include: include
  })
    .then((accountData) => {

      if (!accountData) {
        logger.log(ctx, 'No account found', 'No account');
        return null;
      }

      logger.log(ctx, 'Account fetched', accountData.dataValues.email);
      return {
        ok: true,
        code: 200,
        status: 200,
        message: 'success',
        data: accountData.dataValues
      };
    })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding account');
      logger.log(ctx, msg, err);
    });

  if (!account) {
    return {
      ok: false,
      code: 404,
      status: 404,
      message: 'No User',
      data: {}
    };
  }

  return account;
};

const getUserProfileById = async (id, include = ['accounts', 'locations']) => {

  try {
    let user = await UserProfileConnect.User.findOne({
      where: {id}, include: include
    });
    if (user && user.dataValues) {
      return user.dataValues;
    }
  } catch (error) {
    const msg = new BadRequestError('Error while finding user');
    logger.log(ctx, msg, error);
  }
  return null;
};

const getUserProfileByMobile = async (mobileNum, include = ['accounts']) => {

  let userDetails = await UserProfileConnect.User.findOne({
    where: {primaryPhone: mobileNum}, include: include
  })
    .then((userData) => {

      if (!userData) {
        logger.log(ctx, 'No user found', 'No user');
        return null;
      }

      logger.log(ctx, 'User fetched', userData.dataValues.name);
      return {
        ok: true,
        code: 200,
        status: 200,
        message: 'success',
        data: userData.dataValues
      };
    })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding user');
      logger.log(ctx, msg, err);
    });

  if (!userDetails) {
    return {
      ok: false,
      code: 404,
      status: 404,
      message: 'No User',
      data: {}
    };
  }

  return userDetails;
};

/**
 *
 * @param payload
 * @param payload.dob
 * @param payload.gender
 * @param payload.name
 * @param currentUserMail
 * @returns {Promise<*>}
 */
const updateUserProfile = async (payload, currentUserMail) => {
  return await UserProfileConnect.User.update(
    {
      dob: payload.dob,
      gender: payload.gender,
      name: payload.name
    },
    {where: {email: currentUserMail}}
  );
};

const saveUser = async (payload) => {

  const validateData = validator.isValidPayload(payload, userSchema);

  if (validateData.err) {
    const msg = new BadRequestError('Error in data');
    logger.log(ctx, msg, validateData.err);
    return {
      ok: false,
      code: 403,
      status: 403,
      message: 'Error in data',
      data: {}
    };
  }

  let userDetails = await UserProfileConnect.User.create(payload).then((user) => {
    logger.log(ctx, 'User created in profile', user.dataValues.name);
    return {
      ok: true,
      code: 200,
      status: 200,
      message: 'success',
      data: user.dataValues
    };
  }).catch((error) => {

    // console.log('lets check this',error.errors[0].message);
    let msg;
    if (error.message) {
      msg = error.message;
    } else {
      msg = error.errors[0].message;
    }

    logger.log(ctx, msg, error);
    return {
      ok: true,
      code: 406,
      status: 406,
      message: error.errors[0].message,
      data: {}
    };
  });

  if (!userDetails) {
    return {
      ok: false,
      code: 500,
      status: 500,
      message: 'error saving',
      data: {}
    };
  }
  return userDetails;
};

const changeUserEmail = async (payload, currentUserMail) => {
  let userDetails = await UserProfileConnect.User.update(
    {email: payload.value},
    {where: {email: currentUserMail}}
  )
    .then((userData) => {
      if (!userData) {
        logger.log(ctx, 'No user found', 'No user');
        return {
          ok: false,
          code: 404,
          status: 404,
          message: 'No User',
          data: {}
        };
      }

      return {
        ok: true,
        code: 200,
        status: 200,
        message: 'success',
        data: {}
      };

    })
    .catch((err) => {
      const errMsg = new BadRequestError('Error while finding user');
      logger.log(ctx, errMsg, err);
      return {
        ok: false,
        code: 404,
        status: 404,
        message: errMsg.message,
        data: {}
      };
    });

  return userDetails;
};

const changeUserMobile = async (payload, currentUserMail) => {

  let userDetails = await UserProfileConnect.User.update(
    {primaryPhone: payload.value},
    {where: {email: currentUserMail}}
  )
    .then((userData) => {
      if (!userData) {
        logger.log(ctx, 'No user found', 'No user');
        return {
          ok: false,
          code: 404,
          status: 404,
          message: 'No User',
          data: {}
        };
      }

      return {
        ok: true,
        code: 200,
        status: 200,
        message: 'success',
        data: {}
      };

    })
    .catch((err) => {
      const errMsg = new BadRequestError('Error while finding user');
      logger.log(ctx, errMsg, err);
      return {
        ok: false,
        code: 404,
        message: errMsg.message,
        status: 404,
        data: {}
      };
    });

  return userDetails;
};

const addUserLocation = async (user, location, fixedLine, internetLine) => {
  const validatedData = validator.isValidPayload(location, locationSchema);
  if (validatedData.err) {
    const msg = new BadRequestError('Error in data');
    logger.log(ctx, msg, validatedData.err);
    return {
      ok: false,
      status: 403,
      message: 'Error in data',
      data: {}
    };
  }

  try {

    // create new Account with the location
    const account = {
      internetNum: internetLine.internetNumber,
      //change indiHomeNumber based on the priority,  if fixed phone is available, it's fixed phone, otherwise internet num
      indiHomeNum: fixedLine.telephone || internetLine.internetNumber,
      fixedPhoneNum: fixedLine.telephone,
      ncli: fixedLine.ncli,
      email: user.email,
      Location: location,
      UserId: user.id,
      transaction: fixedLine.transaction,
      reservationId: internetLine.reservationId,
      reservationPort: fixedLine.reservationPort
    };

    location.UserId = user.id;

    //save account
    let savedAccount = await UserProfileConnect.Account.create(account, {include: [Location]});
    // let savedAccount = await UserProfileConnect.Account.upsert(account, {include: [Location], where: {id: account.id}});
    logger.log(ctx, `saved created account with the location: : ${savedAccount}`);


    // add account to the user
    user.accounts.push(account);
    user.locations.push(location);

    const updatedUser = await UserProfileConnect.User.upsert(user, {where: {id: user.id}, include: [Account, Location]});
    logger.log(ctx, `added Location: ${JSON.stringify(location)} as : ${JSON.stringify(updatedUser)}`);
    return {
      ok: true,
      status: 200,
      message: 'success',
      data: {
        location,
        // savedLocation,
        user,
        updatedUser,
        account,
        savedAccount
      }
    };
  } catch (error) {
    logger.log(ctx, error.message || error.errors[0].message, error);
    return {
      ok: false,
      status: 406,
      message: error.message || error.errors[0].message, error,
      data: {}
    };
  }
};

const updateUserLocation = async (user, location, fixedLine, internetLine, inactiveAccount) => {
  const validatedData = validator.isValidPayload(location, locationSchema);
  if (validatedData.err) {
    const msg = new BadRequestError('Error in data');
    logger.log(ctx, msg, validatedData.err);
    return {
      ok: false,
      status: 403,
      message: 'Error in data',
      data: {}
    };
  }

  try {

    const oldAccountId = inactiveAccount.indiHomeNum;
    location.UserId = user.id;
    location.AccoutId = oldAccountId;
    inactiveAccount.Location = location;

    inactiveAccount.internetNum = internetLine.internetNumber;
    inactiveAccount.indiHomeNum = fixedLine.telephone || internetLine.internetNumber;
    inactiveAccount.fixedPhoneNum = fixedLine.telephone;
    inactiveAccount.ncli = fixedLine.ncli;
    inactiveAccount.email = user.email;
    inactiveAccount.Location = location;
    inactiveAccount.UserId = user.id;
    inactiveAccount.transaction = fixedLine.transaction;
    inactiveAccount.reservationId = internetLine.reservationId;
    inactiveAccount.reservationPort = fixedLine.reservationPort;

    const inactiveAccountInfo = await getAccountByIndiHomeNum(oldAccountId);
    if (inactiveAccountInfo && inactiveAccountInfo.data) {
      //todo: check is this updating the location or not
      //this is not updating
      let newLocation = location;
      newLocation.id = inactiveAccountInfo.data.Location.dataValues.id;
      UserProfileConnect.Location.update(newLocation, {where: {id: newLocation.id}});
    }

    //save account
    let savedAccount = await UserProfileConnect.Account.upsert(inactiveAccount, {include: [Location], where: {id: oldAccountId}});
    logger.log(ctx, `updated account with the location:  ${JSON.stringify(savedAccount)}`);

    // add account to the user
    user.accounts.push(inactiveAccount);
    user.locations.push(location);

    const updatedUser = await UserProfileConnect.User.upsert(user, {where: {id: user.id}, include: [Account, Location]});
    logger.log(ctx, `added Location: ${JSON.stringify(location)} as : ${JSON.stringify(updatedUser)}`);
    return {
      ok: true,
      status: 200,
      message: 'success',
      data: {
        location,
        // savedLocation,
        user,
        updatedUser,
        account: inactiveAccount,
        savedAccount
      }
    };
  } catch (error) {
    logger.log(ctx, error.message || error.errors[0].message, error);
    return {
      ok: false,
      status: 406,
      message: error.message || error.errors[0].message, error,
      data: {}
    };
  }
};

const getUserLocationsForAccount = async (id, accountId) => {
  if (_.isEmpty(id)) {
    return {
      ok: false,
      code: 403,
      status: 403,
      message: 'Error in data',
      data: {}
    };
  }

  try {
    //  load the the user with the location
    let user;
    if (accountId) {
      user = await UserProfileConnect.User.findOne({
        where: {id},
        include: [{model: Account, as: 'accounts', where: {id: accountId}, include: [{model: Location}]}]
      });
    } else {
      user = await UserProfileConnect.User.findOne({
        where: {id},
        include: [
          {
            model: Account,
            as: 'accounts',
            where: {UserId: id},
            include: [{model: Location}
            ]
          }
        ]
      });
    }

    if (user && user.dataValues) {
      logger.log(ctx, `user Found ${JSON.stringify(user)}`);
      return {
        ok: true,
        status: 200,
        message: 'success',
        data: {...user.dataValues}
      };
    }
    return {
      ok: false,
      status: 200,
      message: 'no users found',
      data: {}
    };
  } catch (error) {
    logger.log(ctx, error.message || error.errors[0].message, error);
    return {
      ok: true,
      status: 406,
      message: error.message || error.errors[0].message,
      data: {}
    };
  }
};

const getUserLocations = async (id) => {
  if (_.isEmpty(id)) {
    return {
      ok: false,
      code: 403,
      status: 403,
      message: 'Error in data',
      data: {}
    };
  }

  try {
    //  load the the user with the location
    //todo: get the location associated to the accountId or default location
    let user = await UserProfileConnect.User.findOne({
      where: {id},
      include: ['locations']
    });
    if (user && user.dataValues) {
      logger.log(ctx, `user Found ${JSON.stringify(user)}`);
      return {
        ok: true,
        status: 200,
        message: 'success',
        data: {...user.dataValues}
      };
    }
    return {
      ok: false,
      status: 200,
      message: 'no users found',
      data: {}
    };
  } catch (error) {
    logger.log(ctx, error.message || error.errors[0].message, error);
    return {
      ok: true,
      status: 406,
      message: error.message || error.errors[0].message,
      data: {}
    };
  }
};

const deleteUser = async (email) => {

  try {
    //  load the the user with the location
    //todo: get the location associated to the accountId or default location
    let userDeleted = await UserProfileConnect.User.destroy({
      where: {email: email}
    });
    if (userDeleted === 1) {
      logger.log(ctx, 'User deleted');
      return {
        ok: true,
        status: 200,
        message: 'success',
        data: {}
      };
    }
    return {
      ok: false,
      status: 200,
      message: 'error in deleting user',
      data: {}
    };
  } catch (error) {
    logger.log(ctx, error.message || error.errors[0].message, error);
    return {
      ok: true,
      status: 406,
      message: error.message || error.errors[0].message,
      data: {}
    };
  }
};


const verifyUserEmail = async (payload, currentUserMail) => {
  let userDetails = await UserProfileConnect.User.update(
    {emailVarified: true},
    {where: {email: currentUserMail}}
  )
    .then((userData) => {
      if (!userData) {
        logger.log(ctx, 'No user found', 'No user');
        return {
          ok: false,
          code: 404,
          status: 404,
          message: 'No User',
          data: {}
        };
      }

      return {
        ok: true,
        code: 200,
        status: 200,
        message: 'success',
        data: {}
      };

    })
    .catch((err) => {
      const errMsg = new BadRequestError('Error while finding user');
      logger.log(ctx, errMsg, err);
      return {
        ok: false,
        code: 404,
        status: 404,
        message: errMsg.message,
        data: {}
      };
    });

  return userDetails;
};

const getParentAccountsByIndiHomeNum = async (indiHomeNumIn, include = ['Location']) => {

  let account = await UserProfileConnect.Account.findAll({
    where: {indiHomeNum: indiHomeNumIn, parent: 'self'}, include: include

  })
    .then((accountData) => {

      if (!accountData) {
        logger.log(ctx, 'No account found', 'No account');
        return null;
      }

      logger.log(ctx, 'Account fetched', accountData.dataValues);
      return {
        ok: true,
        code: 200,
        status: 200,
        message: 'success',
        data: accountData
      };
    })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding account');
      logger.log(ctx, msg, err);
      return {
        ok: false,
        code: 500,
        status: 500,
        message: 'Error while finding account',
        data: {}
      };


    });

  if (!account) {
    return {
      ok: false,
      code: 404,
      status: 404,
      message: 'No User',
      data: {}
    };
  }

  return account;
};


const getChildAccountsByIndiHomeNum = async (indiHomeNumIn, userId) => {

  let account = await UserProfileConnect.Account.findAll({
    where: {indiHomeNum: indiHomeNumIn, parent: userId}
  })
    .then((accountData) => {

      if (!accountData) {
        logger.log(ctx, 'No account found', 'No account');
        return null;
      }

      logger.log(ctx, 'Account fetched', accountData.dataValues);
      return {
        ok: true,
        code: 200,
        status: 200,
        message: 'success',
        data: accountData
      };
    })
    .catch((err) => {
      const msg = new BadRequestError('Error while finding account');
      logger.log(ctx, msg, err);
    });

  if (!account) {
    return {
      ok: false,
      code: 404,
      status: 404,
      message: 'No User',
      data: {}
    };
  }

  return account;
};

const getAccountsEmailandId = async (indiHomeNumIn, emailIn) => {
  return UserProfileConnect.Account.findOne({
    where: {indiHomeNum: indiHomeNumIn, email: emailIn}
  });
};

const updateUserInformation = async (transactionId, updatedProfile, updatedAccount, updatedLocation) => {

  const kyc = await UserProfileConnect.Kyc.findOne({where: {trackId: transactionId}}).catch(error => {
    logger.log(ctx, 'get account\'s KYC error', error);
    return null;
  });

  if (!(kyc && kyc.dataValues && kyc.dataValues.AccountId)) {
    return {
      ok: false,
      message: 'transaction information not found',
      status: 4004,
      data: {}
    };
  }

  const account = await UserProfileConnect.Account.findOne({where: {id: kyc.dataValues.AccountId}}).catch(error => {
    logger.log(ctx, 'get account error', error);
    return null;
  });

  let updates = [];

  let userId = account.dataValues.UserId;
  let accountId = account.dataValues.id;

  if (!(account && account.dataValues && userId)) {
    return {
      ok: false,
      message: 'account information not found',
      status: 4004,
      data: {}
    };
  }

  if (!_.isEmpty(updatedProfile)) {
    updates.push(
      UserProfileConnect.User
        .update(
          {
            dob: updatedProfile.dateOfBirth,
            idNum: updatedProfile.identityNumber,
            profession: updatedProfile.profession,
            nationality: updatedProfile.nationality,
            birthPlace: updatedProfile.placeOfBirth
          },
          {where: {id: userId}}
        ));
  }

  if (!_.isEmpty(updatedAccount)) {
    updates.push(
      UserProfileConnect.Account
        .update(
          {
            internetNum: updatedAccount.internetNumber,
            fixedPhoneNum: updatedAccount.telephone,
            ncli: updatedAccount.ncli,
            transaction: updatedAccount.transaction,
            reservationId: updatedAccount.reservationId,
            reservationPort: updatedAccount.reservationPort,
          },
          {where: {id: accountId}}
        ));
  }

  if (!_.isEmpty(updatedLocation)) {
    updates.push(
      UserProfileConnect.Location.update(
        {
          city: updatedLocation.city,
          cityCode: updatedLocation.cityCode,
          district: updatedLocation.district,
          districtCode: updatedLocation.districtCode,
          street: updatedLocation.street,
          streetCode: updatedLocation.streetCode,
          blockNumber: updatedLocation.blockNumber,
          postalCode: updatedLocation.postalCode,
          latitude: updatedLocation.latitude,
          longitude: updatedLocation.longitude,
          addressDesc: updatedLocation.addressDesc,
        },
        {where: {UserId: userId}}
      ));
  }

  const [profileUpdate, accountUpdate, locationUpdate] = await Promise.all(updates)
    .catch(reason => {
        logger.log(ctx, reason, 'update error');
        return {
          ok: false,
          message: `system error: ${reason.message}`,
          status: 500,
          data: {}
        };
      }
    );

  return {
    ok: true,
    message: 'data updated',
    status: 200,
    code: 200,
    data: {
      profileUpdate, accountUpdate, locationUpdate
    }
  };
};


const manageNotification = async (userEmail, type, value) => {
  let option = {};

  switch (type) {
  case 'PURCHASE':
    option = {isPurchaseNoti: value};
    break;
  case 'BILL':
    option = {isBillNoti: value};
    break;
  case 'ACCOUNT':
    option = {isAccountChangeNoti: value};
    break;
  case 'PROMO':
    option = {isPromotionsNoti: value};
    break;
  case 'LANGUAGE':

    if (!(value === 'en' || value === 'id')) {
      return {
        ok: false,
        status: 404,
        message: 'language type not found',
        data: {}
      };
    }
    option = {language: value};
    break;
  default:
    return {
      ok: false,
      status: 404,
      message: 'Type not found',
      data: {}
    };
  }

  let setAccount = await UserProfileConnect.User.update(
    option,
    {where: {email: userEmail}}
  ).then((userData) => {
    if (!userData) {
      logger.log(ctx, 'No user found', 'No user');
      return {
        ok: false,
        status: 404,
        message: 'No User',
        data: {}
      };
    }

    return {
      ok: true,
      status: 200,
      message: 'success',
      data: {}
    };

  })
    .catch((err) => {
      const errMsg = new BadRequestError('Error while finding user');
      logger.log(ctx, errMsg, err);
      return {
        ok: false,
        status: 404,
        message: errMsg.message,
        data: {}
      };
    });

  if (!setAccount.ok) {
    return setAccount;
  }

  return {
    ok: true,
    status: 200,
    message: 'success updating notification settings',
    data: {}
  };
};


module.exports = {
  getAccountByIndiHomeNum,
  getParentAccountsByIndiHomeNum,
  getChildAccountsByIndiHomeNum,
  getAccountsEmailandId,
  getBasicUserByIndiHomeNum,
  getUserProfileByEmail,
  getUserProfileByMobile,
  saveUser,
  changeUserEmail,
  changeUserMobile,
  getUserProfileById,
  addUserLocation,
  updateUserLocation,
  getUserLocations,
  getUserLocationsForAccount,
  deleteUser,
  verifyUserEmail,
  updateUserProfile,
  updateUserInformation,
  manageNotification
};
