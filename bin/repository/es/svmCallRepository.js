/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 1/29/20, 2:41 PM
 */
const elastic = require('../../helpers/es/db');
const config = require('../../config');

class SvmCallRepository {

  constructor() {
    this.elastic = elastic;
    this.esConig = config.get('/elasticSearch');
    this.index = config.get('/elasticIndexes').getEsSVMCallDataIndex;
    this.type = '_doc';
  }

  async getSvmCallData(userMobile) {
    const query = {
      index: this.index,
      type: this.type,
      body: {
        query: {
          match: {
            'Phone': userMobile
          }
        }
      }
    };
    return this.elastic.findOne(this.esConig, query);
  }

  /**
   * Save ktp data
   * Create or update
   * @param payload
   * @returns {Promise<{data: *, error: null} | {data: null, error: *}>}
   */
  async saveSvmCallData(payload) {
    const data = {
      index: this.index,
      type: this.type,
      id: payload.indiHomeNum,
      body: payload
    };
    return await this.elastic.insertData(this.esConig, data);
  }

  /**
   * update ktp data
   * @param payload
   * @returns {Promise<Promise<{data: *, error: null} | {data: null, error: *}>>}
   */
  async updateCallData(payload) {
    const query = {
      index: this.index,
      type: this.type,
      id: payload.indiHomeNum,
      body: {
        doc: payload
      }
    };
    return await this.elastic.updateData(this.esConig, query);
  }
}

module.exports = SvmCallRepository;
