const esClient = require('./client');
const config = require('../../config').get('/elasticSearch');

const createIndex = async function(indexName){
  let conn = await esClient.getConnection(config);
  return await conn.indices.create({
    index: indexName
  });
};

module.exports = createIndex;


// async function test(){
//   try {
//     const resp = await createIndex('inboxNotifications');
//     console.log(resp);
//   } catch (e) {
//     console.log(e);
//   }
// }

//test();
