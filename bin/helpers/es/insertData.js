const esClient = require('./client');
const config = require('../../config').get('/elasticSearch');

const insertDoc = async function(indexName, mappingType, data){
  let conn = await esClient.getConnection(config);
  const esData = {
    index: indexName,
    type: mappingType,
    body: data
  };
  return await conn.index(esData);
};


const insertBulkDoc = async function(bulkBody){
  let conn = await esClient.getConnection(config);
  return await conn.bulk({
    body: bulkBody
  });
};


module.exports = {insertDoc,insertBulkDoc};


// async function test(){
//   let newNotifications = [];
//   const metaData = { 'index' : { '_index' : 'inbox_notifications', '_type' : 'messages'} };
//   const data = {
//     timestamp: new Date().getTime() ,
//     checkDate: new Date().getTime() ,
//     header: 'Jalan Gatot Subroto Kav ',
//     referenceId: 'MYIN-15688904759911016',
//     isChecked: false,
//     messageType: 'promo',
//     imageUrl: 'https//asdasdasdasdadasdadadadadadad',
//     userId:'qweqweqwe1'
//   };
//   newNotifications.push(metaData);
//   newNotifications.push(data);

//   try {
//     console.log('gonna save',newNotifications);
//     const resp = await insertBulkDoc(newNotifications);
//     console.log(resp);
//   } catch (e) {
//     console.log(e);
//   }
// }


//test();




