const esClient = require('./client');
const config = require('../../config').get('/elasticSearch');

const searchDoc = async function(indexName, mappingType, payload){
  let conn = await esClient.getConnection(config);
  const esData = {
    index: indexName,
    type: mappingType,
    body: payload
  };

  return await conn.search(esData);
};

module.exports = searchDoc;


// async function test(){
//   const body = {
//     query: {
//       match: {
//         'title': 'Learn'
//       }
//     }
//   };
//   try {
//     const resp = await searchDoc('blog', 'ciphertrick', body);
//     console.log(resp);
//   } catch (e) {
//     console.log(e);
//   }
// }


//test();
