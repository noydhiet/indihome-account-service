const esClient = require('./client');
const config = require('../../config').get('/elasticSearch');

const addmappingToIndex = async function(indexName, mappingType, payload){
  // console.log(mapping);

  let conn = await esClient.getConnection(config);
  const esData = {
    index: indexName,
    type: mappingType,
    body: payload
  };
  return await conn.indices.putMapping(esData);
};

module.exports = addmappingToIndex;


// test function to explain how to invoke.
// async function InitialNotification(){
//   const mapping = {
//     properties: {
//       header: {
//         type: 'text'
//       },
//       isChecked: {
//         type: 'boolean'
//       },
//       checkDate: {
//         type: 'date'
//       },
//       referenceId: {
//         type: 'text'
//       },
//       messageType: {
//         type: 'text'
//       },
//       imageUrl: {
//         type: 'text'
//       },
//       timestamp: {
//         type: 'date'
//       }
//     }
//   };
//   try {
//     const resp = await addmappingToIndex('inboxNotifications', 'messages', mapping);
//     console.log(resp);
//   } catch (e) {
//     console.log(e);
//   }
// }


//test();
