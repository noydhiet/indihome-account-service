const esClient = require('./client');
const config = require('../../config').get('/elasticSearch');

const updateDoc = async function(indexName, mappingType,id, data){
  let conn = await esClient.getConnection(config);
  const esData = {
    index: indexName,
    type: mappingType,
    id: id,
    body: data
  };
  return await conn.index(esData);
};


module.exports = updateDoc;
