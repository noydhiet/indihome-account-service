/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/18/2019
 */

module.exports = {
  NOTIFICATION_TYPES: {
    ACTIVITY: 'activity'
  },
  NOTIFICATION_CATEGORIES: {
    UPDATE_KYC: 'UPDATE_KYC',
    VERIFY_KTP: 'VERIFY_KTP'
  },
  NOTIFICATION_GROUPS: {
    FULFILLMENT: 'FULFILLMENT',
    ASSURANCE: 'ASSURANCE',
    PROFILE: 'PROFILE',
  },
  KTP_VERIFICATION_STATUS: {
    IN_PROGRESS : 'IN_PROGRESS',
    COMPLETE : 'COMPLETE',
    FAILED : 'FAILED'
  },
  SVM_LEVEL: {
    ONE: '1',
    TWO: '2'
  },
  KYC_STATUS: {
    CANCELLED : 'CANCELLED',
    PROCESSING : 'PROCESSING',
    PROCESSED : 'PROCESSED',
    INVALID : 'INVALID',
    NOT_RESPONDING: 'NOT_RESPONDING'
  }
};
