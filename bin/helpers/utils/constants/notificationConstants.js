/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/28/2019
 */
const config = require('../../../config/index');

const homeNotificationTypes = config.get('/homeNotificationTypes').split(",");

module.exports = {
    NOTIFICATION_TYPES: homeNotificationTypes
};
