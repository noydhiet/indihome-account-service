/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 2/25/2020
 */
module.exports = {
    ACCOUNT_STATUS: {
        ACTIVE: 'active',
        INACTIVE: 'inactive'
    },
    REDIS_KEYS: {
        SVM_LEVEL: 'svmLevel',
        ACTIVE: 'active'
    }
};
