/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/28/2019
 */

module.exports = {
    MESSAGE_TYPES: {
        PROMO: 'public',
        PERSONAL: 'personal'
    }
};
