
const getDateFormatted =  async () => {
  let dateobj= new Date() ;
  let dd   = ('00'+dateobj.getDate().toString()).slice(-2);
  let mm   = ('00'+(dateobj.getMonth()+1).toString()).slice(-2); //January is 0!
  let yyyy = dateobj.getFullYear().toString();
  let hour = ('00'+dateobj.getHours().toString()).slice(-2);
  let minu = ('00'+dateobj.getMinutes().toString()).slice(-2);
  let sec = ('00'+dateobj.getSeconds().toString()).slice(-2);
  return yyyy+mm+dd+hour+minu+sec;
};

module.exports = {
  getDateFormatted
};
