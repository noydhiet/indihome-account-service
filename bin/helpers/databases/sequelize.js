/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathna on 12/02/2019
 */
const Sequelize = require('sequelize');
const UserModel = require('../../modules/user/models/userModel');
const AccountModel = require('../../modules/user/models/accountModel');
const LocationModel = require('../../modules/user/models/locationModel');
const PackageModel = require('../../modules/user/models/packageModel');
const AccountLinkModel = require('../../modules/user/models/acccountLinkModel');
const DeviceModel = require('../../modules/user/models/deviceModel');
const KycModel = require('../../modules/user/models/kycModel');
const billingDetailsModel = require('../../modules/user/models/billingDetailsModel');

const config = require('../../config');
const mysqlConfig = config.get('/mysqlConfig');

const sequelize = new Sequelize(mysqlConfig.database, mysqlConfig.user, mysqlConfig.password, {
  host: mysqlConfig.host,
  dialect: 'mysql',
  port : mysqlConfig.port,
  charset: 'utf8',
  collate: 'utf8_general_ci',
  timestamps: true
});

const User = UserModel(sequelize, Sequelize);
const Account = AccountModel(sequelize, Sequelize);
const Location = LocationModel(sequelize, Sequelize);
const Package = PackageModel(sequelize, Sequelize);
const AccountLink = AccountLinkModel(sequelize, Sequelize);
const Device = DeviceModel(sequelize, Sequelize);
const Kyc = KycModel(sequelize, Sequelize);
const BillingDetails = billingDetailsModel(sequelize, Sequelize);

User.hasMany(Location, {as: 'locations'});
User.hasMany(Account, {as: 'accounts', onDelete: 'cascade',
  hooks: true});
Location.belongsTo(User);
Account.belongsTo(User);
Account.hasMany(Kyc, {as: 'kycs'});
Account.hasMany(BillingDetails, {as: 'billingDetails'});

Account.hasMany(Package, {as: 'packages',onDelete: 'cascade',hooks: true});
Package.belongsTo(Account);
Account.hasOne(Location);

module.exports = {
  User,
  Account,
  Location,
  Package,
  AccountLink,
  Device,
  Kyc,
  BillingDetails,
  sequelize
};
